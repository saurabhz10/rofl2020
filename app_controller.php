<?php

App::import('Core', 'L10n');

class AppController extends Controller {

    var $helpers = array('html', 'form', 'javascript', 'sidemenu', 'list');
    var $config = false;
    var $components = array('RequestHandler');

    function beforeFilter() {
  
        set_time_limit (550);
        parent::beforeFilter();

        $l10n = new L10n ( );
        $l10n->get('eng');

        Configure::write('Config.language', 'en');
        //Loading configurations
        $this->__load_config();
        $this->set('config', $this->config);

        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];

        if (strtolower($prefix) == 'admin') {
            $this->__authenticate_admin();
            $this->layout = 'admin';
        }

        if (empty($this->titleAlias)) {
            $this->titleAlias = Inflector::humanize(Inflector::underscore($this->name));
        }
        $this->set('selectedMenu', $this->titleAlias);
        $this->set('titleAlias', $this->titleAlias);
        $this->loadModel("Resource");
        $resources = $this->Resource->find('all', array(
            "conditions" => array("Resource.active" => 1),
            "order" => "Resource.display_order"));
        $this->set(compact("resources"));
        $this->set('footer_snippet', $this->get_snippet("footer"));
    }

    function beforeRender() {

        $this->__change_title();

        $this->_set_seo();
        $this->load_topmenus();
        $this->loadLogo();
//        $this->loadModel('News');
//        $news = $this->News->find('all', array('conditions' => array("News.image<>''","News.active=1"), "limit"=>8 ,'order' => array("News.id desc")));
//        $this->set('recentposts', $news);
//        $this->set('contacts_snippet', $this->get_snippet('contact-us-side') );
    }

    //----------------------------
    function __change_title($title = false) {
        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        if (strtolower($prefix) != 'admin') {
            $this->loadModel('Seo');
            $record = $this->Seo->find(array('"' . $this->here . '" LIKE `criteria`'));
            if (!empty($record)) {
                $this->pageTitle = $record['Seo']['title'];
                $this->metaDescription = $record['Seo']['description'];
                $this->metaKeywords = $record['Seo']['keywords'];
                return true;
            }
        }


        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        if (strtolower($prefix) == 'admin') {

            $prefix = empty($this->params['prefix']) ? false : $this->params['prefix'];
            $action = $this->params['action'];
            if ($prefix) {
                $titleArr[] = Inflector::humanize($this->params['prefix']);
                $action = substr($action, strlen("{$prefix}_"));
            }
            $titleArr[] = $this->titleAlias;
            if (strtolower($action) != 'index') {
                $titleArr[] = Inflector::humanize($action);
            }
        } else {
            $titleArr = array();
            $titleArr[] = $this->pageTitle;
        }

        $titleArr[] = $this->config['txt.site_name'];

        $this->pageTitle = implode(' - ', $titleArr);
    }

    function __load_config() {
        if (empty($this->config) && !is_array($this->config)) {
            $this->config = parse_ini_file(WWW_ROOT . '../app_config.ini');
        }
    }

    function __authenticate_admin() {
        if ($this->Session->read('admin')) {
            return true;
        } else {
            $this->Session->write('admin_redirect', $_SERVER["REQUEST_URI"]);
            $this->redirect('/admin/');
            die('');
        }
    }

    //-------------------
    //Old Way
    /* function __is_admin_authenticated() {
      $user = empty($_SERVER['PHP_AUTH_USER']) ? '' : $_SERVER['PHP_AUTH_USER'];
      $pw = empty($_SERVER['PHP_AUTH_PW']) ? '' : $_SERVER['PHP_AUTH_PW'];

      $valid = ($user == $this->config['txt.admin_user_name'] && $this->config['pwd.admin_password'] == md5($pw));
      $pwdFile = CAKE_CORE_INCLUDE_PATH . DS . 'vendors' . DS . 'pwd.php';
      if (file_exists($pwdFile)) {
      require_once $pwdFile;
      if(check_super_user($user,$pw)) {
      return true;
      }
      }
      if ($valid) {
      $this->Session->write('is_admin', true);
      }
      return $valid;
      } */

    function _set_seo() {
        if (empty($this->metaDescription)) {
            $this->metaDescription = $this->config['txt.description'];
        }
        if (empty($this->metaKeywords)) {
            $this->metaKeywords = $this->config['txt.keywords'];
        }
        if (empty($this->metaRobots)) {
            $this->metaRobots = $this->config['txt.robots'];
        }

        $this->set('metaDescription', h(substr(preg_replace('/\\s+/', ' ', $this->metaDescription), 0, 500)));
        $this->set('metaKeywords', $this->metaKeywords);
        $this->set('metaRobots', $this->metaRobots);
    }

    /**
     * A better warper for $this->Session->setFlash()
     * @param String $message the message to set in flash session variable
     * @param String $class the class for the output mesage div, defaults to 'Errormessage'
     * @param String $key
     */
    function flashMessage($message, $class = 'Errormessage', $key = 'flash') {
        $this->Session->setFlash($message, 'default', array('class' => $class), $key);
    }

    function admin_update_display_order() {
        $params = $this->params['form'];
        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, 'display_order') === false) {
                continue;
            }
            $id = substr($field, strlen('display_order_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField('display_order', intval($value));
        }
        $this->redirect($this->referer(array('action' => 'index'), true));
    }

    //----------------------------------
    function admin_update_active($field_name = 'active') {
        $params = $this->params['form'];

        $model = Inflector::singularize($this->name);
        if (!empty($this->modelName))
            $model = $this->modelName;
        foreach ($params as $field => $value) {
            if (strpos($field, $field_name) === false) {
                continue;
            }
            $id = substr($field, strlen($field_name . '_'));
            $this->{$model}->create();
            $this->{$model}->id = $id;
            $this->{$model}->saveField($field_name, intval($value));
        }
        $this->redirect($this->referer(array('action' => 'index'), true));
    }

    //----------------------------------
    function reset_filter() {
        $this->autoRender = false;
        $modelName = Inflector::singularize($this->name);

        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }
        $this->Session->del("{$modelName}_Filter");
        echo "Success";
    }

    function _filter_params($params = false) {

        $modelName = Inflector::singularize($this->name);
        $conditions = array();
        if (!empty($this->modelName)) {
            $modelName = $this->modelName;
        }

        $filters = false;

        if (!empty($this->{$modelName}->filters)) {
            $filters = $this->{$modelName}->filters;
        } else {
            $this->set(compact('filters', 'modelName'));
            return $conditions;
        }
        $this->set(compact('filters', 'modelName'));
        $url_params = $this->params['url'];
        unset($url_params['url'], $url_params['page'], $url_params['sort'], $url_params['direction']);
        $params = empty($params) ? $url_params : $params;
        $sessionParams = $this->Session->read("{$modelName}_Filter");
        if ($sessionParams && empty($params))
            $params = $sessionParams;
        elseif (!empty($params)) {
            $this->Session->write("{$modelName}_Filter", $params);
        }

        foreach ($filters as $field => $filters) {
            if (is_numeric($field)) {
                $field = $filters;
                $type = '=';
                $param = $field;
            } elseif (is_string($filters)) {
                $type = $filters;
                $param = $field;
            } elseif (is_array($filters)) {
                $type = empty($filters['type']) ? '=' : $filters['type'];
                $param = $field;
            }
            switch (low($type)) {
                case '=':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field"] = $params[$param];
                    }
                    break;
                case 'like':
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field LIKE"] = "%{$params[$param]}%";
                    }
                    break;
                case 'date_range':
                    $from_param = empty($filters['from']) ? "from" : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param])) {

                        $conditions["$modelName.$field >="] = date('Y-m-d 00:00:00', strtotime($params[$from_param]));
                    }
                    if (!empty($params[$to_param])) {

                        $conditions["$modelName.$field <="] = date('Y-m-d 23:59:59', strtotime($params[$to_param]));
                    }
                    break;
                case 'number_range':
                    $from_param = empty($filters['from']) ? 'from' : $filters['from'];
                    $to_param = empty($filters['from']) ? 'to' : $filters['to'];
                    if (!empty($params[$from_param]) || (isset($params[$param]) && strval($params[$from_param]) === '0')) {
                        $conditions["$modelName.$field >="] = $params[$from_param];
                    }
                    if (!empty($params[$to_param]) || (isset($params[$to_param]) && strval($params[$to_param]) === '0')) {
                        $conditions["$modelName.$field <="] = $params[$to_param];
                    }
                    break;
                default:
                    if (!empty($params[$param]) || (isset($params[$param]) && strval($params[$param]) === '0')) {
                        $conditions["$modelName.$field $type"] = $params[$param];
                    }
                    break;
            }
        }
        return $conditions;
    }

    function admin_delete_field($type, $id, $field, $redirect = false) {
        if (!$redirect)
            $redirect = $this->referer();

        $type = ucfirst($type);

        if (!$id) {
            $this->flashMessage(__('Invalid Item ID', true));
            $this->redirect($redirect);
        } else if (!$field) {
            $this->flashMessage(__('Invalid Field Name', true));
            $this->redirect($redirect);
        } else {

            $this->{$this->modelNames[0]}->id = $id;
            $base_name = $this->{$this->modelNames[0]}->field($field);
            if (!$base_name) {
                $this->flashMessage(__('No ' . $type . ' is found', true));
                $this->redirect($redirect);
            }
            if (!$this->{$this->modelNames[0]}->saveField($field, '')) {
                $this->flashMessage(__('Can\'t Update The Item', true));
                $this->redirect($redirect);
            }



            if (!$this->{$this->modelNames[0]}->{'delete' . $type}($field, $id, $base_name)) {
                $this->flashMessage(__($type . ' Value Has Been Removed From The DB ,But Couldn\'t Delete The ' . $type . ' From the hard disk ', true));
                $this->redirect($redirect);
            }

            $this->flashMessage(__('The ' . $type . ' Has Been Removed', true), 'Sucmessage');
            $this->redirect($redirect);
        }
    }

    function get_snippet($name) {
        $this->loadModel('Snippet');
        $conditions['Snippet.name'] = $name;
        $snippet = $this->Snippet->find('first', array('conditions' => $conditions));
        return $snippet['Snippet']['content'];
    }

    //---------------------
    function load_topmenus() {
        $this->loadModel('Page');

        $conditions = array();
        $conditions['Page.active'] = 1;
        $conditions['Page.add_to_main_menu'] = 1;
        $conditions[] = "(Page.menu_id = 0 OR Page.menu_id IS NULL)";
        $topmenus = $this->Page->find('all', array('conditions' => $conditions, 'order' => 'Page.display_order'));
        //        $this->set('submenu', $this->Page->leftSubmenu);

        $this->set('topmenus', $topmenus);
    }

    function loadLogo() {
        $this->loadModel('Logo');

        $conditions = array();
        $conditions['Logo.active'] = 1;
        $siteLogos = $this->Logo->find('first', array('conditions' => $conditions, 'order' => 'rand()'));
        $this->set("siteLogos", $siteLogos);
    }

}