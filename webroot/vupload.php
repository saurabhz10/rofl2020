<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
 /*
  *
  * 
Copyright (c) 2009 Ronnie Garcia, Travis Nickels

This file is part of Uploadify v1.6.2

Permission is hereby granted, free of charge, to any person obtaining a copy
of Uploadify and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
*/
$SErrors =array();
if (stristr(PHP_OS, 'WIN')) {
 $fmpeg_path='ffmpeg.exe';
} else {
 $fmpeg_path='/usr/bin/ffmpeg';
}



$tmp_dir='';
function UploadFile($input_file_name,$folder='videos',$maxsize_MGB=120,$formats='flv',$file_name='')
	{
		$upload_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR. $folder. DIRECTORY_SEPARATOR  . 'files' . DIRECTORY_SEPARATOR . $folder;
		global $SErrors;
		$temp = $_FILES[$input_file_name]['tmp_name'];
		$file = $_FILES[$input_file_name]['name'];
		$size = $_FILES[$input_file_name]['size'];

		$tmp_parts= explode('.',$file);
		$ext=$tmp_parts[sizeof($tmp_parts)-1];
		
		if(strpos(" ".strtolower($formats),strtolower($ext))<1)
		{
			$SErrors[]=  " Invalid file format";
			return false;
		}

		if(!$file_name)
		$file_name=substr(md5(rand(1,213213212)),1,5)."_".$_FILES[$input_file_name]['name'];
		else
		$file_name=str_replace('..','.',$file_name.".".$ext);

		$file_name=str_replace(array('\'','"',' ','`'),'_',$file_name);
		$GLOBALS['tmp_dir']=dirname($temp);

		$FileName =  $GLOBALS['tmp_dir'] . DIRECTORY_SEPARATOR .$file_name;

		$FileName=str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR,$FileName);


		$type = gettype($size);
		if(strcmp($type, "NULL") == 0 || $size > (1024 * 1024 * $maxsize_MGB))
		{
			$SErrors[]=  "Sorry,  the maximum size of the file to upload has been limited to $maxsize_MGB MB.";
			return false;
		}

		if($size == 0)
		{
			$SErrors[]= "Please select the file to upload.";
			return false;
		}
		

		move_uploaded_file( $temp, $FileName);


		// encodeing may take a few minutes.
		set_time_limit(99999);

		// disable output buffering so we can send the encoding progress to the browser.


		ob_implicit_flush();

		return $file_name;


	}
	/**********************************************************************************\
	 * ********************************************************************************\
	 **********************************************************************************/
set_time_limit(99999);
ini_set("memory_limit","2542M");
$params = $_GET['params'];
$params = str_replace(substr($params, strpos($params,'?')),'',$params);
$params = explode(';', $params);
$folder = $params[0];
$formats= $params[1];
$size = $params[2];
$file_name = $params[3]?$params[3]:'';

if(!($file_name= UploadFile('Filedata',$folder,$size,$formats,$file_name)))
{
	echo "0";
	foreach ($SErrors as $Error)
	echo "<p>$Error</p>";
	
}
else 
{
    
	$full_file_name = $GLOBALS['tmp_dir'] . DIRECTORY_SEPARATOR .$file_name ;
	$convert_result = flv_convert($full_file_name);
        //print_r($convert_result);
	if(!empty($convert_result['error']))
	echo "0".$convert_result['error'];
	else
	echo "1".$convert_result['file_name'];
}

 function getVideoInfo($file){
		$ffmpeg = $GLOBALS['fmpeg_path'];
		
		$cmd = "$ffmpeg -i $file 2>&1";
		$second = 5;
		$duration = 0;
		$aspect = '480x360';
		$result = `$cmd`;
		if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', $result, $times)){
			$duration = $times[1];
			$total = ($times[2] * 3600) + ($times[3] * 60) + $times[4];
			$second = rand(0, $total);
		}
		if (preg_match('/, ((\d+)x(\d+))/', $result, $aspects)){
			$aspect = "{$aspects[2]}x{$aspects[3]}";
		}
		
		
		return array(
			'second' => $second,
			'duration' => $duration,
			'aspect' => $aspect
		);
	}
	
function flv_convert($source_file_name, $target_file_name = false, $options = array()){
		
		
		//Setting defaults
		set_time_limit(99999);
		$info = getVideoInfo($source_file_name);
        
		$aspects = $info['aspect'];
		$dims = explode('x', $aspects);
		
		
		$width = 320;
        $height = 240;
		$duration = $info['duration'];
		if (abs(intval((intval($dims[0])/intval($dims[1])) * 100)  - intval((16/9) * 100))<4){
			
			$width = 320;
			$height = 180;
		}
		else {
			
		}

		$dir = dirname($source_file_name);
		$base = basename($source_file_name);
		$defaults = array(
			'thumb' => $dir. DIRECTORY_SEPARATOR ."thumb_$base.jpg",
			'preview' => "$source_file_name.jpg",
			'width' => $width,
			'height' => $height,
			'thumb_width' => $GLOBALS['thumb_image_width'],
			'thumb_height' => $GLOBALS['thumb_image_height'],
			'image_second' => $info['second'],
			'bitrate' => '64kb',
		);

		$options = array_merge($defaults, $options);
		$target_file_name = $target_file_name? $target_file_name : "$source_file_name.flv";
		$return = array();
		$return['error'] = false;
		$return['file_name'] = false;
		$return['preview'] = false;
		$return['thumb'] = false;

		//Preparing for command execute
		$ffmpeg = $GLOBALS['fmpeg_path'];
		$descriptor_spec = array(
			array('pipe', 'r'), array('pipe', 'w'), array('pipe', 'w')
		);
		$pipes = array();

		//Converting to flv command
                unlink($target_file_name);
		$cmd = "$ffmpeg -i $source_file_name -ar 44100 -s {$options['width']}x{$options['height']} -qmin 3 -qmax 6  -f flv $target_file_name";
		file_put_contents('/tmp/debug.log', $cmd);
		
		//die($cmd);
		//echo "<li>Encoding the file: <code>$cmd</code></li>";
		$exec = proc_open($cmd,$descriptor_spec,$pipes);
		
		fclose($pipes[0]);fclose($pipes[1]);fclose($pipes[2]);
		$convert_result = proc_close($exec);
		if ($convert_result != 0){
			$return['error'] = 'Failed to convert the file to a webformat video';
			return $return;
		} else {
			$return['file_name'] = basename($target_file_name);
			$return['width'] = $width;
			$return['height'] = $height;
			$return['duration'] = $duration;
		}

		//Creating preview image
		$cmd = "$ffmpeg -i $source_file_name -s {$options['width']}x{$options['height']} -an -deinterlace -vframes 1 -ss {$options['image_second']} -qmin 3 -qmax 6 -f mjpeg -vcodec mjpeg {$options['preview']}";
		//echo "<li>Extracting preview image: <code>$cmd</code></li>";
		$exec = proc_open($cmd, $descriptor_spec, $pipes);
		fclose($pipes[0]);fclose($pipes[1]);fclose($pipes[2]);
		$preview_result = proc_close($exec);
		if ($preview_result == 0){
			$return['preview'] = basename($options['preview']);
		}

		//Creating thumb image
		$cmd = "$ffmpeg -i $source_file_name -s {$options['thumb_width']}x{$options['thumb_height']} -an -deinterlace -vframes 1 -ss {$options['image_second']} -qmin 3 -qmax 6 -f mjpeg -vcodec mjpeg {$options['thumb']}";
		//echo "<li>Extracting thumbnail: <code>$cmd</code></li>";
		$exec = proc_open($cmd, $descriptor_spec, $pipes);
		fclose($pipes[0]);fclose($pipes[1]);fclose($pipes[2]);
		$thumb_result = proc_close($exec);
		if ($thumb_result == 0){
			$return['thumb'] = basename($options['thumb']);
		}
		return $return;
	}

?>