<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
 /*
  *
  *
Copyright (c) 2009 Ronnie Garcia, Travis Nickels

This file is part of Uploadify v1.6.2

Permission is hereby granted, free of charge, to any person obtaining a copy
of Uploadify and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
*/
$SErrors =array();
$tmp_dir='';
function UploadFile($input_file_name,$folder='videos',$maxsize_MGB=120,$formats='flv',$file_name='')
	{
		$upload_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR. $folder. DIRECTORY_SEPARATOR  . 'files' . DIRECTORY_SEPARATOR . $folder;
		global $SErrors;
		$temp = $_FILES[$input_file_name]['tmp_name'];
		$file = $_FILES[$input_file_name]['name'];
		$size = $_FILES[$input_file_name]['size'];

		$tmp_parts= explode('.',$file);
		$ext=$tmp_parts[sizeof($tmp_parts)-1];

		if(strpos(" ".strtolower($formats),strtolower($ext))<1)
		{
			$SErrors[]=  " Invalid file format";
			return false;
		}

		if(!$file_name)
		$file_name=substr(md5(rand(1,213213212)),1,5)."_".$_FILES[$input_file_name]['name'];
		else
		$file_name=str_replace('..','.',$file_name.".".$ext);

		$file_name=str_replace(array('\'','"',' ','`'),'_',$file_name);
		$GLOBALS['tmp_dir']=dirname($temp);

		$FileName =  $GLOBALS['tmp_dir'] . DIRECTORY_SEPARATOR .$file_name;

		$FileName=str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR,$FileName);


		$type = gettype($size);
		if(strcmp($type, "NULL") == 0 || $size > (1024 * 1024 * $maxsize_MGB))
		{
			$SErrors[]=  "Sorry,  the maximum size of the file to upload has been limited to $maxsize_MGB MB.";
			return false;
		}

		if($size == 0)
		{
			$SErrors[]= "Please select the file to upload.";
			return false;
		}

                
		move_uploaded_file( $temp, $FileName);


		// encodeing may take a few minutes.
		set_time_limit(1800);

		// disable output buffering so we can send the encoding progress to the browser.


		ob_implicit_flush();

		return $file_name;


	}
	/**********************************************************************************\
	 * ********************************************************************************\
	 **********************************************************************************/
$params = $_GET['params'];
$params = str_replace(substr($params, strpos($params,'?')),'',$params);
$params = explode(';', $params);
$folder = $params[0];
$formats= $params[1];
$size = $params[2];
$file_name = $params[3]?$params[3]:'';

if(!($file_name= UploadFile('Filedata',$folder,$size,$formats,$file_name)))
{
	echo "0";
	foreach ($SErrors as $Error)
	echo "<p>$Error</p>";

}
else
{
	$full_file_name = $GLOBALS['tmp_dir'] . DIRECTORY_SEPARATOR .$file_name ;


	echo "1".$file_name;
}




?>