<?php
/* SVN FILE: $Id: app_model.php 7945 2008-12-19 02:16:01Z gwoo $ */

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision: 7945 $
 * @modifiedby    $LastChangedBy: gwoo $
 * @lastmodified  $Date: 2008-12-18 18:16:01 -0800 (Thu, 18 Dec 2008) $
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppModel extends Model {



   /**
     * function to get recursive data for only one assoc class in hasMany or hasAndBelongsToMany assoicated , and remove all other associations in this data retreival , to reduce the amount of associated data fetched, but all your associations are on the first level of recursion.
     * @param string $className calssName to get the associated items for it only
     * @param array $params same as regular find array params (conditions, order ...etc (
     * @param array $relation_overwright to overwright some paramteres on the assoc array , e.g to overwright the condition paramter of the realation set $relation_overright=array('conditions'=>'1=1') that equevelant to $this->asoocName[className][conditions]='1=1'
     * @return array of results
     */
    function findOnlyMany($className, $params=null,$relation_overwright=null)
    {

        if((!is_array($this->hasAndBelongsToMany)||!key_exists($className, $this->hasAndBelongsToMany))&& (!is_array($this->hasMany)||!key_exists($className, $this->hasMany)))
        {
            trigger_error(sprintf(__('Invalid hasAndBelongsToMany/hasMany "%s"', true),$className), E_USER_WARNING);
            return false;
        }
        else
        {
            if(is_array($this->hasAndBelongsToMany)&&key_exists($className, $this->hasAndBelongsToMany))
            $assocName= 'hasAndBelongsToMany' ;
            else if(is_array($this->hasMany)&&key_exists($className, $this->hasMany))
            $assocName= 'hasMany' ;
            $thmp_old_assoc = $old_assoc=  $this->{$assocName}[$className];

            if(is_array($relation_overwright))
            foreach ($relation_overwright as $param=>$value)
            {
                $thmp_old_assoc[$param]=$value;
            }

            unset($this->{$assocName}[$className]);

            $this->unbindModel(array('hasMany'=>array_keys($this->hasMany),'hasAndBelongsToMany'=>array_keys($this->hasAndBelongsToMany)));

            $this->{$assocName}[$className]=$thmp_old_assoc;


            $recursive=1;
            $return = $this->find('all',$params);
            $this->{$assocName}[$className]=$old_assoc;
            return $return;
        }
    }
}
?>