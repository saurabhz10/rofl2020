<?php

class ContactsController extends AppController {

    var $name = 'Contacts';
    var $helpers = array('Html', 'Form');
    var $components = array('Session', 'Email');

    function admin_index() {
        $this->Contact->recursive = 0;
        $conditions = $this->_filter_params();
        $this->paginate['Contact']['order'] = 'Contact.id desc';

        $this->set('contacts', $this->paginate('Contact', $conditions));

        $this->set("contactTypes", array("contact" => "contact", "gallery" => "gallery", "course" => "course"));
    }

    //--------------------------------
    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Contact.', true));
            $this->redirect(array('action' => 'index'));
        }

        $contact = $this->Contact->read(null, $id);
        $this->set('contact', $contact);
    }

    //--------------------------------
    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('contact', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('contact', true);
        if (count($id) > 1) {
            $module_name = __('contacts', true);
        }
        $contacts = $this->Contact->find('all', array('conditions' => array('Contact.id' => $id)));
        if (empty($contacts)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Contact->deleteAll(array('Contact.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('contacts', $contacts);
    }

    function admin_delete_multi() {


        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('contact', true)));
            $this->redirect(array('action' => 'index'));
        }

        $module_name = __('contact', true);
        if (count($id) > 1) {
            $module_name = __('contacts', true);
        }
        if ($this->Contact->deleteAll(array('Contact.id' => $_POST['ids']))) {
            $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action' => 'index'));
        }
    }

    //--------------------------------

    function contactus($id = null) {
        $this->pageTitle = 'Contact Us';
        $this->contactTitle = 'Contact us';
        $to = $this->config['txt.admin_mail'];
        $this->set('sitename', $this->config['txt.site_name']);
        $this->set('contact_snippet', $this->get_snippet("contact-us"));
        if (!empty($this->data)) {
            $this->Contact->set($this->data);
            if ($this->Contact->save($this->data)) {
                $this->Email->to = $to;
                $subject = "{$this->config['txt.site_name']}: New Contact us";
                $name = $this->data['Contact']['first_name'] . " " . $this->data['Contact']['last_name'];
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'contact';
                $this->Email->from = $name . '<' . $this->data['Contact']['email'] . '>';
                $this->Email->subject = "Richard O'Farrell - contact us enquiry";
                $this->set('contact_data', $this->data['Contact']);


                if ($this->Email->send()) {
                    $this->data = array();
                    $this->flashMessage(__('Your message has been sent.', true), 'flashMessage Sucmessage');
                } else {
                    $this->flashMessage(__('can\'t send email', true), 'flashMessage Errormessage');
                }
            } else {
                $this->flashMessage(__('Your message could not be saved, please check for input errors and try again.', true), 'flashMessage Errormessage');
            }
        }
    }

    function ajax_contact() {
        Configure::write('debug', 0);
//        $this->layout = '';
        $this->layout = $this->autoRender = false;
        $message = array();
        $to = $this->config['txt.admin_mail'];
        $this->set('sitename', $this->config['txt.site_name']);
        if (!empty($this->data)) {
            $this->Contact->set($this->data);
            if ($this->Contact->save($this->data)) {
                $this->Email->to = $to;
                $subject = $this->data['Contact']['subject'];
//                $subject = "{$this->config['txt.site_name']}: ";
//                $subject .= ($this->data['Contact']['subject']) ? $this->data['Contact']['subject'] : "Contact Message";
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'contact';
                $this->Email->from = $this->data['Contact']['name'] . '<' . $this->data['Contact']['email'] . '>';
                $this->Email->subject = $subject;
                $this->set('contact_data', $this->data['Contact']);
                if ($this->Email->send($this->data['Contact']['message'])) {
                    $this->data = array();
                    $this->flashMessage(__('Your message has been sent.', true), 'flashMessage Sucmessage');
                    $message = array('message' => 'Your message has been sent.', 'success' => 1);
                    //$this->render('ajax/contact_us');
                } else {
                    $this->flashMessage(__('can\'t send email', true), 'flashMsg errorMsg');
                    $message = array('error' => 'can\'t send email', 'success' => 0);
                    $this->render('ajax/contact_us');
                }
            } else {
                $this->flashMessage(__('Your message could not be saved, please check for input errors and try again.', true), 'flashMessage Errormessage');
                $message = array('error' => $this->Contact->validationErrors, 'success' => 0);
                $this->render('ajax/contact_us');
            }
        }
        header('Content-type: application/javascript; charset=utf-8');
        die(json_encode($message));

        exit();
    }

}

?>
