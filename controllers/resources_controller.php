<?php
class ResourcesController extends AppController {

	var $name = 'Resources';

	/**
	 * @var Resource
	 */
	var $Resource;
	var $helpers = array('Html', 'Form');

	function admin_index() {
		$this->Resource->recursive = 0;
		$conditions = $this->_filter_params();
		$this->set('resources', $this->paginate('Resource', $conditions));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->flashMessage(__(sprintf (__('Invalid %s', true), __('resource', true)),true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('resource', $this->Resource->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Resource->create();
			if ($this->Resource->save($this->data)) {
				$this->flashMessage(sprintf (__('The %s has been saved', true), __('resource',true)), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('resource',true)));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->flashMessage(sprintf (__('Invalid %s.', 'resource',true)));
			$this->redirect(array('action'=>'index'));
		}
		$resource = $this->Resource->read(null, $id);
		if (!empty($this->data)) {
			if ($this->Resource->save($this->data)) {
				$this->flashMessage(sprintf (__('The %s  has been saved', true), __('resource',true)), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(sprintf (__('The %s could not be saved. Please, try again', true), __('resource',true)));
			}
		}
		if (empty($this->data)) {
			$this->data = $resource;
		}
		$this->render('admin_add');
	}

	function admin_delete($id = null) {
		if (empty($id) && !empty ($_POST['ids'])) {
			$id = $_POST['ids'];
		 } 
 		if (!$id && empty ($_POST)) {
			$this->flashMessage(sprintf (__('Invalid id for %s', true), __('resource',true)));
			$this->redirect(array('action'=>'index'));
		}
		$module_name= __('resource', true);
		if(count($id) > 1){
			$module_name= __('resources', true);
		 } 
		$resources = $this->Resource->find('all',array('conditions'=>array('Resource.id'=>$id)));
		if (empty($resources)){
			$this->flashMessage(sprintf(__('%s not found', true), $module_name));
			$this->redirect($this->referer(array('action' => 'index'), true));
		}
		if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
			if ($_POST['submit_btn'] == 'yes' && $this->Resource->deleteAll(array('Resource.id'=>$_POST['ids']))) {
				$this->flashMessage(sprintf (__('%s deleted', true), $module_name), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			}
			else{
				$this->redirect(array('action'=>'index'));
			}
		}
		$this->set('resources',$resources);
	}
}
?>