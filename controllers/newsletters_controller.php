<?php

class NewslettersController extends AppController {

    var $name = 'Newsletters';

    /**
     * @var Newsletter
     */
    var $Newsletter;
    var $helpers = array('Html', 'Form');

    function admin_export() {
        $this->layout = false;
        $inflector = new Inflector;
        $ModelName = $inflector->singularize($this->name);
        $emails = $this->Newsletter->find('all', array('fields' => array('email', 'created'), 'callbacks' => false));
        $this->set('items', $emails);
        $schema = array('Email', 'Date');
        $schema = array_map(array('Inflector', 'humanize'), $schema);

        $this->set('ModelName', $ModelName);
        $this->set('schema', $schema);
    }

    function admin_index() {
        $this->Newsletter->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('newsletters', $this->paginate('Newsletter', $conditions));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Newsletter->create();
            if ($this->Newsletter->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s has been saved', true), __('newsletter', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('newsletter', true)));
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(sprintf(__('Invalid %s.', 'newsletter', true)));
            $this->redirect(array('action' => 'index'));
        }
        $newsletter = $this->Newsletter->read(null, $id);
        if (!empty($this->data)) {
            if ($this->Newsletter->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s  has been saved', true), __('newsletter', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('newsletter', true)));
            }
        }
        if (empty($this->data)) {
            $this->data = $newsletter;
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('newsletter', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('newsletter', true);
        if (count($id) > 1) {
            $module_name = __('newsletters', true);
        }
        $newsletters = $this->Newsletter->find('all', array('conditions' => array('Newsletter.id' => $id)));
        if (empty($newsletters)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Newsletter->deleteAll(array('Newsletter.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('newsletters', $newsletters);
    }

    function save_subscriber($data) {
        $required = array('rule' => 'notEmpty', 'message' => "Required", 'required' => true);

        $this->validate['security_code'] = array($required, array('rule' => 'chkSecurityCode', 'message' => 'Invalid code'));
        $this->validate['email'] = array('rule' => 'isunique', 'message' => 'This Email is already exists in our newsletter');
        $this->set($data);
        if ($this->validates()) {
            if ($this->save($data, array('validate' => false))) {
                return $this->getLastInsertID();
            }
        }
        debug();
        return false;
    }

    function add() {
        Configure::write('debug', 0);
        $this->layout = $this->autoRender = false;
        $message = array();
        if (!empty($this->data)) {
            $this->Newsletter->validate['email'] = array(
                array('rule' => 'isunique', 'message' => 'This Email is already exists in our newsletter'),
                array('rule' => 'email', 'allowEmpty' => false, 'message' => 'vaild email required')
            );

            $data = $this->data;
            $this->Newsletter->set($data);
            if ($this->Newsletter->validates()) {
                if ($this->Newsletter->save($data)) {
                    $message = array('message' => 'thanks, your address has been added', 'success' => 1);
                }
            } else {
                $message = array('error' => $this->Newsletter->validationErrors, 'success' => 0);
            }
        }
        header('Content-type: application/javascript; charset=utf-8');
        print sprintf('%s(%s);', $_GET['callback'], json_encode($message));

        exit();
    }

    //------------------------------------------------
    function unsubscribe($code = false) {
        if (!$code) {
            $this->flashMessage('Invalid URL');
        } else {

            $email = base64_decode(base64_decode($code));

            if ($this->Newsletter->deleteAll(compact('email'))) {
                $this->flashMessage('You were unsubscribed successfully from our newletter.', 'Sucmessage');
                $this->loadModel('NewsJob');
                $this->NewsJob->deleteAll(array('NewsJob.email' => $email, 'is_sent' => 0));
            } else {
                $this->flashMessage('Couldnot unsubscribe. Please try again later.');
            }
        }
        $this->redirect('/');
    }

}

?>