<?php

class TabsController extends AppController {

    var $name = 'Tabs';

    /**
     * @var Tab
     */
    var $Tab;
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->Tab->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('tabs', $this->paginate('Tab', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('tab', true)), true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('tab', $this->Tab->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Tab->create();
            if ($this->Tab->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s has been saved', true), __('tab', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('tab', true)));
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(sprintf(__('Invalid %s.', 'tab', true)));
            $this->redirect(array('action' => 'index'));
        }
        $tab = $this->Tab->read(null, $id);
        if (!empty($this->data)) {
            if ($this->Tab->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s  has been saved', true), __('tab', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('tab', true)));
            }
        }
        if (empty($this->data)) {
            $this->data = $tab;
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('tab', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('tab', true);
        if (count($id) > 1) {
            $module_name = __('tabs', true);
        }
        $tabs = $this->Tab->find('all', array('conditions' => array('Tab.id' => $id)));
        if (empty($tabs)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Tab->deleteAll(array('Tab.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('tabs', $tabs);
    }

}

?>