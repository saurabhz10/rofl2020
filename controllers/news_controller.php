<?php

/**
 * @property News News
 * 
 */class NewsController extends AppController {

    var $name = 'News';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->News->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('news', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid News.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('news', $this->News->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->News->create();
            if ($this->News->save($this->data)) {
                $this->flashMessage(__('The News has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The News could not be saved. Please, try again.', true));
            }
        }

        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());
        $this->set('image_settings', $this->News->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid News', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->News->save($this->data)) {
                $this->flashMessage(__('The News has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The News could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->News->read(null, $id);
        }

        $this->set('image_settings', $this->News->getImageSettings());
        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for News', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->News->del($id)) {
//            $this->flashMessage(__('News deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for News', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->News->deleteAll(array('News.id' => $_POST['ids']))) {
//            $this->flashMessage(__('News items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('news', true)), true));
        }
        $module_name = __('news', true);
        if (count($id) > 1) {
            $module_name = __('news', true);
        }
        $news = $this->News->find('all', array('conditions' => array('News.id' => $id)));
        if (empty($news)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->News->deleteAll(array('News.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('The %s has been deleted', true), __('news', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__(sprintf(__('Error in deleting %s', true), __('news', true)), true));
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('news', $news);
    }

    function index() {
        $conditions = array('active' => 1);
        $this->pageTitle = 'News';
        $this->paginate['News']['order'] = 'post_date desc';
        $this->paginate['News']['limit'] = 5;


        $news = $this->paginate('News', $conditions);
        $this->set('news', $news);
        $this->set('snippet', $this->get_snippet('news_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $news = $this->News->find('first', array('conditions' => array('News.permalink' => $permalink, 'News.active' => 1)));

        if (!$news) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'News - ' . $news['News']['title'];

        $this->set('news', $news);
    }

}

?>