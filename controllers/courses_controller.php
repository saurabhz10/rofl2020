<?php

/**
 * @property Course Course
 * 
 */class CoursesController extends AppController {

    var $name = 'Courses';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Course->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('courses', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Course.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('courses', $this->Course->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Course->create();
            $this->data["Course"]["course_details"] = implode(",", $this->data["Course"]["course_details"]);
            if ($this->Course->save($this->data)) {
                $this->flashMessage(__('The Course has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Course could not be saved. Please, try again.', true));
            }
        }

        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());
        $this->set('image_settings', $this->Course->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Course', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
//            debug($this->data);die;
            $this->data["Course"]["course_details"] = implode(",", $this->data["Course"]["course_details"]);
            if ($this->Course->save($this->data)) {
                $this->flashMessage(__('The Course has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Course could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Course->read(null, $id);
        }

        $this->set('image_settings', $this->Course->getImageSettings());
        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Course', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Course->del($id)) {
//            $this->flashMessage(__('Course deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Course', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Course->deleteAll(array('Course.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Course items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }


    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('course', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('course', true);
        if (count($id) > 1) {
            $module_name = __('courses', true);
        }
        $courses = $this->Course->find('all', array('conditions' => array('Course.id' => $id)));
        if (empty($courses)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Course->deleteAll(array('Course.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('courses', $courses);
    }

    function index() {
        $conditions = array('active' => 1);
        $this->pageTitle = 'Course';
        $this->paginate['Course']['order'] = 'display_order ASC';
        $this->paginate['Course']['limit'] = 5;


        $courses = $this->paginate('Course', $conditions);
        $this->set('courses', $courses);
        $this->set('snippet', $this->get_snippet('education_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $courses = $this->Course->find('first', array('conditions' => array('Course.permalink' => $permalink, 'Course.active' => 1)));

        if (!$courses) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'Course - ' . $courses['Course']['title'];

        $this->set('courses', $courses);
    }

}

?>