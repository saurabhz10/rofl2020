<?php

/**
 * @property MailLog MailLog
 * 
 */
class MailLogsController extends AppController {

	var $name = 'MailLogs';
	var $helpers = array('Html', 'Form');

	
	function admin_index() {
		$this->MailLog->recursive = 0;
		$this->set('mailLogs', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid MailLog.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('mailLog', $this->MailLog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MailLog->create();
			if ($this->MailLog->save($this->data)) {
				$this->flashMessage(__('The MailLog has been saved', true), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(__('The MailLog could not be saved. Please, try again.', true));
			}
		}
		$groupsContacts = $this->MailLog->GroupsContact->find('list');
		$newsMessages = $this->MailLog->NewsMessage->find('list');
		$this->set(compact('groupsContacts', 'newsMessages'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->flashMessage(__('Invalid MailLog', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->MailLog->save($this->data)) {
				$this->flashMessage(__('The MailLog has been saved', true), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(__('The MailLog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MailLog->read(null, $id);
		}
		$this->render('admin_add');
		$groupsContacts = $this->MailLog->GroupsContact->find('list');
		$newsMessages = $this->MailLog->NewsMessage->find('list');
		$this->set(compact('groupsContacts','newsMessages'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->flashMessage(__('Invalid id for MailLog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MailLog->del($id)) {
			$this->flashMessage(__('MailLog deleted', true), 'Sucmessage');
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_delete_multi() {
		if (empty($_POST['ids'])||!is_array($_POST['ids'])) {
			$this->flashMessage(__('Invalid ids for MailLog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MailLog->deleteAll(array('MailLog.id'=>$_POST['ids'])))  {
			$this->flashMessage(__('MailLog items deleted', true), 'Sucmessage');
			$this->redirect(array('action'=>'index'));
		}
		else  {
			$this->flashMessage(__('Unknown error', true));
			$this->redirect(array('action'=>'index'));
		}
	}

}
?>