<?php

class GalleriesController extends AppController {

    var $name = 'Galleries';

    /**
     * @var Gallery
     */
    var $Gallery;
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->Gallery->recursive = 1;
        $conditions = $this->_filter_params();
        $this->paginate['Gallery']['order'] = 'Gallery.id desc';
        $this->__form_common();

        $this->set('galleries', $this->paginate('Gallery', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('gallery', true)), true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('gallery', $this->Gallery->read(null, $id));
    }

    function view($id = null) {

        $this->loadModel('GalleryImage');
        $this->paginate = array(
            'GalleryImage' => array('limit' => 10, 'order' => array('GalleryImage.display_order asc'),),
        );
        $conditions = array('gallery_id' => $id, "GalleryImage.active" => 1);
        if (!$id) {

//            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('gallery', true)), true));
//            $this->redirect(array('action' => 'index'));
        }
        $gallery = $this->Gallery->read(null, $id);

        $galleies = $this->Gallery->find("all", array("order" => array("display_order asc")));
        if (empty($gallery) && !empty($galleies))
            $gallery = $galleies[0];
        $this->pageTitle = $gallery['Gallery']['title'];

        $this->set('galleryimages', $this->paginate('GalleryImage', $conditions));
        $this->set('gallery', $gallery);


        $this->set('galleies', $galleies);
    }

    function admin_add() {
        $this->loadModel('GalleryImage');

        if (!empty($this->data)) {
            $Errors = '';
            $this->Gallery->create();
            if ($this->Gallery->save($this->data)) {

                if (!empty($this->data['GalleryImage'])) {
                    // print_r($this->data['GalleryImage']);

                    foreach ($this->data['GalleryImage'] as $i => $item) {

                        if (!empty($this->data['GalleryImage'][$i]['image']) && $this->data['GalleryImage'][$i]['image']['size'] > 0) {
                            $this->data['GalleryImage'][$i]['gallery_id'] = $this->Gallery->id;

                            if (empty($this->data['GalleryImage'][$i]['display_order'])) {
                                $this->data['GalleryImage'][$i]['display_order'] = $i;
                            }


                            $this->GalleryImage->create();
                            $data['GalleryImage'] = $this->data['GalleryImage'][$i];

                            if (!$this->GalleryImage->save($data['GalleryImage'], false)) {
                                $Errors.="Can't Upload for " . $data['GalleryImage']['title'] . " " . (is_array($data['GalleryImage']['image']) ? $data['GalleryImage']['image']['name'] : $data['GalleryImage']['image']['name']) . ":<br/>" . implode("<br/>", $this->GalleryImage->validationErrors) . "<br/><br/>";
                            }
                        } else
                            unset($this->data['GalleryImage'][$i]);
                    }
                }

                if (empty($Errors)) {
                    $this->flashMessage(sprintf(__('The %s has been saved', true), __('gallery', true)), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->flashMessage($Errors);
                    $this->redirect(array('action' => 'edit', $this->Gallery->id));
                }
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('gallery', true)));
            }
        }
        
        $this->__form_common();
        
    }

    function admin_edit($id = null) {
   
        $this->loadModel('GalleryImage');
        $Errors = "";

        if (!$id && empty($this->data)) {
            $this->flashMessage(sprintf(__('Invalid %s.', 'gallery', true)));
            $this->redirect(array('action' => 'index'));
        }
        $gallery = $this->Gallery->read(null, $id);
        if (!empty($this->data)) {
            if ($this->Gallery->save($this->data)) {

                if (!empty($this->data['GalleryImage'])) {
                    $this->GalleryImage->recursive = -1;
                    $_gallery_images = $this->GalleryImage->find('all', array('conditions' => array('GalleryImage.gallery_id' => $id)));

                    $gallery_images_ids = array();
                    foreach ($_gallery_images as $pag) {
                        $gallery_images_ids[] = $pag['GalleryImage']['id'];
                    }
                    $delete_pages_array = array();
                    foreach ($this->data['GalleryImage'] as $i => $item) {
                        $posted_ids[] = $item['id'];
                    }
                    $delete_pages_array = array_diff($gallery_images_ids, $posted_ids);
                    $this->GalleryImage->deleteAll(array('GalleryImage.id' => $delete_pages_array));
                } else {
                    $this->GalleryImage->deleteAll(array('GalleryImage.property_id' => $id));
                }


                if (!empty($this->data['GalleryImage'])) {
                   //  Configure::write('debug', 2); 
                   // debug($this->data['GalleryImage']);
                   // die;

//                     debug($this->data);
//                      die;
                    foreach ($this->data['GalleryImage'] as $i => $item) {
                        $data = array();
                        $this->data['GalleryImage'][$i]['gallery_id'] = $this->Gallery->id;
                        $data['GalleryImage'] = $this->data['GalleryImage'][$i];
                        if ($this->data['GalleryImage'][$i]['image']['size'] != 0) {
//                            unset($this->data['GalleryImage'][$i]['image']);
                            //else {
                            $this->data['GalleryImage'][$i]['gallery_id'] = $this->Gallery->id;

                            if (empty($this->data['GalleryImage'][$i]['display_order'])) {
                                $this->data['GalleryImage'][$i]['display_order'] = $i;
                            }



                            $this->GalleryImage->create();
                            $data['GalleryImage'] = $this->data['GalleryImage'][$i];
                          

                            if (!$this->GalleryImage->save($data['GalleryImage'], false))
                                $Errors.="Can't Upload for " . $data['GalleryImage']['title'] . " " . (is_array($data['GalleryImage']['image']) ? $data['GalleryImage']['image']['name'] : $data['GalleryImage']['image']['name']) . ":<br/>" . implode("<br/>", $this->GalleryImage->validationErrors) . "<br/><br/>";
                        }else{
                            $save = $this->GalleryImage->save($data['GalleryImage'], false);
                
                        }

                        // }
//                        else
//                            unset($this->data['GalleryImage'][$i]);
                    }
                }


 
               
                if (empty($Errors)) {
                    $this->flashMessage(sprintf(__('The %s  has been saved', true), __('gallery', true)), 'Sucmessage');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->flashMessage($Errors);
                    $this->redirect(array('action' => 'edit', $this->Gallery->id));
                }
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('gallery', true)));
            }
        }
        if (empty($this->data)) {
            $this->data = $gallery; 
        }
//        $this->set('image_settings', $this->Gallery->getImageSettings());
$this->__form_common();
        $galleryimage = $this->GalleryImage->find('all', array('conditions' => array('GalleryImage.gallery_id' => $id)));

        $this->set('image_settings', $this->GalleryImage->getImageSettings());
        $this->set('galleryimage', $galleryimage);
        $this->render('admin_add');
    }

    function __form_common() {
        $this->loadModel('Category');
        $categories = $this->Category->find('list',array('conditions' => array('Category.active' => 1)));
        $this->set(compact('categories'));
        
    }
    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('gallery', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('gallery', true);
        if (count($id) > 1) {
            $module_name = __('galleries', true);
        }
        $galleries = $this->Gallery->find('all', array('conditions' => array('Gallery.id' => $id)));
        if (empty($galleries)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Gallery->deleteAll(array('Gallery.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('galleries', $galleries);
    }

}

?>
