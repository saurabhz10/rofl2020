<?php

/**
 * @property Testimonial Testimonial
 * 
 */class TestimonialsController extends AppController {

    var $name = 'Testimonials';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Testimonial->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('testimonials', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Testimonial.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('testimonials', $this->Testimonial->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Testimonial->create();
            if ($this->Testimonial->save($this->data)) {
                $this->flashMessage(__('The Testimonial has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Testimonial could not be saved. Please, try again.', true));
            }
        }

        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());
        $this->set('image_settings', $this->Testimonial->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Testimonial', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Testimonial->save($this->data)) {
                $this->flashMessage(__('The Testimonial has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Testimonial could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Testimonial->read(null, $id);
        }

        $this->set('image_settings', $this->Testimonial->getImageSettings());
        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Testimonial', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Testimonial->del($id)) {
//            $this->flashMessage(__('Testimonial deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Testimonial', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Testimonial->deleteAll(array('Testimonial.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Testimonial items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }


    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('testimonial', true)), true));
        }
        $module_name = __('testimonial', true);
        if (count($id) > 1) {
            $module_name = __('testimonials', true);
        }
        $testimonials = $this->Testimonial->find('all', array('conditions' => array('Testimonial.id' => $id)));
        if (empty($testimonials)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Testimonial->deleteAll(array('Testimonial.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('The %s has been deleted', true), __('testimonial', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__(sprintf(__('Error in deleting %s', true), __('testimonial', true)), true));
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('testimonials', $testimonials);
    }

    function index() {
        $conditions = array('active' => 1);
        $this->pageTitle = 'Testimonial';
        $this->paginate['Testimonial']['order'] = 'display_order ASC';
        $this->paginate['Testimonial']['limit'] = 5;


        $testimonials = $this->paginate('Testimonial', $conditions);
        $this->set('testimonials', $testimonials);
        $this->set('snippet', $this->get_snippet('testimonials_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $testimonials = $this->Testimonial->find('first', array('conditions' => array('Testimonial.permalink' => $permalink, 'Testimonial.active' => 1)));

        if (!$testimonials) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'Testimonial - ' . $testimonials['Testimonial']['title'];

        $this->set('testimonials', $testimonials);
    }

}

?>