<?php

class NewsMessagesController extends AppController {

    var $name = 'NewsMessages';
    var $helpers = array('Html', 'Form', 'Fck');
    var $components = array('Session', 'Email');
    var $titleAlias = 'Newsletter Messages';

    function admin_index() {
        $cond = $this->_filter_params();
        $this->NewsMessage->recursive = 0;
        $this->set('newsMessages', $this->paginate('NewsMessage', $cond));


        $news_messages = $this->NewsMessage->find('list');
        $this->set('news_messages', $news_messages);
        $this->set('h2', 'Newsletter Messages');
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid Newsletter Message.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->layout = 'newsletters';


        $this->set('newsMessage', $this->NewsMessage->read(null, $id));
        //$this->set('content_for_layout', $this->NewsletterTemplate->read(null, $id));
        $this->set('h2', 'View Newsletter Message');
        $this->layout = '';
    }

    //----------------------------------------
    function admin_add($template_id = null) {
        $this->loadModel('NewsMessageFile');
        if (!empty($this->data)) {

//            $this->set('content_for_layout', $this->data["NewsMessage"]["html"]);
//
//            $view = $this->render("render_view", "newsletters");
//            $this->data["NewsMessage"]["html"] = $view;
            $this->NewsMessage->create();
            if ($this->NewsMessage->save($this->data)) {

                $news_id = $this->NewsMessage->id;
                if (!empty($this->data['NewsMessageFile'])) {
                    foreach ($this->data['NewsMessageFile'] as &$file) {
                        if (!empty($file) && $file['file']['size'] != 0) {
                            $this->NewsMessageFile->create();
                            $data2['NewsMessageFile']['news_message_id'] = $news_id;
                            $data2['NewsMessageFile']['file'] = $file['file'];
                            $this->NewsMessageFile->save($data2);
                        }
                    }
                }

                $this->flashMessage(__('The Newsletter Message has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Newsletter Message could not be saved. Please, try again.', true));
            }
        } /*else if (!isset($template_id)) {
            $this->loadModel('NewsletterTemplate');
            $this->NewsletterTemplate->recursive = -1;
            $newsletter_templates = $this->NewsletterTemplate->find('list');
            $this->set('newsletter_templates', $newsletter_templates);
            $this->set('h2', 'Add Newsletter Message');
            $this->render('admin_select_template');
        } */else {
            if (!empty($template_id) && $template_id > 0) {
                $this->loadModel('NewsletterTemplate');
                $data = $this->NewsletterTemplate->findById($template_id);
                $this->set('template_html', $data['NewsletterTemplate']['html']);
            }
        }
        $this->set('h2', 'Add Newsletter Message');
    }

    //----------------------------------------
    function admin_edit($id = null) {
        $this->loadModel('NewsMessageFile');

        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Newsletter Message', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if (!empty($this->data['NewsMessageFile'])) {
                foreach ($this->data['NewsMessageFile'] as &$file) {
                    if (!empty($file) && $file['file']['size'] != 0) {
                        $data2['NewsMessageFile']['news_message_id'] = $id;
                        $data2['NewsMessageFile']['id'] = $file['id'];
                        $data2['NewsMessageFile']['file'] = $file['file'];
                        $this->NewsMessageFile->save($data2);
                    }
                }
            }
            if ($this->NewsMessage->save($this->data)) {
                $this->flashMessage(__('The Newsletter Message has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Newsletter Message could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->NewsMessage->read(null, $id);
            $this->data['NewsMessageFile'] = $this->NewsMessageFile->find('all', array('conditions' => array('NewsMessageFile.news_message_id' => $id)));
        }
        $this->set('id', $id);

        $this->set('h2', 'Edit Newsletter Message');

        $this->render('admin_add');
    }

    //----------------------------------------
//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Newsletter Message', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->NewsMessage->del($id)) {
//            $this->flashMessage(__('Newsletter Message deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    //----------------------------------------
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Message', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->NewsMessage->deleteAll(array('NewsMessage.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Messages deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('message', true)), true));
        }
        $module_name = __('message', true);
        if (count($id) > 1) {
            $module_name = __('newsMessages', true);
        }
        $newsMessages = $this->NewsMessage->find('all', array('conditions' => array('NewsMessage.id' => $id)));
        if (empty($newsMessages)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->NewsMessage->deleteAll(array('NewsMessage.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('The %s has been deleted', true), __('message', true)), 'Sucmessage');
            } else {
                $this->flashMessage(__(sprintf(__('Error in deleting %s', true), __('message', true)), true));
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('newsMessages', $newsMessages);
    }

    //----------------------------------------
    function admin_send_test_email() {
        $message = $this->NewsMessage->findById($_POST['id']);
        $emails = explode(",", $_POST['email_address']);
        foreach ($emails as $key => $value) {
            if (!Validation::custom($value, VALID_EMAIL)) {
                $this->flashMessage(__('Invalid email Address', true));
                $this->redirect(array('action' => 'index'));
            }
        }
//        if (!Validation::custom($_POST['email_address'], VALID_EMAIL)) {
//            $this->flashMessage(__('Invalid email Address', true));
//        }


        if (!sizeof($message)) {
            $this->flashMessage(__('Invalid id for Newsletter Message', true));
        } else {
            $this->loadModel('NewsMessageFile');
            $news_attachments = $this->NewsMessageFile->find('all', array('conditions' => array('NewsMessageFile.news_message_id' => $message['NewsMessage']['id'])));
            $attachments = array();
            foreach ($news_attachments as $attach) {
                $attachments[] = $this->NewsMessageFile->getFileAbsolutePath('file', $attach);
            }
            $this->Email->from = $this->config['txt.site_name'] . "<{$this->config['txt.send_mail_from']}>";
            $this->Email->subject = $message['NewsMessage']['subject'];
            $this->Email->sendAs = 'html';
            $this->Email->template = 'plain';
            $this->Email->layout = 'newsletter';
            if (!empty($attachments)) {
                $this->Email->attachments = $attachments;
            }
            $this->set('message', $message['NewsMessage']['html']);

            foreach ($emails as $key => $email) {
                $this->Email->to = $email;
                $this->Email->send($message['NewsMessage']['html']);
//                if ($this->Email->send($message['NewsMessage']['html']))
                $this->flashMessage(__('Your message has been sent', true), 'Sucmessage');
//                else
//                    $this->flashMessage(__('couldn\'t send email', true));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

//----------------------------------------------------
    function admin_export($message = null) {
        if (!$message) {
            $this->flashMessage(__('Invalid id for Newsletter Message', true));
            $this->redirect(array('action' => 'index'));
        }
        $contents = $this->NewsMessage->findById($message);
        //$this->file_put_contents($contents['Message']['subject'].'.html', $contents['Message']['html']);
        $this->set('message', $contents);
        $this->layout = '';
    }

     //--------------------------------------------------
    function cron_send_messages() {
        if (file_get_contents('flag_email.txt') == 0) {
            file_put_contents('flag_email.txt', 1);
            $conditions['MailJob.date <='] = get_today_date('Y-m-d H:i');
            $conditions['MailJob.is_sent'] = 0;

            $this->loadModel('MailJob');
            $message_jobs = $this->MailJob->find('all', array('conditions' => $conditions));
            foreach ($message_jobs as $message_job) {
                $messages = $message_job['MailLog'];

                if (!empty($messages)) {
                    foreach ($messages as $message) {
                        $this->loadModel('NewsMessageFile');
                        $news_attachments = $this->NewsMessageFile->find('all', array('conditions' => array('NewsMessageFile.news_message_id' => $message['news_message_id'])));
                        $attachments = array();
                        foreach ($news_attachments as $attach) {

                            $attachments[] = $this->NewsMessageFile->getFileAbsolutePath('file', $attach);
                        }


                        $NewsMessage = $this->NewsMessage->findById($message['news_message_id']);

                        $this->Email->to = $message['email'];
                        $this->Email->from = $this->config['txt.site_name'] . "<{$this->config['txt.send_mail_from']}>";
                        $this->Email->attachments = $attachments;
                        $this->Email->subject = $message['subject'];
                        $this->Email->sendAs = 'html';
                        $this->Email->template = 'plain';
                        $this->Email->layout = 'newsletter';
//                        $this->set('content_for_layout', $NewsMessage['NewsMessage']['html']);
//                        $view = $this->render("render_view", "newsletters");
//                        $message['NewsMessage']['html'] = $view;
                        $this->set('message', $NewsMessage['NewsMessage']['html']);
                        if ($this->Email->send($NewsMessage['NewsMessage']['html'])) {
                            $this->data['MailLog']['id'] = $message['id'];
                            $this->data['MailLog']['is_sent'] = 1;
                            $this->NewsMessage->MailLog->save($this->data['MailLog']);
                        }
                    }
                }
                $this->data['MailJob']['id'] = $message_job['MailJob']['id'];
                $this->data['MailJob']['is_sent'] = 1;
                $this->MailJob->save($this->data['MailJob']);
            }

            echo 'Message has been sent to all contacts';

            file_put_contents('flag_email.txt', 0);
        } else {
            mail('developer6@silvertrees.net', 'Cant run email sender on ' . $this->config['txt.site_name'], $this->action);
//            mail('developer6@hazemserver.com', 'Cant run email sender on ' . $this->config['txt.site_name'], $this->action);
            echo 'All messages have been Sent before..there are not any delayed messages';
        }
        exit();
    }

//---------------------------------------
    function file_put_contents($file, $contents = '', $method = 'a+') {
        $file_handle = fopen($file, $method);
        fwrite($file_handle, $contents);
        fclose($file_handle);
        return true;
    }

    //---------------------------------------------
    function admin_add_schedule() {
        if (!empty($this->data)) {
            $this->_schedule();
            $this->flashMessage('Message Scheduled', 'Sucmessage');
            $this->redirect(array('action' => 'index', 'controller' => 'mail_jobs'));
        }

        $this->pageTitle = "Add Schedule Job";
        $news_messages = $this->NewsMessage->find('list');
        $this->set('news_messages', $news_messages);
        $this->set('h2', $this->pageTitle);
    }

    //--------------------------------------------------------
    function _schedule() {
        if (empty($this->data['NewsMessage']['news_message_id'])) {
            $this->flashMessage('Please select newsletter message');
            $this->redirect(array('action' => 'add_schedule'));
        }

        $this->data['MailLog'] = $this->data['NewsMessage'];
        $day = empty($this->data['MailLog']['day']) ? get_today_date('d-m-Y') : $this->data['MailLog']['day'];
        $hour = $this->data['MailLog']['hour'];
        $minute = $this->data['MailLog']['minute'];
        $send_time = date('Y-m-d H:i', strtotime("$day $hour:$minute"));


        $this->loadModel('Newsletter');
        $conditions = array();
        $conditions['Newsletter.active'] = 1;
//        if (!empty($this->data['NewsMessage']['location_id'])) {
//            $conditions['User.location_id'] = $this->data['NewsMessage']['location_id'];
//        }
//        if (!empty($this->data['NewsMessage']['category'])) {
//            $conditions['User.category'] = $this->data['NewsMessage']['category'];
//        }
//        if (!empty($this->data['NewsMessage']['membership_type'])) {
//            $conditions['User.membership_type'] = $this->data['NewsMessage']['membership_type'];
//        }
//        if ($this->data['NewsMessage']['user_type'] == 2) {
//            $conditions[] = 'User.id in (select user_id from subscriptions where CURDATE() <= expiry_date)';
//        }
//        if ($this->data['NewsMessage']['user_type'] == 3) {
//            $conditions[] = 'User.id in (select user_id from subscriptions where CURDATE() > expiry_date)';
//        }
        $contacts = $this->Newsletter->find('all', array('conditions' => $conditions, 'recursive' => -1));
//        $modelName = 'User';
//        $type = 'user';
        if (empty($contacts)) {
            $this->flashMessage('No contacts to schedule');
            $this->redirect(array('action' => 'index', 'controller' => 'mail_jobs'));
        }

        $message = $this->NewsMessage->find('first', array('conditions' => array('NewsMessage.id' => $this->data['MailLog']['news_message_id'])));
        $this->loadModel('MailJob');
        $job = array(
            'MailJob' => array(
                // 'location_id' => empty($this->data['NewsMessage']['location_id']) ? 0 : (',' . implode(',', array_filter($this->data['NewsMessage']['location_id']))),
                // 'membership_type' => empty($this->data['NewsMessage']['membership_type']) ? 0 : (',' . implode(',', array_filter($this->data['NewsMessage']['membership_type']))),
                'news_message_id' => $this->data['MailLog']['news_message_id'],
                'date' => $send_time,
                'is_sent' => 0,
            //'user_type' => $this->data['NewsMessage']['user_type']
            )
        );
        $this->MailJob->create();
        if ($this->MailJob->save($job)) {
            foreach ($contacts as $contact) {
                $job_id = $this->MailJob->getLastInsertID();
                $rec = array(
                    'MailLog' => array(
                        'subject' => $message['NewsMessage']['subject'],
                        'ref_id' => 'job_' . $job_id,
                        'mail_job_id' => $job_id,
                        'email' => $contact["Newsletter"]['email'],
                        'news_message_id' => $this->data['MailLog']['news_message_id'],
                        'send_time' => $send_time,
                        'is_sent' => 0));
                $this->NewsMessage->MailLog->create();
                $this->NewsMessage->MailLog->save($rec);
            }
        }
        file_put_contents('flag_email.txt', 0);
    }

   
    function beforeFilter() {
        parent::beforeFilter();
        $this->titleAlias = "Newsletters";
    }

}

?>