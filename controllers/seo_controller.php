<?php

/**
 * @@property Seo Seo
 */
class SeoController extends AppController {

	var $name = 'Seo';
	var $helpers = array('Html', 'Form', 'Fck', 'Mixed');

	function admin_index() {
		$conditions = $this->_filter_params();
		$this->Seo->recursive = 0;
		$this->set('seos', $this->paginate('Seo', $conditions));
	}

	function admin_add() {

		if (!empty($this->data)) {
			$this->Seo->create();
			if ($this->Seo->save($this->data)) {
				$this->Session->setFlash(__('The Seo has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Seo could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Seo', true));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->data)) {

			if ($this->Seo->save($this->data)) {
				$this->Session->setFlash(__('The Seo has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Seo could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Seo->read(null, $id);
		}
		$this->render('admin_add');
	}

	function admin_delete($id = null) {
		if (empty($id) && !empty ($_POST['ids'])) {
			$id = $_POST['ids'];
		 } 
 		if (!$id && empty ($_POST)) {
			$this->flashMessage(sprintf (__('Invalid id for %s', true), __('seo',true)));
			$this->redirect(array('action'=>'index'));
		}
		$module_name= __('seo', true);
		if(count($id) > 1){
			$module_name= __('seo', true);
		 } 
		$seos = $this->Seo->find('all',array('conditions'=>array('Seo.id'=>$id)));
		if (empty($seos)){
			$this->flashMessage(sprintf(__('%s not found', true), $module_name));
			$this->redirect($this->referer(array('action' => 'index'), true));
		}
		if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
			if ($_POST['submit_btn'] == 'yes' && $this->Seo->deleteAll(array('Seo.id'=>$_POST['ids']))) {
				$this->flashMessage(sprintf (__('%s deleted', true), $module_name), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			}
			else{
				$this->redirect(array('action'=>'index'));
			}
		}
		$this->set('seos',$seos);
	}

}

?>