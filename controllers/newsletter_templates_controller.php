<?php
class NewsletterTemplatesController extends AppController {

	var $name = 'NewsletterTemplates';
	var $helpers = array('Html', 'Form','Fck');
	
	
	function admin_index() {
		$cond = $this->_filter_params();
		$this->NewsletterTemplate->recursive = 0;
		$this->set('newsletterTemplates', $this->paginate('NewsletterTemplate', $cond));
		$this->set('h2','Newsletter Templates');
	}
	
	
	function admin_view($id = null) {
		if (!$id) {
			$this->flashMessage(__('Invalid Newsletter Template.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->layout='newsletters';
		
		$this->set('newsletterTemplate', $this->NewsletterTemplate->read(null, $id));
		//$this->set('content_for_layout', $this->NewsletterTemplate->read(null, $id));
		$this->set('h2','View Newsletter Template');
	}

	function admin_add() {
		
		if (!empty($this->data)) {
			
			$this->NewsletterTemplate->create();
			if ($this->NewsletterTemplate->save($this->data)) {
				$this->flashMessage(__('The Newsletter Template has been saved', true), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			}  else
			{
				$this->flashMessage(__('The Newsletter Template could not be saved. Please, try again.', true));
			}
		}
		$this->set('h2','Add Newsletter Template');
	}

	function admin_edit($id = false) {
		if (!$id && empty($this->data)) {
			$this->flashMessage(__('Invalid News letterTemplate', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->NewsletterTemplate->save($this->data)) {
				$this->flashMessage(__('The Newsletter Template has been saved', true), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(__('The Newsletter Template could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->NewsletterTemplate->read(null, $id);
		}
		$this->set('h2','Edit Newsletter Template');
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->flashMessage(__('Invalid id for Newsletter Template', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->NewsletterTemplate->del($id)) {
			$this->flashMessage(__('Newsletter Template deleted', true), 'Sucmessage');
			$this->redirect(array('action'=>'index'));
		}
	}
	
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->titleAlias = "Newsletters";
	}
	
	

}
?>