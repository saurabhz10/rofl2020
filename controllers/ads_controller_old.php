<?php

/**
 * @property Ad Ad
 *
 */
class AdsController extends AppController {

    var $name = 'Ads';
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->Ad->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('ads', $this->paginate('Ad', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid ad.', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->set('ad', $this->Ad->read(null, $id));
    }

    function admin_add($place = false) {
         
        if (!empty($this->data)) {
            $this->Ad->create();
            $this->Ad->set_settings($places[$this->data['Ad']['place']]);
            if ($this->Ad->save($this->data)) {
                $this->flashMessage(__('The ad has been saved', true), 'Sucmessage');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->flashMessage(__('The ad could not be saved. Please, try again.', true));
            }
        }

        

        $this->set('image_settings',$this->Ad->getImageSettings());
        
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid ad', true));
            $this->redirect(array('action'=>'index'));
        }
        $ad = $this->Ad->read(null, $id);

         

        if (!empty($this->data)) {
            if ($this->Ad->save($this->data)) {
                $this->flashMessage(__('The ad has been saved', true), 'Sucmessage');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->flashMessage(__('The ad could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $ad;
            
        }
        $this->set('image_settings',$this->Ad->getImageSettings());

        
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for ad', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Ad->del($id)) {
            $this->flashMessage(__('ad deleted', true), 'Sucmessage');
            $this->redirect(array('action'=>'index'));
        }
    }

    function admin_delete_multi() {
        if (empty($_POST['ids'])||!is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for ad', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Ad->deleteAll(array('Ad.id'=>$_POST['ids'])))  {
            $this->flashMessage(__('ad items deleted', true), 'Sucmessage');
            $this->redirect(array('action'=>'index'));
        }
        else  {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action'=>'index'));
        }
    }

}
?>