<?php
class TemplatesController extends AppController {

	var $name = 'Templates';

	/**
	 * @var Template
	 */
	var $Template;
	var $helpers = array('Html', 'Form','Fck');

	
	function admin_index() {
		$this->Template->recursive = 0;
		$conditions = $this->_filter_params();
		$this->set('templates', $this->paginate('Template', $conditions));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Template->create();
			if ($this->Template->save($this->data)) {
				$this->flashMessage(sprintf (__('The %s has been saved', true), __('template',true)), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('template',true)));
			}
		}
		$this->set('image_settings',$this->Template->getImageSettings());
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->flashMessage(sprintf (__('Invalid %s.', 'template',true)));
			$this->redirect(array('action'=>'index'));
		}
		$template = $this->Template->read(null, $id);
		if (!empty($this->data)) {
			if ($this->Template->save($this->data)) {
				$this->flashMessage(sprintf (__('The %s  has been saved', true), __('template',true)), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(sprintf (__('The %s could not be saved. Please, try again', true), __('template',true)));
			}
		}
		if (empty($this->data)) {
			$this->data = $template;
		}
		$this->set('image_settings',$this->Template->getImageSettings());
		$this->render('admin_add');
	}

	function admin_delete($id = null) {
		if (empty($id) && !empty ($_POST['ids'])) {
			$id = $_POST['ids'];
		 } 
 		if (!$id && empty ($_POST)) {
			$this->flashMessage(sprintf (__('Invalid id for %s', true), __('template',true)));
			$this->redirect(array('action'=>'index'));
		}
		$module_name= __('template', true);
		if(count($id) > 1){
			$module_name= __('templates', true);
		 } 
		$templates = $this->Template->find('all',array('conditions'=>array('Template.id'=>$id)));
		if (empty($templates)){
			$this->flashMessage(sprintf(__('%s not found', true), $module_name));
			$this->redirect($this->referer(array('action' => 'index'), true));
		}
		if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
			if ($_POST['submit_btn'] == 'yes' && $this->Template->deleteAll(array('Template.id'=>$_POST['ids']))) {
				$this->flashMessage(sprintf (__('%s deleted', true), $module_name), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			}
			else{
				$this->redirect(array('action'=>'index'));
			}
		}
		$this->set('templates',$templates);
	}
}
?>