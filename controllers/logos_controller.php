<?php

class LogosController extends AppController {

    var $name = 'Logos';

    /**
     * @var Logo
     */
    var $Logo;
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->Logo->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('logos', $this->paginate('Logo', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('logo', true)), true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('logo', $this->Logo->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Logo->create();
            if ($this->Logo->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s has been saved', true), __('logo', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__(sprintf(__('Error in saving  %s', true), __('logo', true)), true));
            }
        }
        $this->set('image_settings', $this->Logo->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            //$this->flash(sprintf(__('Invalid %s', true), __('logo', true)), array('action' => 'index'));
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('logo', true)), true));
        }
        $logo = $this->Logo->read(null, $id);
        if (!empty($this->data)) {
            if ($this->Logo->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s has been saved', true), __('logo', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__(sprintf(__('Error in saving  %s', true), __('logo', true)), true));
            }
        }
        if (empty($this->data)) {
            $this->data = $logo;
        }
        $this->set('image_settings', $this->Logo->getImageSettings());
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('logo', true)), true));
        }
        $module_name = __('logo', true);
        if (count($id) > 1) {
            $module_name = __('logos', true);
        }
        $logos = $this->Logo->find('all', array('conditions' => array('Logo.id' => $id)));
        if (empty($logos)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Logo->deleteAll(array('Logo.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('The %s has been deleted', true), __('logo', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__(sprintf(__('Error in deleting %s', true), __('logo', true)), true));
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('logos', $logos);
    }

}

?>