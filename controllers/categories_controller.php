<?php

/**
 * @property Category Category
 * 
 */class CategoriesController extends AppController {

    var $name = 'Categories';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Category->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('categories', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Category.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('categories', $this->Category->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Category->create();
            if ($this->Category->save($this->data)) {
                $this->flashMessage(__('The Category has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Category could not be saved. Please, try again.', true));
            }
        }

        $this->set('image_settings', $this->Category->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Category', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Category->save($this->data)) {
                $this->flashMessage(__('The Category has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Category could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Category->read(null, $id);
        }

        $this->set('image_settings', $this->Category->getImageSettings());
        

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Category', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Category->del($id)) {
//            $this->flashMessage(__('Category deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Category', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Category->deleteAll(array('Category.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Category items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }
    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('link', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('Category', true);
        if (count($id) > 1) {
            $module_name = __('categories', true);
        }
        $categories = $this->Category->find('all', array('conditions' => array('Category.id' => $id)));
        if (empty($categories)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Category->deleteAll(array('Category.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('categories', $categories);
    }

    function index() {
        $conditions = array('active' => 1);
        $this->pageTitle = 'Category';
        $this->paginate['Category']['order'] = 'display_order ASC';
        $this->paginate['Category']['limit'] = 5;


        $categories = $this->paginate('Category', $conditions);
        $this->set('categories', $categories);
        $this->set('snippet', $this->get_snippet('links_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $categories = $this->Category->find('first', array('conditions' => array('Category.permalink' => $permalink, 'Category.active' => 1)));

        if (!$categories) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'Category - ' . $categories['Category']['title'];

        $this->loadModel('Gallery');
        $conditions = array('Gallery.active' => 1, 'Gallery.category_id' => $categories['Category']['id']);
//        $this->Paginator->options(array('url' => $this->passedArgs));
//        $this->Gallery->recursive = 1;
         $this->paginate['Gallery']['recursive'] = 1;
        
         $this->paginate = array(
                            'Gallery' =>array(
                                'conditions' => $conditions,
                                'limit' => $this->config['number.pagination_number_for_the_gallery_list_pages'],
                                /*AY 19/12*/
                                'order' => 'Gallery.display_order ASC',
                                /*END AY*/
                            'contain' => array('GalleryImage'=>array(
                                
                            'order' => 'GalleryImage.id ASC'
                            ,'limit' => 1,
                            ))
                            )
                          );
         
  
        $galleries = $this->paginate('Gallery');

        
        $categoriessnippet=$this->get_snippet('categoriessnippet');
        $this->set(compact('categories','galleries','permalink','categoriessnippet'));
    }

}

?>