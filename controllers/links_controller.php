<?php

/**
 * @property Link Link
 * 
 */class LinksController extends AppController {

    var $name = 'Links';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Link->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('links', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Link.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('links', $this->Link->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Link->create();
            if ($this->Link->save($this->data)) {
                $this->flashMessage(__('The Link has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Link could not be saved. Please, try again.', true));
            }
        }

        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());
        $this->set('image_settings', $this->Link->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Link', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Link->save($this->data)) {
                $this->flashMessage(__('The Link has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Link could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Link->read(null, $id);
        }

        $this->set('image_settings', $this->Link->getImageSettings());
        $this->loadModel('Template');
        $this->set('templates', $this->Template->get_templates_list());

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Link', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Link->del($id)) {
//            $this->flashMessage(__('Link deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Link', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Link->deleteAll(array('Link.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Link items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }
    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('link', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('link', true);
        if (count($id) > 1) {
            $module_name = __('links', true);
        }
        $links = $this->Link->find('all', array('conditions' => array('Link.id' => $id)));
        if (empty($links)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Link->deleteAll(array('Link.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('links', $links);
    }

    function index() {
        $conditions = array('active' => 1);
        $this->pageTitle = 'Link';
        $this->paginate['Link']['order'] = 'display_order ASC';
        $this->paginate['Link']['limit'] = 5;


        $links = $this->paginate('Link', $conditions);
        $this->set('links', $links);
        $this->set('snippet', $this->get_snippet('links_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $links = $this->Link->find('first', array('conditions' => array('Link.permalink' => $permalink, 'Link.active' => 1)));

        if (!$links) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'Link - ' . $links['Link']['title'];

        $this->set('links', $links);
    }

}

?>