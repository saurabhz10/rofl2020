<?php

class MailJobsController extends AppController {

    var $name = 'MailJobs';
    /**
     * @var MailJob
     */
    var $MailJob;
    var $helpers = array('Html', 'Form');


    function admin_index() {
        $this->MailJob->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('mailJobs', $this->paginate('MailJob', $conditions));
        $this->set('h2', 'Scheduled Jobs');
    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('mail job', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('mailJob', true);
        if (count($id) > 1) {
            $module_name = __('mailJobs', true);
        }
        $mailJobs = $this->MailJob->find('all', array('conditions' => array('MailJob.id' => $id)));
        if (empty($mailJobs)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->MailJob->deleteAll(array('MailJob.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('mailJobs', $mailJobs);
        $this->set('h2', 'Delete Schedule Jobs');
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->titleAlias = "Newsletters";
    }

}

?>