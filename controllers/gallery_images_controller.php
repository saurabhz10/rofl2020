<?php

class GalleryImagesController extends AppController {

    var $name = 'GalleryImages';

    /**
     * @var GalleryImage
     */
    var $GalleryImage;
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->loadModel("Gallery");
        $this->set('galleries', $this->Gallery->find('list'));
        $this->GalleryImage->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('galleryImages', $this->paginate('GalleryImage', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('gallery image', true)), true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('galleryImage', $this->GalleryImage->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->GalleryImage->create();
            if ($this->GalleryImage->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s has been saved', true), __('gallery image', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('gallery image', true)));
            }
        }
        $galleries = $this->GalleryImage->Gallery->find('list');
        $this->set(compact('galleries'));
        $this->set('image_settings', $this->GalleryImage->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(sprintf(__('Invalid %s.', 'gallery image', true)));
            $this->redirect(array('action' => 'index'));
        }
        $galleryImage = $this->GalleryImage->read(null, $id);
        if (!empty($this->data)) {
            if ($this->GalleryImage->save($this->data)) {
                $this->flashMessage(sprintf(__('The %s  has been saved', true), __('gallery image', true)), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(sprintf(__('The %s could not be saved. Please, try again', true), __('gallery image', true)));
            }
        }
        if (empty($this->data)) {
            $this->data = $galleryImage;
        }
        $this->set('image_settings', $this->GalleryImage->getImageSettings());
        $galleries = $this->GalleryImage->Gallery->find('list');
        $this->set(compact('galleries'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(sprintf(__('Invalid id for %s', true), __('gallery image', true)));
            $this->redirect(array('action' => 'index'));
        }
        $module_name = __('galleryImage', true);
        if (count($id) > 1) {
            $module_name = __('galleryImages', true);
        }
        $galleryImages = $this->GalleryImage->find('all', array('conditions' => array('GalleryImage.id' => $id)));
        if (empty($galleryImages)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->GalleryImage->deleteAll(array('GalleryImage.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('%s deleted', true), $module_name), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('galleryImages', $galleryImages);
    }

}

?>