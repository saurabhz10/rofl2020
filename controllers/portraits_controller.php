<?php

/**
 * @property Portrait Portrait
 * 
 */class PortraitsController extends AppController {

    var $name = 'Portraits';
    var $helpers = array('Html', 'Form', 'Fck', 'Paginator', 'Text');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Portrait->recursive = 0;
        $conditions = $this->_filter_params();
        $this->set('portraits', $this->paginate($conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Portrait.', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('portraits', $this->Portrait->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Portrait->create();
            if ($this->Portrait->save($this->data)) {
                $this->flashMessage(__('The Portrait has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Portrait could not be saved. Please, try again.', true));
            }
        }

        // $this->loadModel('Template');
        //$this->set('templates', $this->Template->get_templates_list());
        $this->loadModel('Gallery');
        $this->set('galleries', $this->Gallery->find("list", array("conditions" => array("active" => 1))));
        $this->set('image_settings', $this->Portrait->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->flashMessage(__('Invalid Portrait', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Portrait->save($this->data)) {
                $this->flashMessage(__('The Portrait has been saved', true), 'Sucmessage');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->flashMessage(__('The Portrait could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Portrait->read(null, $id);
        }

        $this->set('image_settings', $this->Portrait->getImageSettings());
        $this->loadModel('Gallery');
        $this->set('galleries', $this->Gallery->find("list", array("conditions" => array("active" => 1))));
//        $this->loadModel('Template');
//        $this->set('templates', $this->Template->get_templates_list());

        $this->render('admin_add');
    }

//    function admin_delete($id = null) {
//        if (!$id) {
//            $this->flashMessage(__('Invalid id for Portrait', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Portrait->del($id)) {
//            $this->flashMessage(__('Portrait deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        }
//    }
//
//    function admin_delete_multi() {
//        if (empty($_POST['ids']) || !is_array($_POST['ids'])) {
//            $this->flashMessage(__('Invalid ids for Portrait', true));
//            $this->redirect(array('action' => 'index'));
//        }
//        if ($this->Portrait->deleteAll(array('Portrait.id' => $_POST['ids']))) {
//            $this->flashMessage(__('Portrait items deleted', true), 'Sucmessage');
//            $this->redirect(array('action' => 'index'));
//        } else {
//            $this->flashMessage(__('Unknown error', true));
//            $this->redirect(array('action' => 'index'));
//        }
//    }


    function admin_delete($id = null) {
        if (empty($id) && !empty($_POST['ids'])) {
            $id = $_POST['ids'];
        }
        if (!$id && empty($_POST)) {
            $this->flashMessage(__(sprintf(__('Invalid %s', true), __('portrait', true)), true));
        }
        $module_name = __('portrait', true);
        if (count($id) > 1) {
            $module_name = __('portraits', true);
        }
        $portraits = $this->Portrait->find('all', array('conditions' => array('Portrait.id' => $id)));
        if (empty($portraits)) {
            $this->flashMessage(sprintf(__('%s not found', true), $module_name));
            $this->redirect($this->referer(array('action' => 'index'), true));
        }
        if (!empty($_POST['submit_btn']) && !empty($_POST['ids'])) {
            if ($_POST['submit_btn'] == 'yes' && $this->Portrait->deleteAll(array('Portrait.id' => $_POST['ids']))) {
                $this->flashMessage(sprintf(__('The %s has been deleted', true), __('portrait', true)), 'Sucmessage');
                                $this->redirect(array('action' => 'index'));

            } else {
                $this->flashMessage(__(sprintf(__('Error in deleting %s', true), __('portrait', true)), true));
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('portraits', $portraits);
    }

    function index() {

        $conditions = array('active' => 1);
        $this->pageTitle = 'Portrait';
        $this->paginate['Portrait']['order'] = 'display_order ASC';
        $this->paginate['Portrait']['limit'] = 5;


        $portraits = $this->paginate('Portrait', $conditions);
        $this->set('portraits', $portraits);
        $this->set('snippet', $this->get_snippet('portraits_header'));
    }

    function view($permalink) {
        if (!$permalink) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }
        $portraits = $this->Portrait->find('first', array('conditions' => array('Portrait.permalink' => $permalink, 'Portrait.active' => 1)));

        if (!$portraits) {
            $this->flashMessage('Invalid URL');
            $this->redirect('/');
        }

        $this->pageTitle = 'Portrait - ' . $portraits['Portrait']['title'];

        $this->set('portraits', $portraits);
    }

}

?>