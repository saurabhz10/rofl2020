<?php
/**
 * @property Snippet Snippet
 *
 */
class SnippetsController extends AppController {

    var $name = 'Snippets';
    var $helpers = array('Html', 'Form','Fck' , 'Mixed');



    function admin_index() {
        //$conditions=$this->_filter_params();

        $snippets=array();

        foreach($this->Snippet->snippetss as $snippet) {
            $snippets[]=array('Snippet'=>array('name'=>$snippet['name'],
                            'title'=>$snippet['title']));
        }
        //debug($this->Snippet->snippetss);exit();
        $this->set('snippets', $snippets);
    }

//----------------------
    function admin_edit($name=null) {

        if ((empty ($name)) && empty($this->data)) {
            $this->flashMessage(__('Invalid Snippet', true));
            $this->redirect(array('action'=>'index'));
        }

        if (!empty($this->data)) {
            if ($this->Snippet->save($this->data,false)) {
                $this->flashMessage(__('The Snippet has been saved', true), 'Sucmessage');
                $this->redirect(array('action'=>'index'));
            } else {
                $this->flashMessage(__('The Snippet could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $id=$this->Snippet->field('id', array('Snippet.name'=>$name));

            //if this snippet does not exist in the DB , Add it
            if(empty ($id)) {
                if(!$this->Snippet->save(array('Snippet'=>array('name'=>$name)),false)) {
                    $this->flashMessage(__('System Error, please contact the admin', true));
                    $this->redirect(array('action'=>'index'));
                }
                else {
                    $id=$this->Snippet->id;
                }

            }

            $this->data = $this->Snippet->read(null, $id);
        }
        /*$this->set('advanced_editor',
            isset($this->Snippet->snippetss[$name]['advanced_editor'])&&!$this->Snippet->snippetss[$name]['advanced_editor']?false:true );
            $this->set('snippet_properties',$this->Snippet->snippetss[$name]);*/

        $this->render('admin_add');
    }
//------------------------------
    function admin_delete($id = null) {
        if (!$id) {
            $this->flashMessage(__('Invalid id for Snippet', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Snippet->del($id)) {
            $this->flashMessage(__('Snippet deleted', true), 'Sucmessage');
            $this->redirect(array('action'=>'index'));
        }else{
            $this->flashMessage(__('Invalid id for Snippet', true));
            $this->redirect(array('action'=>'index'));
        }
    }
//---------------------------------
    function admin_delete_multi() {
        if (empty($_POST['ids'])||!is_array($_POST['ids'])) {
            $this->flashMessage(__('Invalid ids for Snippet', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Snippet->deleteAll(array('Snippet.id'=>$_POST['ids']))) {
            $this->flashMessage(__('Snippet items deleted', true), 'Sucmessage');
            $this->redirect(array('action'=>'index'));
        }
        else {
            $this->flashMessage(__('Unknown error', true));
            $this->redirect(array('action'=>'index'));
        }
    }


}
?>