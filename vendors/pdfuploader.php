<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PDFUploader
{
	var $folder;
	var $maxSize;

	var $types = array(
		'application/pdf',
		'application/x-pdf',
		'application/adobe',
		'applications/vnd.pdf',
		'text/pdf',
		'text/x-pdf'
	);

	function PDFUploader($folder = 'files/pdfs', $maxSize = '20000000')
	{
		$this->folder = $folder;
		$this->maxSize = $maxSize;
	}

	function uploadPdf($fileData, $fileName = false)
	{
		if (!is_writable($this->folder)){
			$error = "File cannot be uploaded {$this->folder} is not writable";
			return array(false, $error);
		}
		else if (!in_array($fileData['type'], $this->types)){
			$error = "{$fileData['name']} has invalid file type";
			return array(false, $error);
		} else if ($fileData['size'] > $this->maxSize){
			$error = "{$fileData['name']} is too large";
			return array(false, $error);
		} else if (!is_uploaded_file($fileData['tmp_name'])){
			$error = "Couldn't upload {$fileData['name']}";
			return array(false, $error);
		} else {
			if ($fileName){
				$name = $fileName;
			} else {
				$name = substr(md5(uniqid(rand())), 0, 7) . '.pdf';
			}

			if (move_uploaded_file($fileData['tmp_name'], $this->folder . DS . $name)){
				return array($name, false);
			} else {
				return array(false, 'Error occurred while uploading');
			}
		}
	}
}
?>
