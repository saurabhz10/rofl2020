<?php

class Newsletter extends AppModel {

    var $name = 'Newsletter';
    var $filters = array('email' => 'like');

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'email' => array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'email', 'allowEmpty' => true, 'message' => __('vaild email address required', true)),
                array('rule' => 'isunique', 'allowEmpty' => true, 'message' => __('This Email is already added', true))),
        );
    }

}

?>