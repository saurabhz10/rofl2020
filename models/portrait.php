<?php

class Portrait extends AppModel {

    var $name = 'Portrait';
    var $filter = array('title' => 'like', 'short_description' => 'like');
    var $actsAs = array(
        'image' => array('image' => array(
                'width' => 113,
                'height' => 0,
                'required' => false,
                'resize' => 1,
                'crop' => 0))
    );
    var $validate = array(
        'title' => array('rule' => 'notempty', 'message' => 'title cannot be left balnk'),
        'url' => array(
            //array('rule' => 'notempty', 'message' => 'url cannot be left balnk'),
            array('rule' => 'urlCustom', 'message' => 'please enter valid url'),
            array('rule' => 'portraitUrl', 'message' => 'you must enter url or choose gallery'),
        ),
        'gallery_id' => array(
            //  array('rule' => 'notempty', 'message' => 'please choose gallery'),
            array('rule' => 'portraitUrl', 'message' => 'you must enter url or choose gallery'),
        ),
        'title' => array('rule' => 'notempty', 'message' => 'title cannot be left balnk'),
        'permalink' => array(array('rule' => '/[a-z][0-9a-z_\-]*/i', 'message' => 'valid perma link required', 'allowEmpty' => false),
            array('rule' => 'isUnique', 'message' => 'permalink already used')),
        'short_description' => array('rule' => array('maxLength', 500), 'message' => 'only 500 characters allowed')
    );

    function urlCustom($data) {

        if (filter_var($this->data[$this->name]['url'], FILTER_VALIDATE_URL) && !empty($this->data[$this->name]['url'])) {
            return TRUE;
        } else if (!empty($this->data[$this->name]['gallery_id'])) {
            return true;
        } else {
            return FALSE;
        }
    }

    function portraitUrl($data) {
        return (!empty($this->data[$this->name]['url']) && empty($this->data[$this->name]['gallery_id'])) || (empty($this->data[$this->name]['url']) && !empty($this->data[$this->name]['gallery_id']));
    }

    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        return true;
    }

}

?>