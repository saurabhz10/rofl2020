<?php
class Seo extends AppModel {
	var $name = 'Seo';
	var $useTable = 'seo';


    var $filters = array('criteria'=>'like','title'=>'like');
    
	public function __construct($id = null, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->validate = array(
			'criteria' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
			'title' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
			'keywords' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
			'description' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
		);
	}
}
?>
