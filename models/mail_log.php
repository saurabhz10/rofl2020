<?php

class MailLog extends AppModel {

    var $name = 'MailLog';
    var $belongsTo = array(
        'NewsMessage' => array('className' => 'NewsMessage', 'foreignKey' => 'news_message_id'),
        'MailJob' => array('className' => 'MailJob', 'foreignKey' => 'mail_job_id'),
    );

}

?>