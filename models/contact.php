<?php

class Contact extends AppModel {

    var $name = 'Contact';
    var $filters = array(
        'email' => 'like',
        'subject' => 'like',
        'contact_type',
        'created' => array('from' => 'From', 'to' => 'To', 'type' => 'date_range')
    );

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'first_name' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'last_name' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'email' => array('rule' => 'email', 'allowEmpty' => false, 'message' => __('Please enter a valid email.', true)),
            'subject' => array('rule' => 'notempty', 'message' => __('Please enter a valid subject.', true)),
            'message' => array('rule' => 'notempty', 'message' => __('Please enter a valid message.', true)),
            'security_code' => array('rule' => 'checkCaptcha', 'message' => __('Invalid security code.', true))
        );
    }

//    function checkCaptcha($data) {
//        return low($data['security_code']) == low($_SESSION['security_code']);
//    }

    function checkCaptcha($data) {
        return ( low($data['security_code']) == low($_SESSION['security_code']) && !empty($data['security_code']) );
    }

}

?>