<?php

class NewsMessageFile extends AppModel {

    var $name = 'NewsMessageFile';
    var $filters = array();
    var $actsAs = array(
        'file' => array(
            'file' => array(
                'folder' => 'files/uploads/',
                'extensions' => array('pdf', 'doc','xls','docx','xlsx','txt'))
        )
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'NewsMessage' => array('className' => 'NewsMessage', 'foreignKey' => 'news_message_id')
    );


    function __construct($id = false, $table = null, $ds =null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

    function getFilters() {
        return array(); //set filters here
    }

}

?>