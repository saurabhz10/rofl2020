<?php

class Link extends AppModel {

    var $name = 'Link';
    var $filter = array('title' => 'like', 'short_description' => 'like');
    var $actsAs = array(
        'image' => array('image' => array(
                'width' => 0 ,
                'height' => 0,
                'required' => false,
                'resize' => 0 ,
                'crop' => 0
            )
        )
    );
    var $validate = array(
        'title' => array('rule' => 'notempty', 'message' => 'Title cannot be left balnk'),
        'url' => array('rule' => 'notempty', 'message' => 'Url cannot be left balnk'),
        'short_description' => array('rule' => array('maxLength', 500), 'message' => 'only 500 characters allowed')
    );

//    function __construct($id = false, $table = null, $ds = null) {
//        parent::__construct($id, $table, $ds);
//        $this->validate = array();
//    }

    function getFilters() {
        return array(); //set filters here
    }

    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = Inflector::slug($this->data[$this->name]['title'], '-');
        }
        return true;
    }

}

?>