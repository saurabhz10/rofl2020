<?php

class Logo extends AppModel {

    var $name = 'Logo';
    var $filters = array();
    var $actsAs = array(
        'image' => array(
            'image' => array(
                'resize' => 2,
                'width' => 0,
                'height' => 0,
                'thumb_width' => 134,
                'thumb_height' => 131,
                'required' => true,
                'aspect_required' => false,
            )
        )
    );

//        var $validate = array(
//		'image' => array('notempty')
//	);


    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

    function getFilters() {
        return array(); //set filters here
    }

}

?>