<?php

class imageBehavior extends ModelBehavior {

    /**
     * Errors
     *
     * @var array
     */
    var $errors = array();
    var $_files_to_delete = array();
    var $_files_to_be_renamed = array();

    /**
     * Defaults
     *
     * @var array
     * @access protected
     */
    var $_defaults = array(
        'image' =>
        array(
            'resize' => 2,
            'width' => '640',
            'height' => '480',
            'default' => false,
            'thumb_width' => '320',
            'thumb_height' => '240',
            'thumb_default' => false,
            'thumb2_width' => '160',
            'thumb2_height' => '120',
            'thumb2_default' => false,
            'thumb_prefix' => 'thumb_',
            'thumb2_prefix' => 'thumb2_',
            'file_name' => '{$rand}_{$file_name}', // {$rand}:random chars, {$file_name}: the original file name
            'extensions' => array('jpg', 'png', 'gif'),
            'folder' => 'img/uploads/',
            'aspect_required' => false,
            'aspect_tolerance' => '0.05',
            'crop' => false,
            'required' => false,
            'delete_file_on_update' => true,
            'folder_prefix_field' => false
        )
    );

    /**
     *
     * @param AppModel $Model
     * @param <type> $config
     */
    function setup(&$Model, $config = array()) {

        if (is_array($config) && sizeof($config)) {

            foreach ($config as $field => $opitions) {
                if (is_array($opitions))
                    $this->settings[$Model->name][$field] = array_merge($this->_defaults['image'], $opitions);
                else if (!empty($opitions) && is_string($opitions))
                    $this->settings[$Model->name][$opitions] = $this->_defaults['image'];
            }
        }
        else if (!empty($config) && is_string($config)) {

            $this->settings[$Model->name][$config] = $this->_defaults['image'];
        } else {
            $this->settings[$Model->name] = $this->_defaults;
        }
    }

    /**
     * After save method. Called after all saves
     *
     * Overriden to transparently manage setting the lft and rght fields if and only if the parent field is included in the
     * parameters to be saved.
     *
     * @param AppModel $Model
     * @param boolean $created indicates whether the node just saved was created or updated
     * @return boolean true on success, false on failure
     * @access public
     */
    function beforeSave(&$Model) {

        $ret = true;
        $this->_files_to_delete = array();
        foreach ($this->settings[$Model->name] as $field => $options) {

            if (!empty($Model->data[$Model->name][$field]) && is_array($Model->data[$Model->name][$field])) {
                if (!$this->_upload_image($Model, $field)) {
                    $ret = false;
                } else if (isset($Model->data[$Model->name]['id']) && $Model->data[$Model->name]['id']) {
                    //Get the current image filename stored in the db to remove
                    $Model->id = $Model->data[$Model->name]['id'];
                    $old_file_name = $Model->field($field);
                    if ($old_file_name != $Model->data[$Model->name][$field] && $options['delete_file_on_update'] && $old_file_name) {
                        $this->_files_to_delete[$field][] = str_replace($Model->data[$Model->name][$field], $old_file_name, $this->_getImageAbsolutePath($Model, $field, $Model->data));
                        if ($options['resize'] > 1)
                            $this->_files_to_delete[$field][] = str_replace($Model->data[$Model->name][$field], $old_file_name, $this->_getImageAbsolutePath($Model, $field, $Model->data, 1));
                        if ($options['resize'] > 2)
                            $this->_files_to_delete[$field][] = str_replace($Model->data[$Model->name][$field], $old_file_name, $this->_getImageAbsolutePath($Model, $field, $Model->data, 2));
                    }
                }
            }
        }

        return $ret;
    }

    /**
     * After save method. Called after all saves
     *
     *
     *
     * @param AppModel $Model
     * @param boolean $created indicates whether the node just saved was created or updated
     * @return boolean true on success, false on failure
     * @access public
     */
    function afterSave(&$Model, $created) {
        if (!$created)
            foreach ($this->_files_to_delete as $field => $filenames) {
                foreach ($filenames as $filename)
                    if (file_exists($filename))
                        unlink($filename);
            }
        return true;
    }

    /**
     *
     * @param AppModel $Model
     * @return <type>
     */
    function beforeDelete(&$Model) {

        $Model->read();
        foreach ($this->settings[$Model->name] as $field => $options) {
            if ($Model->data[$Model->name][$field] && $options['delete_file_on_update'])
                $this->_files_to_delete[$field][] = $this->_getImageAbsolutePath($Model, $field, $Model->data);
            if ($options['resize'] > 1)
                $this->_files_to_delete[$field][] = $this->_getImageAbsolutePath($Model, $field, $Model->data, 1);
            if ($options['resize'] > 2)
                $this->_files_to_delete[$field][] = $this->_getImageAbsolutePath($Model, $field, $Model->data, 2);
        }
        return true;
    }

    function afterDelete(&$Model) {
        foreach ($this->_files_to_delete as $field => $filenames) {
            foreach ($filenames as $filename)
                if (file_exists($filename))
                    unlink($filename);
        }
        return true;
    }

    function afterFind(&$Model, $results) {
        foreach ($results as $key => $val) {
            foreach ($this->settings[$Model->name] as $field => $options)
                if (isset($val[$Model->name][$field]) && $val[$Model->name][$field]) {
                    $prefix_folder = $this->__get_prefix_folder(&$Model, $field, $val);
                    $results[$key][$Model->name][$field . '_full_path'] = Router::url('/' . $prefix_folder . $options['folder'] . $val[$Model->name][$field]);
                    if ($options['resize'] > 1)
                        $results[$key][$Model->name][$field . '_thumb_full_path'] = Router::url('/' . $prefix_folder . $options['folder'] . $options['thumb_prefix'] . $val[$Model->name][$field]);
                    if ($options['resize'] > 2)
                        $results[$key][$Model->name][$field . '_thumb2_full_path'] = Router::url('/' . $prefix_folder . $options['folder'] . $options['thumb2_prefix'] . $val[$Model->name][$field]);
                }
                else {
                    if ($options['default'])
                        $results[$key][$Model->name][$field . '_full_path'] = Router::url($options['default']);

                    if ($options['thumb_default'])
                        $results[$key][$Model->name][$field . '_thumb_full_path'] = Router::url($options['thumb_default']);

                    if ($options['thumb2_default'])
                        $results[$key][$Model->name][$field . '_thumb2_full_path'] = Router::url($options['thumb2_default']);
                }
        }
        return $results;
    }

    /**
     * delete the file and the thumb of the image
     * @param string $field
     * @param string $file_name
     */
    function deleteImage(&$Model, $field, $id, $base_name) {
        $Model->id = $id;
        $data = $Model->read();

      
        return unlink($this->_getImageAbsolutePath($Model, $field, $data) . $base_name) && ($this->settings[$Model->name][$field]['resize'] > 1 ? unlink($this->_getImageAbsolutePath($Model, $field, $data, 1) . $base_name) : true) && ($this->settings[$Model->name][$field]['resize'] > 2 ? unlink($this->_getImageAbsolutePath($Model, $field, $data, 2) . $base_name) : true);
    }

    function getImageSettings(&$Model, $field = false, $param = false) {
        if (!$field && !$param)
            return $this->settings[$Model->name];
        else if (!$param)
            return $this->settings[$Model->name][$field];
        else
            return $this->settings[$Model->name][$field][$param];
    }

    function setImageSettings(&$Model, $field, $param, $value) {

        $this->settings[$Model->name][$field][$param] = $value;
    }

    /**
     * Get the file absolute path i.e. /var/www/html/project/webroot/img/uploads/file_name.jpg
     * @param string $field
     * @param string $file_name
     *
     * @return full file path
     */
    function _getImageAbsolutePath(&$Model, $field, $data, $thumb=0) {
        $prefix_folder = $this->__get_prefix_folder($Model, $field, $data);
        if (!$thumb)
            return WWW_ROOT . $prefix_folder . str_replace('/', DS, $this->settings[$Model->name][$field]['folder']) . $data[$Model->name][$field];
        else if ($thumb == 1)
            return WWW_ROOT . $prefix_folder . str_replace('/', DS, $this->settings[$Model->name][$field]['folder']) . $this->settings[$Model->name][$field]['thumb_prefix'] . $data[$Model->name][$field];
        else if ($thumb == 2)
            return WWW_ROOT . $prefix_folder . str_replace('/', DS, $this->settings[$Model->name][$field]['folder']) . $this->settings[$Model->name][$field]['thumb2_prefix'] . $data[$Model->name][$field];
    }

    /**
     * 	Uploads an image to defined folder and does operations as required. on failure it assigns validation error message to the field
     * @param array $image image array as it in $_FILES variable
     * @return true on success uploading, false if failed for any reason
     */
    function _upload_image(&$Model, $field) {
        $image = $Model->data[$Model->name][$field];

        $prefix_folder = $this->__get_prefix_folder(&$Model, $field, $Model->data);

        $folder = WWW_ROOT . DS . $prefix_folder . $this->settings[$Model->name][$field]['folder'] . DS;
        switch ($image['error']) {
            CASE UPLOAD_ERR_INI_SIZE:
                $Model->validationErrors[$field] = __('File size is too large', true);
                return false;
                break;
            CASE UPLOAD_ERR_FORM_SIZE:
                $Model->validationErrors[$field] = __('File size is too large', true);
                return false;
                break;
            CASE UPLOAD_ERR_NO_TMP_DIR:
                $Model->validationErrors[$field] = __('Cannot write to temp directory', true);
                return false;
                break;
            CASE UPLOAD_ERR_CANT_WRITE:
                $Model->validationErrors[$field] = __('Cannot write to temp directory', true);
                return false;
                break;
            CASE UPLOAD_ERR_PARTIAL:
                $Model->validationErrors[$field] = __('File could not be uploaded', true);
                return false;
                break;
            CASE UPLOAD_ERR_NO_FILE:

                if (!empty($Model->data[$Model->name]['id'])) {
                    $bnr = $Model->findById($Model->data[$Model->name]['id']);
                    $Model->data[$Model->name][$field] = $bnr[$Model->name][$field];

                    return true;
                } else {
                    if ($this->settings[$Model->name][$field]['required']) {
                        $Model->validationErrors[$field] = __('No file selected. This field is required', true);
                        return false;
                    } else {
                        $Model->data[$Model->name][$field] = '';
                        return true;
                    }
                }
                break;
        }


        $exts = array_map('low', $this->settings[$Model->name][$field]['extensions']);
        $ext = $this->__get_extension($image['name']);
        if (!in_array($ext, $exts)) {
            $Model->validationErrors[$field] = __('Invalid file type required (jpg, png, gif or tif)', true);
            return false;
        }
        $spec = $this->__get_image_dimensions($image['tmp_name']);
        if ($this->settings[$Model->name][$field]['aspect_required'] && $this->settings[$Model->name][$field]['width'] && $this->settings[$Model->name][$field]['height']) {

            $spec_aspect = $this->settings[$Model->name][$field]['width'] / $this->settings[$Model->name][$field]['height'];
            $image_aspect = $spec['width'] / $spec['height'];

            if (!($image_aspect >= $spec_aspect - $this->settings[$Model->name][$field]['aspect_tolerance'] && $image_aspect <= $spec_aspect + $this->settings[$Model->name][$field]['aspect_tolerance'])) {
                $Model->validationErrors[$field] = sprintf(__('Image with size %d x %d is required, or the same aspect', true), $this->settings[$Model->name][$field]['width'], $this->settings[$Model->name][$field]['height']);
                return false;
            }
        }

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        if (!is_writable($folder)) {

            $Model->validationErrors[$field] = __('Destination folder is not writable or never exists!', true);
            return false;
        } else {
            //$folder = $this->settings[$Model->name][$field]['folder
            $uniqid = uniqid();
            $filename = str_replace('{$rand}', $uniqid, $this->settings[$Model->name][$field]['file_name']);
            $name = substr($image['name'], 0, strrpos($image['name'], '.'));
            $ext = substr($image['name'], strrpos($image['name'], '.'));
            $filename = str_replace('{$file_name}', Inflector::slug($name) . $ext, $filename);


            if (move_uploaded_file($image['tmp_name'], $folder . $filename)) {
                $Model->data[$Model->name][$field] = $filename;
                if (!empty($this->settings[$Model->name][$field]['resize']) && $this->settings[$Model->name][$field]['resize']) {


                    if (($this->settings[$Model->name][$field]['width'] > 0 || $this->settings[$Model->name][$field]['height'] > 0)) {
                        $width = $this->settings[$Model->name][$field]['width'] > 0 ? $this->settings[$Model->name][$field]['width'] : 10000;
                        $height = $this->settings[$Model->name][$field]['height'] > 0 ? $this->settings[$Model->name][$field]['height'] : 10000;

                        $this->smart_resize_image("$folder/$filename", $width, $height, true, 'file', "$folder/$filename");
                    }

                    if ($this->settings[$Model->name][$field]['resize'] > 1 && ($this->settings[$Model->name][$field]['thumb_width'] > 0 || $this->settings[$Model->name][$field]['thumb_height'] > 0)) {
                        $width = $this->settings[$Model->name][$field]['thumb_width'] > 0 ? $this->settings[$Model->name][$field]['thumb_width'] : 10000;
                        $height = $this->settings[$Model->name][$field]['thumb_height'] > 0 ? $this->settings[$Model->name][$field]['thumb_height'] : 10000;
                        $this->smart_resize_image("$folder/$filename", $width, $height, true, 'file', "$folder/{$this->settings[$Model->name][$field]['thumb_prefix']}$filename");
                    }
                    if ($this->settings[$Model->name][$field]['resize'] > 2 && ($this->settings[$Model->name][$field]['thumb2_width'] > 0 || $this->settings[$Model->name][$field]['thumb2_height'] > 0)) {
                        $width = $this->settings[$Model->name][$field]['thumb2_width'] > 0 ? $this->settings[$Model->name][$field]['thumb2_width'] : 10000;
                        $height = $this->settings[$Model->name][$field]['thumb2_height'] > 0 ? $this->settings[$Model->name][$field]['thumb2_height'] : 10000;

                        $this->smart_resize_image("$folder/$filename", $width, $height, true, 'file', "$folder/{$this->settings[$Model->name][$field]['thumb2_prefix']}$filename");
                    }
                }

                return true;
            } else {
                $Model->validationErrors[$field] = __('Error while saving uploaded image', true);
                return false;
            }
        }
        //		}
    }

    /**
     * 	returns the extensions of a given file
     * @param string $name name of the file to get extension for
     * @return string the extension fo rthat file name
     */
    function __get_extension($name) {
        $pos = strpos($name, '.');
        if ($pos !== false) {
            return low(substr($name, $pos + 1));
        }
        return '';
    }

    function __get_prefix_folder(&$Model, $field, $data) {
        $folder_prefix = '';
        if ($this->settings[$Model->name][$field]['folder_prefix_field']
                && isset($data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']])
                && !empty($data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']])) {
            $prefix = preg_replace('/_id$/', '', $this->settings[$Model->name][$field]['folder_prefix_field']);
            $prefix = Inflector::classify($prefix);
            $folder_prefix = low($prefix) . '_' . $data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']] . '/';
        }
        return $folder_prefix;
    }

    /**
     *
     * @param string $name image file path
     * @return array an associative array with keys 'width', 'height' which has the image width and height respectively
     */
    function __get_image_dimensions($name) {
        $spec = getimagesize($name);
        $width = $spec[0];
        $height = $spec[1];
        return compact('width', 'height');
    }

    /**
     * Creates thumbnail from an image
     * @param String $file file path
     */
    function __create_thumb($file) {
        //TODO implement this function
    }

    function smart_resize_image($file, $width = 0, $height = 0, $proportional = true, $output = 'file', $file_name = false, $crop = false) {
        if ($height <= 0 && $width <= 0) {
            return false;
        }


        $info = getimagesize($file);
        $image = '';


        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;



        switch ($info[2]) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($file);
                break;
            default:
                return false;
                break;
        }

        if ($proportional == 1) {
            if ($width == 0)
                $factor = $height / $height_old;
            elseif ($height == 0)
                $factor = $width / $width_old;
            else
                $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        } else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
        }

        $image_resized = imagecreatetruecolor($final_width, $final_height);

        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $trnprt_indx = imagecolortransparent($image);
            // If we have a specific transparent color
            if ($trnprt_indx >= 0) {

                // Get the original image's transparent color's RGB values
                $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

                // Allocate the same color in the new image resource
                $trnprt_indx = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

                // Completely fill the background of the new image with allocated color.
                imagefill($image_resized, 0, 0, $trnprt_indx);

                // Set the background color for new image to transparent
                imagecolortransparent($image_resized, $trnprt_indx);
            }
            // Always make a transparent background color for PNGs that don't have one allocated already
            elseif ($info[2] == IMAGETYPE_PNG) {

                // Turn off transparency blending (temporarily)
                imagealphablending($image_resized, false);

                // Create a new transparent color for image
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);

                // Completely fill the background of the new image with allocated color.
                imagefill($image_resized, 0, 0, $color);

                // Restore transparency blending
                imagesavealpha($image_resized, true);
            }
        }

        if ($crop) {
            $orig_aspect = $width_old / $height_old;
            $new_aspect = $width / $height;
            if ($orig_aspect != $new_aspect) {
                $w_ratio = $width_old / $width;
                $h_ratio = $height_old / $height;

                $mid_w = $width;
                $mid_h = $height;

                if ($h_ratio < $w_ratio) {
                    $mid_w = $width_old * $height / $height_old;
                } else {
                    $mid_h = $height_old * $width / $width_old;
                }


                $image_mid = imagecreatetruecolor($mid_w, $mid_h);
                imagecopyresampled($image_mid, $image, 0, 0, 0, 0, $mid_w, $mid_h, $width_old, $height_old);


                $image_resized = imagecreatetruecolor($width, $height);
                $crop_w = abs($mid_w - $width) / 2;
                $crop_h = abs($mid_h - $height) / 2;

                imagecopy($image_resized, $image_mid, 0, 0, $crop_w, $crop_h, $width, $height);
            } else {
                $image_resized = $image;
            }
        } else {
            imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
        }

        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = ($file_name ? $file_name : $file);
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        switch ($info[2]) {
            case IMAGETYPE_GIF:
                imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($image_resized, $output, 100);
                break;
            case IMAGETYPE_PNG:
                imagepng($image_resized, $output);
                break;
            default:
                return false;
                break;
        }

        return true;
    }
    function set_settings(&$model, $settings) {
        $this->setup($model, $settings);
    }

}

?>