<?php

class Ad extends AppModel {

    var $name = 'Ad';
    var $filters = array('title' => 'LIKE', 'place');
    var $actsAs = array(
        'image' => array(
            'image' => array('width' => 0, 'height' => 0, 'required' => true, 'resize' => 0))
    );
    var $places = array(
        'top_left' => array('image' => array('width' => 325, 'height' => 207, 'required' => true, 'resize' => 1, 'crop' => 1)),
        'top_right' => array('image' => array('width' => 280, 'height' => 275, 'required' => true, 'resize' => 1, 'crop' => 1)),
        'center_left' => array('image' => array('width' => 150, 'height' => 95, 'required' => true, 'resize' => 1, 'crop' => true)),
        'center_right' => array('image' => array('width' => 150, 'height' => 95, 'required' => true, 'resize' => 1, 'crop' => 1)),
        'bottom_left' => array('image' => array('width' => 325, 'height' => 245, 'required' => true, 'aspect_tolerance' => '0.05', 'resize' => 1, 'crop' => 1)),
        'bottom_right' => array('image' => array('width' => 280, 'height' => 298, 'required' => true, 'resize' => 1, 'crop' => 1)),
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notEmpty', 'message' => __('required', true)),
                //'place' => array('rule' => 'notEmpty', 'message' => __('required', true)),
        );
    }

//	function getHomeRightAds() {
//		$results = $this->query('SELECT DISTINCT ON (Ad.display_order) Ad.id, Ad.image, Ad.title, Ad.url FROM ads as Ad WHERE Ad.display_order != 0 AND Ad.place = \'home_page_right\' AND Ad.active = true ORDER BY Ad.display_order, RANDOM() LIMIT 3');
//		$returnResults = array();
//		$imagesFolder = $this->getImageSettings('image', 'folder');
//		foreach ($results as $idx => $result) {
//			$returnResults[$idx][$this->name] = $result[0];
//			$returnResults[$idx][$this->name]['image_full_path'] = Router::url("/$imagesFolder/{$result[0]['image']}");
//		}
//		return $returnResults;
//	}

    public function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        //if ($this->data['Ad']['place'] == 'home_page_right'){
//        $this->validate['display_order'] = array(
//            array('rule' => 'notEmpty', 'message' => 'Display order is rquired for this position', 'required' => true),
//            array('rule' => array('range', 0, 5), 'message' => 'Please enter a number between 1 and 4')
//        );
        //}

        return true;
    }

}