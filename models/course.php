<?php

class Course extends AppModel {

    var $name = 'Course';
    var $filter = array('title' => 'like', 'short_description' => 'like');
    var $actsAs = array(
        'image' => array('image' => array(
            'width' => 0,
            'height' => 0,
            'required' => false, 'resize' => 0 , 'crop' => 0))
    );
    var $validate = array(
        'title' => array('rule' => 'notempty', 'message' => 'title cannot be left balnk'),
        'permalink' => array(array('rule' => '/[a-z][0-9a-z_\-]*/i', 'message' => 'valid perma link required', 'allowEmpty' => false),
            array('rule' => 'isUnique', 'message' => 'permalink already used')),
        'short_description' => array('rule' => array('maxLength', 500), 'message' => 'only 500 characters allowed'),
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

    function getFilters() {
        return array(); //set filters here
    }

    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = Inflector::slug($this->data[$this->name]['title'], '-');
        }
        return true;
    }

}

?>