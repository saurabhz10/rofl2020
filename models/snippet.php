<?php

class Snippet extends AppModel {

    var $name = 'Snippet';
    var $filters = array('title' => 'like');
    var $snippetss = array
        (
//        array('name' => 'contact-us', 'title' => 'Contact Us', 'advanced_editor' => true),
        array('name' => 'news_header', 'title' => 'News Header', 'advanced_editor' => true),
//        array('name' => 'contact-us-side', 'title' => 'Contact Us Side', 'advanced_editor' => true),
//        array('name' => 'staff_header', 'title' => 'Staff header', 'advanced_editor' => true),
//        array('name' => 'shopping_cart_header', 'title' => 'Shopping cart header', 'advanced_editor' => true),
//        array('name' => 'brochure_header', 'title' => 'Brochure   header', 'advanced_editor' => true),
        array('name' => 'categoriessnippet', 'title' => 'View category snippet', 'advanced_editor' => true),
        array('name' => 'links_header', 'title' => 'Links header', 'advanced_editor' => true),
        array('name' => 'education_header', 'title' => 'Education header', 'advanced_editor' => true),
        array('name' => 'portraits_header', 'title' => 'Portraits header', 'advanced_editor' => true),
        array('name' => 'testimonials_header', 'title' => 'Testimonials header', 'advanced_editor' => true),
        array('name' => 'contact-us', 'title' => 'Contact us header', 'advanced_editor' => true),
        array('name' => 'footer', 'title' => 'Footer', 'advanced_editor' => true),
        array('name' => 'landing_page', 'title' => 'Landing Page', 'advanced_editor' => true),
    );

    public function __construct($id = null, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array(array('rule' => 'notEmpty', 'message' => __('Required', true)), array('rule' => 'isUnique', 'message' => __('This name is already existed', true)))
        );
    }

}

?>