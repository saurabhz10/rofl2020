<?php

class Category extends AppModel {

    var $name = 'Category';
    var $filter = array('title' => 'like');
    var $actsAs = array(
        'image' => array('image' => array(
                'width' => 0 ,
                'height' => 0,
                'required' => false,
                'resize' => 0 ,
                'crop' => 0
            )
        )
    );
    var $validate = array(
        'title' => array('rule' => 'notempty', 'message' => 'Title cannot be left balnk'),
        'short_description' => array('rule' => array('maxLength', 500), 'message' => 'only 500 characters allowed')
    );

// var $HasMany = array(
//        'Gallery' => array('className' => 'Gallery', 'foreignKey' => 'gallery_id')
//    );

    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = Inflector::slug($this->data[$this->name]['title'], '-');
        }
        return true;
    }

}

?>