<?php

class NewsMessage extends AppModel {

    var $name = 'NewsMessage';
    var $displayField = 'subject';
    var $filters = array('subject' => 'like');
    var $hasMany = array(
        'MailLog' => array('className' => 'MailLog',
            'foreignKey' => 'news_message_id',
            'dependent' => false,
        ),
    );

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validation = array(
            'subject' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'html' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
    }

}

?>