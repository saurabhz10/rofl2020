<?php

class Gallery extends AppModel {

    var $name = 'Gallery';
    var $filters = array('title' => 'like','category_id');

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notEmpty', 'message' => 'Required'),
        );
    }
 var $hasMany = array(
        'GalleryImage' => array('className' => 'GalleryImage', 'foreignKey' => 'gallery_id')
    );
 var $belongsTo = array(
        'Category' => array('className' => 'Category', 'foreignKey' => 'category_id')
    );
    function getFilters() {
        return array('title' => 'like','category_id'); //set filters here
    }

}

?>