<?php

class Page extends AppModel {

    var $name = 'Page';
    var $filters = array('title' => 'like', 'menu_id'
            //,'access' => array('title' => 'Permission')
    );
    var $belongsTo = array(
        //'Menu' => array('className' => 'Page', 'foreignKey' => 'menu_id'),
        //'Submneu' => array('className' => 'Page', 'foreignKey' => 'submenu_id'),
        'Template' => array('className' => 'Template', 'foreignKey' => 'template_id')
    );
    var $leftSubmenu = array();

    //--------------------
    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'permalink' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
    }

    //--------------------
    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = Inflector::slug($this->data[$this->name]['title'], '-');
        }
        if (!empty($this->data[$this->name]['url'])) {
            $this->validate['url'] = array('rule' => 'notempty', 'message' => __('Required', true));
        } else {
            $this->validate['content'] = array('rule' => 'notempty', 'message' => __('Required', true));
        }
        return true;
    }

    function beforeSave() {
        if (!empty($this->data[$this->name]['has_dynamic_children'])) {
            $this->data[$this->name]['dynamic_children'] = serialize(
                    array(
                        'dc_model' => $this->data[$this->name]['dc_model'],
                        'dc_condition' => $this->data[$this->name]['dc_condition'],
                        'dc_limit' => $this->data[$this->name]['dc_limit'],
                        'dc_order' => $this->data[$this->name]['dc_order'],
                        'dc_controller' => $this->data[$this->name]['dc_controller'],
                        'dc_action' => $this->data[$this->name]['dc_action'],
                        'dc_title_field' => $this->data[$this->name]['dc_title_field'],
                        'dc_uri_field' => $this->data[$this->name]['dc_uri_field'],
                    ));
        }
        return true;
    }

    //--------------------
    public function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
         
        foreach ($results as &$result) {
//            if (!empty($result[$this->name]['attachments'])) {
//                $result['Page']['attachments'] = json_decode($result[$this->name]['attachments'], true);
//            }

            if (empty($result[$this->name]['menu_id']) && !empty($result[$this->name]['add_to_main_menu'])) {
                $result[$this->name]['Submenu'] = array();
                $conditions = array();
                $conditions['Page.active'] = 1;
                $conditions['Page.menu_id'] = $result[$this->name]['id'];
                $conditions[] = "(Page.submenu_id = 0 OR Page.submenu_id IS NULL)";
                $submenus = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                $result[$this->name]['Submenu'] = $submenus;
            }

            if (!empty($result[$this->name]['menu_id']) && empty($result[$this->name]['submenu_id']) && !empty($result[$this->name]['add_to_main_menu'])) {
                $result[$this->name]['SubSubmenu'] = array();
                $conditions = array();
                $conditions['Page.active'] = 1;
                $conditions['Page.submenu_id'] = $result[$this->name]['id'];
                $submenus = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                $result[$this->name]['SubSubmenu'] = $submenus;
            }


            if (!empty($result[$this->name]['has_dynamic_children']) && !empty($result[$this->name]['dynamic_children'])) {
                $dc = unserialize($result[$this->name]['dynamic_children']);
//                 Configure::write('debug', 2); 
//                 debug($result[$this->name]);
//                  die;
                if (is_array($dc)) {
                    $result[$this->name]['dc_model'] = $dc['dc_model'];
                    $result[$this->name]['dc_condition'] = $dc['dc_condition'];
                    $result[$this->name]['dc_limit'] = $dc['dc_limit'];
                    $result[$this->name]['dc_order'] = $dc['dc_order'];
                    $result[$this->name]['dc_controller'] = $dc['dc_controller'];
                    $result[$this->name]['dc_action'] = $dc['dc_action'];
                    $result[$this->name]['dc_title_field'] = $dc['dc_title_field'];
                    $result[$this->name]['dc_uri_field'] = $dc['dc_uri_field'];
                }


                App::import('model', $result[$this->name]['dc_model']);
                $menumodel = new $result[$this->name]['dc_model']();

                $dynmenus = $menumodel->find('all', array('conditions' => $dc['dc_condition'], 'limit' => $dc['dc_limit'], 'order' => $dc['dc_order']));

                // die(print_r($menumodel)); 
                $dynsubmenus = array();

                foreach ($dynmenus as $ik => $dynmenu) {
                    $dynsubmenus[$ik][$this->name]['title'] = $dynmenu[$result[$this->name]['dc_model']][$result[$this->name]['dc_title_field']];
                    $dynsubmenus[$ik][$this->name]['url'] = Router::url(array('controller' => $result[$this->name]['dc_controller'],
                                'action' => $result[$this->name]['dc_action'],
                                $dynmenu[$result[$this->name]['dc_model']][$result[$this->name]['dc_uri_field']]
                            ));
                    $dynsubmenus[$ik][$this->name]['is_url'] = 1;
                    $dynsubmenus[$ik][$this->name]['routed'] = 1;
                    $dynsubmenus[$ik][$this->name]['id'] = $dynmenu[$result[$this->name]['dc_model']]['id'];
                    if (isset($dynmenu[$result[$this->name]['dc_model']]['permalink']))
                        $dynsubmenus[$ik][$this->name]['permalink'] = $dynmenu[$result[$this->name]['dc_model']]['permalink'];
                }
                $menutype = 'Submenu';
                if ($result[$this->name]['menu_id'])
                    $menutype = 'SubSubmenu';
                $result[$this->name][$menutype] = $dynsubmenus;
            }

//
//            if ($result[$this->name]['id'] == 23) { //products and services 
//                App::import('model', 'CategoryMenu');
//                $cm_model = new CategoryMenu();
//                $catmenus = array();
//                foreach ($result[$this->name]['Submenu'] as $ik => $catmenu) {
//
//                    $sidemenu_modules = $cm_model->find('all', array(
//                        'conditions' =>
//                        array(
//                            'CategoryMenu.active' => '1',
//                            'category_id' => $catmenu[$this->name]['id'],
//                        ),
//                        'order' => 'CategoryMenu.display_order',
//                            )
//                            );
//
//
//
//                    $sidemenu_items = array();
//
//
//                    foreach ($sidemenu_modules as $ij => $sidemodule) {
//
//                        if ($cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['model'] == 'Page') {
//                            $module_model = & $this;
//                        } else {
//                            App::import('model', $cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['model']);
//                            $module_model = new $cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['model']();
//                        }
//
//
//                        $module_item = $module_model->findById($sidemodule['CategoryMenu']['item_id']);
//
//
//                        $sidemenu[$this->name]['title'] = $module_item[$cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['model']][$cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['label_field']];
//                        $sidemenu[$this->name]['url'] = Router::url(array('controller' => $cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['controller'], 'action' => $cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['action'], 'id' => $module_item[$cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['model']][$cm_model->modules[$sidemodule['CategoryMenu']['module_name']]['url_field']], 'category' => $sidemodule['CategoryMenu']['category_id']));
//                        $sidemenu[$this->name]['is_url'] = 1;
//                        $sidemenu[$this->name]['routed'] = 1;
//                        $sidemenu[$this->name]['id'] = $sidemodule['CategoryMenu']['item_id'];
//
//                        $sidemenu_items[] = $sidemenu;
//
//                        unset($module_model);
//                    }
//
//                    $catmenu[$this->name]['SubSubmenu'] = $sidemenu_items;
//
//                    if (isset($GLOBALS['Dispatcher']->params['named']['category']) && $GLOBALS['Dispatcher']->params['named']['category'] == $sidemodule['CategoryMenu']['category_id'] || ( $GLOBALS['Dispatcher']->params['controller'] == 'categories' && $GLOBALS['Dispatcher']->params['action'] == 'view' && $catmenu['Page']['permalink'] == $GLOBALS['Dispatcher']->params['pass'][0])) {
//                        $this->leftSubmenu = $sidemenu_items;
//                    } $catmenus[] = $catmenu;
//                }
//
//                $result[$this->name]['Submenu'] = $catmenus;
//            }
        }
        return $results;
    }

    //--------------------
    function get_sub_menus() {
        $pages = $this->find('all', array('conditions' => array('Page.submenu_id' => 0)));
        $pages2 = array();
        foreach ($pages as $i => $page) {
            $pages2['id'][$i] = $page['Page']['id'];
            $pages2['title'][$i] = $page['Page']['title'];
        }
        return $pages2;
    }

}

?>