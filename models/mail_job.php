<?php

class MailJob extends AppModel {

    var $name = 'MailJob';
    var $filters = array();
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        //'Location' => array('className' => 'Location', 'foreignKey' => 'location_id'),
        'NewsMessage' => array('className' => 'NewsMessage', 'foreignKey' => 'news_message_id')
    );
    var $hasMany = array(
        'MailLog' => array('className' => 'MailLog', 'foreignKey' => 'mail_job_id', 'dependent' => false)
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

    public function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);

        $mailLog = ClassRegistry::init('MailLog');
        foreach ($results as &$result) {
            if (!empty($results[0]['MailJob'])) {
                $result['MailJob']['not_sent_emails'] = 0;
                $result['MailJob']['sent_emails'] = 0;
                $result['MailJob']['total_emails'] = 0;

                if (!empty($result['MailJob']['id'])) {
                    $count_not_sent_emails = $mailLog->find('count', array('conditions' => array('MailLog.is_sent' => 0, 'mail_job_id' => $result['MailJob']['id'])));
                    $count_sent_emails = $mailLog->find('count', array('conditions' => array('MailLog.is_sent' => 1, 'mail_job_id' => $result['MailJob']['id'])));
                    $result['MailJob']['not_sent_emails'] = $count_not_sent_emails;
                    $result['MailJob']['sent_emails'] = $count_sent_emails;
                    $result['MailJob']['total_emails'] = $count_not_sent_emails + $count_sent_emails;
                }
            }
        }
        return $results;
    }

}

?>