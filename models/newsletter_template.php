<?php

class NewsletterTemplate extends AppModel {

    var $name = 'NewsletterTemplate';
    var $filters = array(
        'name' => 'like'
    );
    var $image_spec = array(
        'width' => '0',
        'height' => '34',
        'thumb_width' => 0,
        'thumb_height' => 34,
        'resize' => 2,
        'extensions' => array('jpg', 'png', 'gif', 'tif'),
        'folder' => 'img/brands_logos/'
    );

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'html' => array('rule' => 'notempty', 'message' => __('Required', true))
        );
    }

}

?>