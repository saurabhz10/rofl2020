<?php

class GalleryImage extends AppModel {

    var $name = 'GalleryImage';
    var $filters = array('gallery_id', 'title');
    var $actsAs = array(
        'image' => array(
            'image' => array(
                'resize' => 0,
                'width' => 0,
                'height' => 0,
                'thumb_width' => '69',
                'thumb_height' => 73,
                'required' => true,
                'aspect_required' => true,
            )
        )
    );
    var $validate = array(
        'title' => array('alphanumeric'),
        'image' => array('file'),
        'active' => array('boolean'),
        'display_order' => array('numeric')
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Gallery' => array('className' => 'Gallery', 'foreignKey' => 'gallery_id')
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notEmpty', 'message' => 'Required'),
        );
    }

    function getFilters() {
        return array(); //set filters here
    }

//    function beforeValidate($options = array()) {
//        parent::beforeValidate($options);
//        if (!empty($this->data['GalleryImage']['image'])) {
//            $image=$this->data['GalleryImage']['image'];
//            $size = getimagesize($image['tmp_name']);
//            debug($size);
//            exit;
//        }
//    }
}

?>