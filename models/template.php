<?php

class Template extends AppModel {

	var $name = 'Template';
	var $filters = array();
	var $actsAs = array(
		'image' => array(
			'image' => array('resize' => 2,
				'width' => '400',
				'height' => 0,
				'default' => false,
				'thumb_width' => '150',
				'thumb_height' => 0 ,
				'thumb_default' => false,
				'required' => true)
		)
	);

	function __construct($id = false, $table = null, $ds =null) {
		parent::__construct($id, $table, $ds);
		$this->validate = array();
	}

	function getFilters() {
		return array(); //set filters here
	}
	
	function get_templates_list(){
		$ret_templates=array();
		$templates= $this->find('all');
		foreach($templates as $i=>$template){
			$ret_templates[$template[$this->name]['id']]['key']=$template[$this->name]['id'];
			$ret_templates[$template[$this->name]['id']]['full_image']=$template[$this->name]['image_full_path'];
			$ret_templates[$template[$this->name]['id']]['thumb_image']=$template[$this->name]['image_thumb_full_path'];
			$ret_templates[$template[$this->name]['id']]['html']=$template[$this->name]['html'];
		}
		return $ret_templates;
	}

}

?>