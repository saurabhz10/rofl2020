<?php
/* @var html HtmlHelper */
$html = new HtmlHelper();
/* @var javascript JavascriptHelper */
$javascript = new JavascriptHelper();

/* @var form FormHelper */
$form = new FormHelper();
$list = new ListHelper();
$paginator = new PaginatorHelper();
$time = new TimeHelper();
$xml = new XmlHelper();
$text = new TextHelper();
$js = new JsHelper();
$session = new SessionHelper();
$mixed = new MixedHelper();
$number = new NumberHelper();
$uploader = new UploaderHelper();
?>
