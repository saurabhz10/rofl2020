<div class="FormExtended">
<?php
echo $form->create('SystemEmail', array('url' => array('action' => 'edit', $this->data['SystemEmail']['name'])));
echo $form->input('subject', array('class' => 'INPUT required'));
?>
	<div class="fck-placeholders">
	<?php
	/* @var $fck FckHelper */
	echo $fck->create('SystemEmail', 'message', '', 'Custom', false);
	?>
		<div class="placeholders" id="Placeholders">
			<ul>
			<?php foreach ($placeholders as $key => $title): ?>
				<li><strong><a href="#" class="holder-key"><?php echo $key ?> </a>:
				<?php echo $title ?> </strong></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php
	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();
	?>
</div>

<script type="text/javascript">
<!--
$(function(){
	$('.holder-key', '#Placeholders').click(function(){
		var ofck = FCKeditorAPI.GetInstance('data[SystemEmail][message]');
		ofck.InsertHtml(this.textContent);
		return false;
	});
});
//-->
</script>