<div class="systemEmails index">
	<h1><?php __('System Emails'); ?></h1>
	<?php
	//echo $list->filter_form($modelName, $filters);
	?>
	<table id="Table" cellpadding="0" cellspacing="0">
		<tr class="TableHeader">
			<td>Message Name</td>
			<td class="actions">Actions</td>
		</tr>
		<?php foreach ($systemEmails as $name => $email): ?>
			<tr>
				<td><?php echo $html->link($email['title'], array('controller'=>'system_emails','action' => 'edit', $name)); ?></td>
				<td>
					<ul class="action">
						<li><?php echo $html->link('Edit', array('controller'=>'system_emails','action' => 'edit', $name), array('class' => 'Edit', 'title' => "Edit")); ?></li>
					</ul>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

</div>