<?php
require_once dirname(dirname(dirname(__FILE__))) .DS. 'DummyView.php';
class EmailTemplateComponent extends Object {

// This component uses other components
	var $components = array('Session', 'Email');

	 function initialize(&$controller) {
        $this->controller = $controller;
    }

	
	function send_email_template($to, $from, $email_template_name, $replace) {
		$currentView = $this->controller->view;
		$this->Email->Controller = $this->controller;
		$this->controller->view = 'Dummy';
		
		$this->Email->to = $to;
		$this->Email->from = empty($from) ? ($this->config['txt.site_name'] . ' <' . $this->config['txt.send_mail_from'] . '>') : $from;
		$mailData = ClassRegistry::init('SystemEmail')->find(array('name' => $email_template_name));
		if (empty($email_template_name)) {
			return false;
		}
		$this->Email->subject = str_ireplace(array_keys($replace), $replace, $mailData['SystemEmail']['subject']);
		$message =  str_ireplace(array_keys($replace), $replace, $mailData['SystemEmail']['message']);
		$this->controller->set('message',$message);

		$this->Email->sendAs = 'html';
		$this->Email->template = 'plain';
		$this->Email->send();
		
		$this->controller->view = $currentView;
	}

}

?>
