<?php

class SystemEmailsController extends AppController {

	var $name = 'SystemEmails';

	/**
	 * @var SystemEmail
	 */
	var $SystemEmail;
	var $helpers = array('Html', 'Form');

	function admin_index() {
		$this->set('systemEmails', $this->SystemEmail->emails);
	}

	function admin_edit($name = null) {
		if (!isset($this->SystemEmail->emails[$name])){
			$this->flashMessage('Email not found');
			$this->redirect(array('action' => 'index'));
		}
		
		$systemEmail = $this->SystemEmail->find(array('name' => $name));

		if (empty($systemEmail)){
			$this->SystemEmail->create();
			$this->SystemEmail->save(array('SystemEmail' => array('name' => $name, 'subject' => '', 'message' => '')));
			$systemEmail = $this->SystemEmail->findById($this->SystemEmail->getLastInsertID());
		}

		if (!empty($this->data)) {
			$this->data['SystemEmail']['id'] = $systemEmail['SystemEmail']['id'];
			$this->data['SystemEmail']['name'] = $name;
			if ($this->SystemEmail->save($this->data)) {
				$this->flashMessage(sprintf (__('%s  has been saved', true), __('Email',true)), 'Sucmessage');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->flashMessage(sprintf (__('%s could not be saved. Please, try again', true), __('Email',true)));
				unset($this->data['SystemEmail']['id']);
			}
		}
		if (empty($this->data)) {
			$this->data = $systemEmail;
			unset($this->data['SystemEmail']['id']);
		}
		
		$this->set('placeholders', $this->SystemEmail->emails[$name]['placeholders']);

		$this->helpers[] = 'Fck';
		$this->render('admin_add');
	}


}
