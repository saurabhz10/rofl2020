<?php

class SystemEmailsAppController extends AppController {
	var $components = array('Session');
	var $helpers = array('Html', 'Form', 'Session', 'Javascript');
	
	function beforeFilter() {

		parent::beforeFilter();
	}
	function beforeRender() {
		parent::beforeRender();
	}
	
	function _send_email_template($to, $from, $email_template_name, $replace) {
		$this->Email->to = $to;
		$this->Email->from = empty($from) ? ($this->config['txt.site_name'] . ' <' . $this->config['txt.send_mail_from'] . '>') : $from;

		
		$mailData = ClassRegistry::init('SystemEmail')->find(array('name' => $email_template_name));
		if (empty($email_template_name)) {
			return false;
		}


		$this->Email->subject = str_ireplace(array_keys($replace), $replace, $mailData['SystemEmail']['subject']);
		$this->set('message', str_ireplace(array_keys($replace), $replace, $mailData['SystemEmail']['message']));

		$this->Email->sendAs = 'html';
		$this->Email->template = 'plain';

		$this->Email->send();
	}
}
?>