<?php
class FileBehavior extends ModelBehavior {
    /**
     * @var array
     */
    var $errors = array();

    var $_files_to_delete=array();
    /**
     * Defaults
     *
     * @var array
     * @access protected
     */
    var $_defaults = array(
    'file'=> array(
        'file_name'=>'{$rand}_{$file_name}', // {$rand}:random chars, {$file_name}: the original file name
        'extensions' => array('pdf', 'doc'),
        'folder' => 'files/pdfs/',
        'required'=>false,
        'delete_file_on_update'=>true,
        'max_file_size' => '20MB',
        'folder_prefix_field'=>false
        )
    );

    /**
     * Behaviour initialization
     * @param AppModel $Model
     * @param array $config
     */
    function setup(&$Model, $config = array()) {
        if(is_array($config)&&sizeof($config)) {
            foreach ($config as $field=>$options) {
                if(is_array($options))  $this->settings[$Model->name][$field]=array_merge($this->_defaults['file'],$options);
                elseif(!empty ($options)&& is_string($options)) $this->settings[$Model->name][$options]=$this->_defaults['file'];
            }
        }elseif(!empty ($config)&& is_string($config)) {
            $this->settings[$Model->name][$config]=$this->_defaults['file'];
        } else {
            $this->settings[$Model->name]=$this->_defaults;
        }
    }

    function beforeSave(&$Model) {
        $ret=true;
        $this->_files_to_delete=array();
        foreach($this->settings[$Model->name] as $field=>$options) {

            if (!empty($Model->data[$Model->name][$field]) && is_array($Model->data[$Model->name][$field])) {
                if(!$this->_upload_file($Model,$field)) {
                    $ret=false;
                } elseif(!empty($Model->data[$Model->name]['id'])){
                    //Get the current file name stored in the db to remove
                    $Model->id=$Model->data[$Model->name]['id'];
                    $old_file_name=$Model->field($field);
                    if($old_file_name!=$Model->data[$Model->name][$field]&&$options['delete_file_on_update']&&$old_file_name) {
                        $this->_files_to_delete[$field]=str_replace($Model->data[$Model->name][$field],$old_file_name,$this->getFileAbsolutePath($Model, $field, $Model->data));
                    }
                }
            }

        }
        return $ret;
    }

    function afterSave(&$Model, $created) {
        if(!$created)
        foreach($this->_files_to_delete as $field=>$filename) {
            if(file_exists($filename))
            unlink($filename);
        }
        return true;
    }

    function beforeDelete(&$Model) {
        $Model->read();
        foreach($this->settings[$Model->name] as $field=>$options) {
            if($Model->data[$Model->name][$field]&&$options['delete_file_on_update'])
            $this->_files_to_delete[$field]=$this->getFileAbsolutePath($Model, $field, $Model->data) ;
        }
        return true;
    }

    function afterDelete(&$Model) {
        foreach($this->_files_to_delete as $field=>$filename) {
            if(file_exists($filename))
            unlink($filename);
        }
        return true;
    }

    function afterFind(&$Model,$results) {


        foreach ($results as $key => $val) {
            foreach ($this->settings[$Model->name] as $field=>$options)
            if (isset($val[$Model->name][$field])&&$val[$Model->name][$field])
            {
                $prefix_folder=$this->__get_prefix_folder(&$Model, $field,$val);
                $results[$key][$Model->name][$field.'_full_path']=Router::url('/'.$prefix_folder.$options['folder'].$val[$Model->name][$field]);
            }
        }
        return $results;
    }

    /**
     * delete the file and the thumb of the image
     * @param string $field
     * @param string $file_name
     * @return boolean true on deleting file, false on error
     */
    function deleteFile(&$Model,$field,$id)
    {
        $Model->id= $id;
        $data=$Model->read();
        return  unlink($this->getFileAbsolutePath($Model,$field,$data));
    }



    /**
     * Get the file absolute path
     * @param string $field
     * @param string $file_name
     *
     * @return string full file path
     */
    function getFileAbsolutePath(&$Model,$field,$data) {
        $prefix_folder = $this->__get_prefix_folder(&$Model, $field, $data);
        return WWW_ROOT  . $prefix_folder . str_replace('/',DS,$this->settings[$Model->name][$field]['folder']).$data[$Model->name][$field];
    }
    /**
     *	Uploads a file to defined folder. On failure it assigns validation error message to the field
     * @param AppModel $Model The model to apply changes on
     * @param string   $field The name of the field to get file data from
     * @return boolean true on success uploading, false if failed for any reason
     */
    function _upload_file(&$Model, $field) {
        $file=$Model->data[$Model->name][$field];

        $prefix_folder=$this->__get_prefix_folder(&$Model, $field, $Model->data);

        $folder = WWW_ROOT . DS . $prefix_folder . $this->settings[$Model->name][$field]['folder'] . DS;
        ini_set('upload_max_filesize', $this->settings[$Model->name][$field]['max_file_size']);
        ini_set('post_max_size', $this->settings[$Model->name][$field]['max_file_size']);
        switch($file['error']) {
            CASE UPLOAD_ERR_INI_SIZE:
                $Model->validationErrors[$field] = __('File size is too large', true);
                return false;
                break;
            CASE UPLOAD_ERR_FORM_SIZE:
                $Model->validationErrors[$field] = __('File size is too large', true);
                return false;
                break;
            CASE UPLOAD_ERR_NO_TMP_DIR:
                $Model->validationErrors[$field] = __('Cannot write to temp directory', true); return false;
                break;
            CASE UPLOAD_ERR_CANT_WRITE:
                $Model->validationErrors[$field] = __('Cannot write to temp directory', true); return false;
                break;
            CASE UPLOAD_ERR_PARTIAL:
                $Model->validationErrors[$field] = __('File could not be uploaded', true); return false;
                break;
            CASE UPLOAD_ERR_NO_FILE:
                if (!empty($Model->data[$Model->name]['id'])) {
                    $bnr = $Model->findById($Model->data[$Model->name]['id']);
                    $Model->data[$Model->name][$field] = $bnr[$Model->name][$field];

                    return true;
                } else {
                    if ($this->settings[$Model->name][$field]['required']) {
                        $Model->validationErrors[$field] = __('No file selected. This field is required', true); return false;
                    } else {
                        $Model->data[$Model->name][$field] = '';
                        return true;
                    }
                }
                break;
        }

        $ext = $this->__get_extension($file['name']);
		$exts = array_map('low', $this->settings[$Model->name][$field]['extensions']);
        if (!in_array($ext, $exts)) {
            $Model->validationErrors[$field] = __(sprintf('Invalid file type. Types required %s', implode(',', $this->settings[$Model->name][$field]['extensions'])), true);
            return false;
        }

        if (!file_exists($folder)){
            mkdir($folder,0777,true);
        }

        if (!is_writable($folder)) {
            $Model->validationErrors[$field] = __('Destination folder is not writeable, or does not exist!', true);
            return false;
        } else {
            $uniqid=substr(uniqid(), 8);
            $filename=str_replace('{$rand}',$uniqid,$this->settings[$Model->name][$field]['file_name']);
			$base_name = substr($file['name'], 0, strrpos($file['name'], '.'));
			$ext = substr($file['name'], strrpos($file['name'], '.'));
			$fname = Inflector::slug($base_name) . $ext;
            $filename=str_replace('{$file_name}', $fname,$filename);

            if (move_uploaded_file($file['tmp_name'], $folder . $filename)) {
                $Model->data[$Model->name][$field] = $filename;
				chmod($folder . $filename, 0777);
                return true;
            } else {
                $Model->validationErrors[$field] = __('Error while saving uploaded file', true);
                return false;
            }
        }

    }

    /**
     *	returns the extensions of a given file
     * @param string $name name of the file to get extension for
     * @return string the extension fo rthat file name
     */
    function __get_extension($name) {
        $pos = strrpos($name, '.');
        if ($pos !== false) {
            return low(substr($name, $pos + 1));
        }
        return '';
    }


    function getFileSettings(&$Model,$field=false,$param=false)
    {
        if(!$field&&!$param)
        return  $this->settings[$Model->name];
        else if(!$param)
        return  $this->settings[$Model->name][$field];
        else
        return  $this->settings[$Model->name][$field][$param];
    }

    function setFileSettings(&$Model,$field,$param,$value)
    {
        $this->settings[$Model->name][$field][$param]=$value;
    }



    function __get_prefix_folder(&$Model,$field,$data)
    {
        $folder_prefix='';
        if($this->settings[$Model->name][$field]['folder_prefix_field']
            &&isset($data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']])
            &&!empty($data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']]))
        {
            $prefix = preg_replace('/_id$/', '', $this->settings[$Model->name][$field]['folder_prefix_field']);
			$prefix = Inflector::classify($prefix);
            $folder_prefix=low($prefix) . '_' . $data[$Model->name][$this->settings[$Model->name][$field]['folder_prefix_field']] . '/';
        }


        return $folder_prefix;
    }




}
?>