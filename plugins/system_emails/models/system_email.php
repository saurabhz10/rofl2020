<?php

class SystemEmail extends AppModel {

	var $name = 'SystemEmail';
	var $filters = array('subject'=>'like');
	public $emails = array(
		'new-user' => array(
			'title' => 'Message sent to new users',
			'placeholders' => array(
				'{%first_name%}' => 'First name',
				'{%last_name%}' => 'Last name',
				'{%email%}' => 'Email',
				'{%address1%}' => 'address1',
				'{%address2%}' => 'address2',
				'{%suburb%}' => 'Suburb',
				'{%state%}' => 'State',
				'{%postcode%}' => 'Postcode',
				'{%country%}' => 'Country',
				'{%telephone%}' => 'Telephone',
				'{%confirm-url%}' => 'Confirm URL',
				'{%login-url%}' => 'Login URL',
			)
		),
		'payment-to-client-1' => array(
			'title' => 'Payment has been successful',
			'placeholders' => array(
				'{%order-id%}' => 'Order ID',
				'{%first-name%}' => 'First name',
				'{%last-name%}' => 'Last name',
				'{%email%}' => 'Email',
				'{%address1%}' => 'Address Line 1',
				'{%address2%}' => 'Address Line 2',
				'{%suburb%}' => 'Suburb',
				'{%state%}' => 'State',
				'{%postcode%}' => 'Postcode',
				'{%country%}' => 'Country',
				'{%total-amount%}' => 'Total Amount',
				'{%status%}' => 'Status',
				'{%payment_method%}' => 'Payment method',
				'{%transaction-id%}' => 'Transaction ID',
				'{%view-link%}' => 'View payment link',
				'{%product-list%}' => 'Product List'
			)
		),
		'payment-to-client-2' => array(
			'title' => 'Payment is pending',
			'placeholders' => array(
				'{%order-id%}' => 'Order ID',
				'{%first-name%}' => 'First name',
				'{%last-name%}' => 'Last name',
				'{%email%}' => 'Email',
				'{%address1%}' => 'Address Line 1',
				'{%address2%}' => 'Address Line 2',
				'{%suburb%}' => 'Suburb',
				'{%state%}' => 'State',
				'{%postcode%}' => 'Postcode',
				'{%country%}' => 'Country',
				'{%total-amount%}' => 'Total Amount',
				'{%status%}' => 'Status',
				'{%payment_method%}' => 'Payment method',
				'{%transaction-id%}' => 'Transaction ID',
				'{%view-link%}' => 'View payment link',
				'{%product-list%}' => 'Product List'
			)
		),
		'payment-to-client-3' => array(
			'title' => 'Payment has failed',
			'placeholders' => array(
				'{%order-id%}' => 'Order ID',
				'{%first-name%}' => 'First name',
				'{%last-name%}' => 'Last name',
				'{%email%}' => 'Email',
				'{%address1%}' => 'Address Line 1',
				'{%address2%}' => 'Address Line 2',
				'{%suburb%}' => 'Suburb',
				'{%state%}' => 'State',
				'{%postcode%}' => 'Postcode',
				'{%country%}' => 'Country',
				'{%total-amount%}' => 'Total Amount',
				'{%status%}' => 'Status',
				'{%payment_method%}' => 'Payment method',
				'{%transaction-id%}' => 'Transaction ID',
				'{%view-link%}' => 'View payment link',
				'{%product-list%}' => 'Product List'
			)
		)
	);

	function __construct($id = false, $table = null, $ds =null) {
		parent::__construct($id, $table, $ds);
		$this->validate = array();
	}

	function getFilters() {
		return array(); //set filters here
	}

}
