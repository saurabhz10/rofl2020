<?php

define('CSV_SEPARATOR', ';');
define('NL', "\n");

function quote($str) {
    $str = preg_replace('/\s+/', ' ', $str);
    $str = str_replace('"', '""', $str);
    return '"' . $str . '"';
}

function quote_csv($str) {
    $str = preg_replace('/\s+/', ' ', $str);
    $str = str_replace('"', '""', $str);
    return '"' . $str . '"';
}

function format_price($price) {
    if (is_numeric($price)) {
        $price = doubleval($price);
        $config = get_config();
        return $config['txt.currency_symbol'] . $price; // . ' ' . $config['txt.currency']; 
    } else {
        return $price;
    }
}

function get_config() {
    global $ini_config;
    if (empty($ini_config) || !is_array($ini_config)) {
        $ini_config = parse_ini_file(APP . DS . 'app_config.ini');
    }
    return $ini_config;
}

function hashPassword($string) {
    return md5($string);
}

if (!function_exists('get_resized_image_url')) {

    function get_resized_image_url($image, $width = 0, $height = 0, $crop = false, $path = false, $keep_smaller = false) {
        // Configure::write('debug', 2);
        $cash_dir = 'uploads' . DS . 'cache';
        $height_str = $height ? $height : "";
        $width_str = $width ? $width : "";
        $cash_image = DS .$cash_dir . DS . "{$width}x{$height}x{$crop}_$image";

        if (file_exists(WWW_ROOT . $cash_image)) {
            return Router::url($cash_image, true);
        } else {
        $url = Router::url("/resize_image.php?image=" . urlencode(base64_encode($image)) . "&w=$width&h=$height", true);
        $url .= "&crop=$crop";
        if (!empty($path)) {
            $url .= "&path=" . urlencode($path);
        }
        if (!empty($keep_smaller)) {
            $url .= "&keep_smaller=1";
        }
        file_get_contents($url);
        if (file_exists(WWW_ROOT . $cash_image)) {
            return Router::url($cash_image, true);
        }
         return $url;
        }

        return $url;
    }

}

function generate_permalink($input) {
    $input = strtolower($input);
    $input = trim($input);
    $input = str_replace(" ", "-", $input);
    $symbols = array("!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=", "~", "`", "{", "[", "]", "}", ":", ";", "<", ",", ">", ".", "?", "|", "'");
    foreach ($symbols as $symbol)
        $input = str_replace($symbol, "", $input);
    return $input;
}

function get_today_date($format = 'Y-m-d') {
    $today = date($format);
    $session = new CakeSession;
    if ($session->check('changed_date')) {
        $date1 = $session->read('changed_date');

        $date = new DateTime();
        $date->setDate(date('Y', $date1), date('m', $date1), date('d', $date1));
        $today = $date->format($format);
    }

    return $today;
}

function strLimited($string, $limit=255, $etc='...') {
		if (strlen($string) < $limit)
			return $string;
		return substr($string, 0, max(array(strripos(substr($string, 0, $limit), ' '), strripos(substr($string, 0, $limit), "\n")))) . $etc;
	}
/**
 * Escape carriage returns and single and double quotes for JavaScript segments.
 *
 * @param string $script string that might have javascript elements
 * @return string escaped string
 */
function escapeScript($script) {
    $script = str_replace(array("\r\n", "\n", "\r"), '\n', $script);
    $script = str_replace(array('"', "'"), array('\"', "\\'"), $script);
    return $script;
}

/**
 * Escape a string to be JavaScript friendly.
 *
 * List of escaped ellements:
 * 	+ "\r\n" => '\n'
 * 	+ "\r" => '\n'
 * 	+ "\n" => '\n'
 * 	+ '"' => '\"'
 * 	+ "'" => "\\'"
 *
 * @param  string $script String that needs to get escaped.
 * @return string Escaped string.
 */
function escapeString($string) {
    $escape = array("\r\n" => '\n', "\r" => '\n', "\n" => '\n', '"' => '\"', "'" => "\\'");
    return str_replace(array_keys($escape), array_values($escape), $string);
}
?>