-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 16, 2012 at 02:21 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ptint`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_order` int(11) DEFAULT '0',
  `video` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci,
  `type` int(11) DEFAULT NULL,
  `areas` text COLLATE utf8_unicode_ci,
  `delay` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MRG_MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brochures`
--

CREATE TABLE IF NOT EXISTS `brochures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` text COLLATE latin1_general_ci NOT NULL,
  `short_description` text COLLATE latin1_general_ci,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `file` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `question` text COLLATE latin1_general_ci,
  `active` tinyint(1) NOT NULL,
  `permalink` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `hits` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `brochures`
--

INSERT INTO `brochures` (`id`, `title`, `description`, `short_description`, `year`, `month`, `image`, `file`, `question`, `active`, `permalink`, `hits`, `created`, `modified`) VALUES
(1, 'Lorem Ipsum 2010', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 2010, 10, '4ca85dba69ba4_mag_ads.jpg', '98901_SOld_Section_Blank_Final.pdf', 'Vivamus consequat tincidunt quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in', 0, 'lorem-ipsum-2010', 3, '2010-10-03 16:29:42', '2010-10-03 16:29:42'),
(2, 'November 2010', 'ong established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English', NULL, 2010, 9, '4ca85dc7e1258_mag_ads.jpg', '6350e_Lorem_Ipsum.pdf', 'What is Lorem Ipsum', 1, 'november-2010', 0, '2010-10-03 16:35:33', '2010-10-03 16:35:33'),
(3, 'August 2010', 'ong established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English', NULL, 2010, 8, '4ca98c2352ccc_mag_ads.jpg', '8708d_Lorem_Ipsum.pdf', 'What is Lorem Ipsum', 1, 'august-2010', 0, '2010-10-04 16:11:15', '2010-10-04 16:11:15'),
(4, 'july 2011', '<h3>Principal Magazine a publication designed especially for  Real Estate Business Owners and Department Managers.&nbsp; We provide you  with content that is relevant to your position, plus a networking forum  where you can share thoughts, ideas and network with other leaders in  your industry.</h3>\r\n<p>We hope you enjoy reading Principal. &nbsp;You can read this months issue  online, or access prior editions through the archives index on the left  hand side of the page.&nbsp; While you are here, drop us your opinion on  various topics included in the forum!</p>', 'ong established fact that a reader will be distracted by the readable content of a page when looking readable English ong established fact that a reader will be distracted by the readable when looking readable English ong established fact that a read', 2012, 7, '4ca98c521ae52_mag_ads.jpg', '3f2e9_SOld_Section_Blank_Final.pdf', 'What is Lorem Ipsum', 1, 'july-2011', 0, '2010-10-04 16:12:02', '2010-10-04 16:12:02'),
(5, 'New Year Revaluation  ', 'long established fact that a reader will be distracted by the readable content of a page when looking at its layout', NULL, 2010, 12, '4ca9bab0914e1_1.gif', 'a647f_first.pdf', 'Awesome New Year Revaluation are you ?', 1, 'new-year-revaluation', 0, '2010-10-04 19:29:52', '2010-10-04 19:29:52'),
(9, 'title', '<p>details</p>', 'description', 2012, 3, '4f6eddef6d9c9_test5.jpg', '26267_Principal_11_single_page.pdf', NULL, 1, 'title', 0, '2012-03-25 11:57:19', '2012-03-25 11:57:19'),
(10, 'title 2 ', '<p>title 2 </p>', 'title 2 ', 2012, 1, '4f6ee53093d3e_test5.jpg', '6fe3e_Principal_11_single_page.pdf', NULL, 1, 'title-2', 0, '2012-03-25 12:28:16', '2012-03-25 12:28:16'),
(11, 'title 22', '<p>title 22</p>', 'title 22', 2012, 5, '4f6ee5980c3f3_test5.jpg', '82638_Principal_11_single_page.pdf', NULL, 1, 'title-22', 0, '2012-03-25 12:30:00', '2012-03-25 12:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `brochure_pages`
--

CREATE TABLE IF NOT EXISTS `brochure_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `brochure_id` int(11) NOT NULL,
  `pdf` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=237 ;

--
-- Dumping data for table `brochure_pages`
--

INSERT INTO `brochure_pages` (`id`, `title`, `description`, `brochure_id`, `pdf`, `image`, `url`, `active`, `display_order`, `hits`, `created`, `modified`) VALUES
(1, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 1, 'a2573_Lorem_Ipsum.pdf', '4cadb8da045c5_chapter.gif', NULL, 1, 1, 1, '2010-10-06 20:48:01', '2010-10-19 15:15:32'),
(2, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, 'a6948_Lorem_Ipsum.pdf', '4cadb8e735231_chapter.gif', NULL, 1, 2, 2, '2010-10-06 20:48:30', '2010-10-06 20:51:23'),
(3, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, '56107_Lorem_Ipsum.pdf', '4cadb9031a468_chapter.gif', NULL, 1, 1, 1, '2010-10-07 15:30:04', '2010-10-07 15:31:13'),
(4, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, '094c6_Lorem_Ipsum.pdf', '4cadb8f926b81_chapter.gif', NULL, 1, 2, 0, '2010-10-07 15:31:03', '2010-10-07 15:31:13'),
(38, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', 'http://www.sold-magazine.com.au', 1, 10, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(36, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', 'http://www.sold-magazine.com.au', 1, 8, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(37, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', 'http://www.sold-magazine.com.au', 1, 9, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(35, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(34, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(33, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(32, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(31, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(30, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(29, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', 'http://www.sold-magazine.com.au', 1, 1, 3, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(39, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(40, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', 'http://www.sold-magazine.com.au', 1, 1, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(41, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(42, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(43, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(44, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', 'http://www.sold-magazine.com.au', 1, 5, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(45, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(46, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(47, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(48, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(49, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(50, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(51, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', '', 1, 1, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(52, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(53, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(54, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(55, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(56, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(57, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(58, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(59, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(60, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(61, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(62, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', '', 1, 1, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(63, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(64, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(65, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(66, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(67, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(68, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(69, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(70, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(71, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(72, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(73, 'test', 'test', 1, '3bd7e_SOld_Section_Blank_Final_4.pdf', '4cb40caf9977c_image_0001.jpg', NULL, 1, 2, 0, '2010-10-12 15:22:23', '2010-10-19 15:15:32'),
(74, 'new test 2', 'new test 2', 1, '057bd_Lorem_Ipsum.pdf', '4cb41baf52e69_image_0001.jpg', NULL, 1, 3, 0, '2010-10-12 16:26:23', '2010-10-19 15:15:32'),
(117, 'Page 1', ' ', 9, '26267_Principal_11_single_page_0001.pdf', '26267_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(118, 'Page 2', ' ', 9, '26267_Principal_11_single_page_0002.pdf', '26267_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(119, 'Page 3', ' ', 9, '26267_Principal_11_single_page_0003.pdf', '26267_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(120, 'Page 4', ' ', 9, '26267_Principal_11_single_page_0004.pdf', '26267_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(121, 'Page 5', ' ', 9, '26267_Principal_11_single_page_0005.pdf', '26267_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(122, 'Page 6', ' ', 9, '26267_Principal_11_single_page_0006.pdf', '26267_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(123, 'Page 7', ' ', 9, '26267_Principal_11_single_page_0007.pdf', '26267_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(124, 'Page 8', ' ', 9, '26267_Principal_11_single_page_0008.pdf', '26267_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(125, 'Page 9', ' ', 9, '26267_Principal_11_single_page_0009.pdf', '26267_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(126, 'Page 10', ' ', 9, '26267_Principal_11_single_page_0010.pdf', '26267_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(127, 'Page 11', ' ', 9, '26267_Principal_11_single_page_0011.pdf', '26267_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(128, 'Page 12', ' ', 9, '26267_Principal_11_single_page_0012.pdf', '26267_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(129, 'Page 13', ' ', 9, '26267_Principal_11_single_page_0013.pdf', '26267_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(130, 'Page 14', ' ', 9, '26267_Principal_11_single_page_0014.pdf', '26267_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(131, 'Page 15', ' ', 9, '26267_Principal_11_single_page_0015.pdf', '26267_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(132, 'Page 16', ' ', 9, '26267_Principal_11_single_page_0016.pdf', '26267_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(133, 'Page 17', ' ', 9, '26267_Principal_11_single_page_0017.pdf', '26267_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(134, 'Page 18', ' ', 9, '26267_Principal_11_single_page_0018.pdf', '26267_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(135, 'Page 19', ' ', 9, '26267_Principal_11_single_page_0019.pdf', '26267_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(136, 'Page 20', ' ', 9, '26267_Principal_11_single_page_0020.pdf', '26267_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(137, 'Page 1', ' ', 10, '6fe3e_Principal_11_single_page_0001.pdf', '6fe3e_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(138, 'Page 2', ' ', 10, '6fe3e_Principal_11_single_page_0002.pdf', '6fe3e_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(139, 'Page 3', ' ', 10, '6fe3e_Principal_11_single_page_0003.pdf', '6fe3e_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(140, 'Page 4', ' ', 10, '6fe3e_Principal_11_single_page_0004.pdf', '6fe3e_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(141, 'Page 5', ' ', 10, '6fe3e_Principal_11_single_page_0005.pdf', '6fe3e_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(142, 'Page 6', ' ', 10, '6fe3e_Principal_11_single_page_0006.pdf', '6fe3e_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(143, 'Page 7', ' ', 10, '6fe3e_Principal_11_single_page_0007.pdf', '6fe3e_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(144, 'Page 8', ' ', 10, '6fe3e_Principal_11_single_page_0008.pdf', '6fe3e_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(145, 'Page 9', ' ', 10, '6fe3e_Principal_11_single_page_0009.pdf', '6fe3e_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(146, 'Page 10', ' ', 10, '6fe3e_Principal_11_single_page_0010.pdf', '6fe3e_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(147, 'Page 11', ' ', 10, '6fe3e_Principal_11_single_page_0011.pdf', '6fe3e_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(148, 'Page 12', ' ', 10, '6fe3e_Principal_11_single_page_0012.pdf', '6fe3e_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(149, 'Page 13', ' ', 10, '6fe3e_Principal_11_single_page_0013.pdf', '6fe3e_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(150, 'Page 14', ' ', 10, '6fe3e_Principal_11_single_page_0014.pdf', '6fe3e_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(151, 'Page 15', ' ', 10, '6fe3e_Principal_11_single_page_0015.pdf', '6fe3e_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(152, 'Page 16', ' ', 10, '6fe3e_Principal_11_single_page_0016.pdf', '6fe3e_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(153, 'Page 17', ' ', 10, '6fe3e_Principal_11_single_page_0017.pdf', '6fe3e_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(154, 'Page 18', ' ', 10, '6fe3e_Principal_11_single_page_0018.pdf', '6fe3e_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(155, 'Page 19', ' ', 10, '6fe3e_Principal_11_single_page_0019.pdf', '6fe3e_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(156, 'Page 20', ' ', 10, '6fe3e_Principal_11_single_page_0020.pdf', '6fe3e_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(236, 'Page 20', ' ', 11, '82638_Principal_11_single_page_0020.pdf', '82638_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(235, 'Page 19', ' ', 11, '82638_Principal_11_single_page_0019.pdf', '82638_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(234, 'Page 18', ' ', 11, '82638_Principal_11_single_page_0018.pdf', '82638_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(233, 'Page 17', ' ', 11, '82638_Principal_11_single_page_0017.pdf', '82638_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(232, 'Page 16', ' ', 11, '82638_Principal_11_single_page_0016.pdf', '82638_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(231, 'Page 15', ' ', 11, '82638_Principal_11_single_page_0015.pdf', '82638_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(230, 'Page 14', ' ', 11, '82638_Principal_11_single_page_0014.pdf', '82638_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(229, 'Page 13', ' ', 11, '82638_Principal_11_single_page_0013.pdf', '82638_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(228, 'Page 12', ' ', 11, '82638_Principal_11_single_page_0012.pdf', '82638_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(227, 'Page 11', ' ', 11, '82638_Principal_11_single_page_0011.pdf', '82638_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(226, 'Page 10', ' ', 11, '82638_Principal_11_single_page_0010.pdf', '82638_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(225, 'Page 9', ' ', 11, '82638_Principal_11_single_page_0009.pdf', '82638_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(224, 'Page 8', ' ', 11, '82638_Principal_11_single_page_0008.pdf', '82638_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(223, 'Page 7', ' ', 11, '82638_Principal_11_single_page_0007.pdf', '82638_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(222, 'Page 6', ' ', 11, '82638_Principal_11_single_page_0006.pdf', '82638_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(221, 'Page 5', ' ', 11, '82638_Principal_11_single_page_0005.pdf', '82638_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(220, 'Page 4', ' ', 11, '82638_Principal_11_single_page_0004.pdf', '82638_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(219, 'Page 3', ' ', 11, '82638_Principal_11_single_page_0003.pdf', '82638_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(218, 'Page 2', ' ', 11, '82638_Principal_11_single_page_0002.pdf', '82638_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(217, 'Page 1', ' ', 11, '82638_Principal_11_single_page_0001.pdf', '82638_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `permalink` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `permalink`, `description`, `display_order`) VALUES
(1, 'Sign', 'sign', '<p>Consequat non tortor. Donec a elit diam, ut interdum purus. Nam  fringilla dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas  eros neque, fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris  libero. In tristique tincidunt est.Consequat non tortor. Donec a elit  diam, ut interdum purus. Nam fringilla</p>', 1),
(2, 'My Category', 'my-category', '<p>my category</p>', 41);

-- --------------------------------------------------------

--
-- Table structure for table `category_menus`
--

CREATE TABLE IF NOT EXISTS `category_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `category_menus`
--

INSERT INTO `category_menus` (`id`, `module_name`, `item_id`, `category_id`, `display_order`, `active`) VALUES
(1, 'Page', 4, 2, 54, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `product` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `first_name`, `last_name`, `email`, `message`, `product`, `created`, `modified`) VALUES
(1, 'Amr', 'Elsaqqa', 'amrelsaqqa@hotmail.com', 'Hello, Here I am !', NULL, '2012-08-12 13:53:32', '2012-08-12 13:53:32'),
(2, 'asdasd', 'asd', 'asdasd@yahoo.com', 'asd', NULL, '2012-08-14 10:49:58', '2012-08-14 10:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`code`, `name`) VALUES
('AD', 'Andorra'),
('AE', 'United Arab Emirates'),
('AF', 'Afghanistan'),
('AG', 'Antigua And Barbuda'),
('AI', 'Anguilla'),
('AL', 'Albania'),
('AM', 'Armenia'),
('AN', 'Netherlands Antilles'),
('AO', 'Angola'),
('AQ', 'Antarctica'),
('AR', 'Argentina'),
('AS', 'American Samoa'),
('AT', 'Austria'),
('AU', 'Australia'),
('AW', 'Aruba'),
('AZ', 'Azerbaijan'),
('BA', 'Bosnia And Herzegowina'),
('BB', 'Barbados'),
('BD', 'Bangladesh'),
('BE', 'Belgium'),
('BF', 'Burkina Faso'),
('BG', 'Bulgaria'),
('BH', 'Bahrain'),
('BI', 'Burundi'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BN', 'Brunei Darussalam'),
('BO', 'Bolivia'),
('BR', 'Brazil'),
('BS', 'Bahamas'),
('BT', 'Bhutan'),
('BV', 'Bouvet Island'),
('BW', 'Botswana'),
('BY', 'Belarus'),
('BZ', 'Belize'),
('CA', 'Canada'),
('CC', 'Cocos Keeling Islands'),
('CD', 'Congo The Democratic Republic Of The'),
('CF', 'Central African Republic'),
('CG', 'Congo'),
('CH', 'Switzerland'),
('CI', 'Cote D Ivoire'),
('CK', 'Cook Islands'),
('CL', 'Chile'),
('CM', 'Cameroon'),
('CN', 'China'),
('CO', 'Colombia'),
('CR', 'Costa Rica'),
('CU', 'Cuba'),
('CV', 'Cape Verde'),
('CX', 'Christmas Island'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DE', 'Germany'),
('DJ', 'Djibouti'),
('DK', 'Denmark'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('DZ', 'Algeria'),
('EC', 'Ecuador'),
('EE', 'Estonia'),
('EG', 'Egypt'),
('EH', 'Western Sahara'),
('ER	', 'Eritrea'),
('ES', 'Spain'),
('ET', 'Ethiopia'),
('FI', 'Finland'),
('FJ', 'Fiji'),
('FK', 'Falkland Islands Malvinas '),
('FM', 'Micronesia Federated States Of'),
('FO', 'Faroe Islands'),
('FR', 'France'),
('FX', 'France Metropolitan'),
('GA', 'Gabon'),
('GB', 'United Kingdom'),
('GD', 'Grenada'),
('GE', 'Georgia'),
('GF', 'French Guiana'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GL', 'Greenland'),
('GM', 'Gambia'),
('GN', 'Guinea'),
('GP', 'Guadeloupe'),
('GQ', 'Equatorial Guinea'),
('GR', 'Greece'),
('GS', 'South Georgia And The South Sandwich Islands'),
('GT', 'Guatemala'),
('GU', 'Guam'),
('GW', 'Guinea Bissau'),
('GY', 'Guyana'),
('HK', 'Hong Kong'),
('HM', 'Heard And Mc Donald Islands'),
('HN', 'Honduras'),
('HR', 'Croatia Local Name Hrvatska '),
('HT', 'Haiti'),
('HU', 'Hungary'),
('ID', 'Indonesia'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IN', 'India'),
('IO', 'British Indian Ocean Territory'),
('IQ', 'Iraq'),
('IR', 'Iran Islamic Republic Of '),
('IS', 'Iceland'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JO', 'Jordan'),
('JP', 'Japan'),
('KE', 'Kenya'),
('KG', 'Kyrgyzstan'),
('KH', 'Cambodia'),
('KI', 'Kiribati'),
('KM', 'Comoros'),
('KN', 'Saint Kitts And Nevis'),
('KP', 'Korea Democratic People S Republic Of'),
('KR', 'Korea Republic Of'),
('KW', 'Kuwait'),
('KY', 'Cayman Islands'),
('KZ', 'Kazakhstan'),
('LA', 'Lao People S Democratic Republic'),
('LB', 'Lebanon'),
('LC', 'Saint Lucia'),
('LI', 'Liechtenstein'),
('LK', 'Sri Lanka'),
('LR', 'Liberia'),
('LS', 'Lesotho'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('LV', 'Latvia'),
('LY', 'Libyan Arab Jamahiriya'),
('MA', 'Morocco'),
('MC', 'Monaco'),
('MD', 'Moldova Republic Of'),
('MG', 'Madagascar'),
('MH', 'Marshall Islands'),
('MK', 'Macedonia The Former Yugoslav Republic Of'),
('ML', 'Mali'),
('MM', 'Myanmar'),
('MN', 'Mongolia'),
('MO', 'Macau'),
('MP', 'Northern Mariana Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MS', 'Montserrat'),
('MT', 'Malta'),
('MU', 'Mauritius'),
('MV', 'Maldives'),
('MW', 'Malawi'),
('MX', 'Mexico'),
('MY', 'Malaysia'),
('MZ', 'Mozambique'),
('NA', 'Namibia'),
('NC', 'New Caledonia'),
('NE', 'Niger'),
('NF', 'Norfolk Island'),
('NG', 'Nigeria'),
('NI', 'Nicaragua'),
('NL', 'Netherlands'),
('NO', 'Norway'),
('NP', 'Nepal'),
('NR', 'Nauru'),
('NU', 'Niue'),
('NZ', 'New Zealand'),
('OM', 'Oman'),
('PA', 'Panama'),
('PE', 'Peru'),
('PF', 'French Polynesia'),
('PG', 'Papua New Guinea'),
('PH', 'Philippines'),
('PK', 'Pakistan'),
('PL', 'Poland'),
('PM', 'St Pierre And Miquelon'),
('PN', 'Pitcairn'),
('PR', 'Puerto Rico'),
('PT', 'Portugal'),
('PW', 'Palau'),
('PY', 'Paraguay'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SA', 'Saudi Arabia'),
('SB', 'Solomon Islands'),
('SC', 'Seychelles'),
('SD', 'Sudan'),
('SE', 'Sweden'),
('SG', 'Singapore'),
('SH', 'St Helena'),
('SI', 'Slovenia'),
('SJ', 'Svalbard And Jan Mayen Islands'),
('SK', 'Slovakia Slovak Republic '),
('SL', 'Sierra Leone'),
('SM', 'San Marino'),
('SN', 'Senegal'),
('SO', 'Somalia'),
('SR', 'Suriname'),
('ST', 'Sao Tome And Principe'),
('SV', 'El Salvador'),
('SY', 'Syrian Arab Republic'),
('SZ', 'Swaziland'),
('TC', 'Turks And Caicos Islands'),
('TD', 'Chad'),
('TF', 'French Southern Territories'),
('TG', 'Togo'),
('TH', 'Thailand'),
('TJ', 'Tajikistan'),
('TK', 'Tokelau'),
('TM', 'Turkmenistan'),
('TN', 'Tunisia'),
('TO', 'Tonga'),
('TP', 'East Timor'),
('TR', 'Turkey'),
('TT', 'Trinidad And Tobago'),
('TV', 'Tuvalu'),
('TW', 'Taiwan Province Of China'),
('TZ', 'Tanzania United Republic Of'),
('UA', 'Ukraine'),
('UG', 'Uganda'),
('UM', 'United States Minor Outlying Islands'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VA', 'Holy See Vatican City State '),
('VC', 'Saint Vincent And The Grenadines'),
('VE', 'Venezuela'),
('VG', 'Virgin Islands British '),
('VI', 'Virgin Islands U S '),
('VN', 'Viet Nam'),
('VU', 'Vanuatu'),
('WF', 'Wallis And Futuna Islands'),
('WS', 'Samoa'),
('YE', 'Yemen'),
('YT', 'Mayotte'),
('YU', 'Yugoslavia'),
('ZA', 'South Africa'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `description`, `active`, `display_order`, `keywords`, `created`, `updated`) VALUES
(1, 'asdasd', 'asdasd', 1, 0, 'asdasd', '2012-08-13 12:46:24', '2012-08-13 12:46:24'),
(2, 'qweqew', 'qweqwe', 1, 12, '213', '2012-08-13 13:24:09', '2012-08-13 13:24:09'),
(3, 'asd', 'asdasd', 1, 12, 'sdasd', '2012-08-13 13:28:23', '2012-08-13 13:28:23'),
(4, 'asdas', 'dasdasd', 1, 12, 'sad ', '2012-08-13 13:47:02', '2012-08-13 13:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `title`, `description`, `image`, `active`, `display_order`, `gallery_id`, `created`, `updated`) VALUES
(1, 'tristique ', ' ut interdum purus. Nam fringilla dapibus tellus, at sollicitudin lectus  Donec a elit diam, Donec a elit diam', '502ca20cbb691_Penguins.jpg', 1, 1, 1, '2012-08-16 09:32:28', '2012-08-16 09:32:28'),
(2, ' Donec ', ' Donec a elit diam, ut interdum purus. Nam fringilla dapibus tellus, at sollicitudin lectus . Nam fringilla dapibus tellus, at sollicitudin lectus . Nam fringilla dapibus tellus, at sollicitudin lectus ', '502ca20d9cf61_Tulips.jpg', 1, 2, 1, '2012-08-16 09:32:29', '2012-08-16 09:32:29'),
(3, 'lectus ', ' Nam fringilla dapibus tellus,Donec a elit diam, ut interdum purus. at sollicitudin ,Donec a elit diam, ut interdum purus. at sollicitudin ', '502ca20e152e4_Chrysanthemum.jpg', 1, 3, 1, '2012-08-16 09:32:30', '2012-08-16 09:32:30'),
(4, 'tincidunt ', 'Fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In tristique ,Maecenas eros neque,', '502ca20e8a2c6_Hydrangeas.jpg', 1, 4, 1, '2012-08-16 09:32:30', '2012-08-16 09:32:30'),
(5, 'tristique ', 'Maecenas eros neque, fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In ', '502ca20f0a7f7_Koala.jpg', 1, 5, 1, '2012-08-16 09:32:31', '2012-08-16 09:32:31'),
(9, 'With total', 'With total design control, eCommerce features, superior SEO results and free domains, Wix is the ultimate solution for creating your perfect and exquisite HTML website.', '502cab3973dfe_img9.jpg', 1, 2, 2, '2012-08-16 10:11:37', '2012-08-16 10:11:37'),
(10, 'Donec', 'eCommerce features, superior SEO results and free domains, Wix is the ultimate solution for creating your perfect and exquisite', '502cab399ef10_img19.jpg', 1, 4, 2, '2012-08-16 10:11:37', '2012-08-16 10:11:37'),
(11, 'Commerce', ' superior SEO results and free domains, Wix is the ultimate solution for creating your perfect and exquisite', '502cab39ca15e_img14.jpg', 1, 5, 2, '2012-08-16 10:11:37', '2012-08-16 10:11:37'),
(12, 'exquisite', ' superior SEO results and free domains, Wix is the ultimate solution for creating your perfect and exquisite', '502cad7a09473_Hydrangeas.jpg', 1, 23, 2, '2012-08-16 10:21:14', '2012-08-16 10:21:14');

-- --------------------------------------------------------

--
-- Table structure for table `magazines`
--

CREATE TABLE IF NOT EXISTS `magazines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` text COLLATE latin1_general_ci NOT NULL,
  `short_description` text COLLATE latin1_general_ci,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `file` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `question` text COLLATE latin1_general_ci,
  `active` tinyint(1) NOT NULL,
  `permalink` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `hits` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `magazines`
--

INSERT INTO `magazines` (`id`, `title`, `description`, `short_description`, `year`, `month`, `image`, `file`, `question`, `active`, `permalink`, `hits`, `created`, `modified`) VALUES
(1, 'Lorem Ipsum 2010', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 2010, 10, '4ca85dba69ba4_mag_ads.jpg', '98901_SOld_Section_Blank_Final.pdf', 'Vivamus consequat tincidunt quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in', 0, 'lorem-ipsum-2010', 3, '2010-10-03 16:29:42', '2010-10-03 16:29:42'),
(2, 'November 2010', 'ong established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English', NULL, 2010, 9, '4ca85dc7e1258_mag_ads.jpg', '6350e_Lorem_Ipsum.pdf', 'What is Lorem Ipsum', 1, 'november-2010', 0, '2010-10-03 16:35:33', '2010-10-03 16:35:33'),
(3, 'August 2010', 'ong established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English', NULL, 2010, 8, '4ca98c2352ccc_mag_ads.jpg', '8708d_Lorem_Ipsum.pdf', 'What is Lorem Ipsum', 1, 'august-2010', 0, '2010-10-04 16:11:15', '2010-10-04 16:11:15'),
(4, 'july 2011', '<h3>Principal Magazine a publication designed especially for  Real Estate Business Owners and Department Managers.&nbsp; We provide you  with content that is relevant to your position, plus a networking forum  where you can share thoughts, ideas and network with other leaders in  your industry.</h3>\r\n<p>We hope you enjoy reading Principal. &nbsp;You can read this months issue  online, or access prior editions through the archives index on the left  hand side of the page.&nbsp; While you are here, drop us your opinion on  various topics included in the forum!</p>', 'ong established fact that a reader will be distracted by the readable content of a page when looking readable English ong established fact that a reader will be distracted by the readable when looking readable English ong established fact that a read', 2012, 7, '4ca98c521ae52_mag_ads.jpg', '3f2e9_SOld_Section_Blank_Final.pdf', 'What is Lorem Ipsum', 1, 'july-2011', 0, '2010-10-04 16:12:02', '2010-10-04 16:12:02'),
(5, 'New Year Revaluation  ', 'long established fact that a reader will be distracted by the readable content of a page when looking at its layout', NULL, 2010, 12, '4ca9bab0914e1_1.gif', 'a647f_first.pdf', 'Awesome New Year Revaluation are you ?', 1, 'new-year-revaluation', 0, '2010-10-04 19:29:52', '2010-10-04 19:29:52'),
(9, 'title', '<p>details</p>', 'description', 2012, 3, '4f6eddef6d9c9_test5.jpg', '26267_Principal_11_single_page.pdf', NULL, 1, 'title', 0, '2012-03-25 11:57:19', '2012-03-25 11:57:19'),
(10, 'title 2 ', '<p>title 2 </p>', 'title 2 ', 2012, 1, '4f6ee53093d3e_test5.jpg', '6fe3e_Principal_11_single_page.pdf', NULL, 1, 'title-2', 0, '2012-03-25 12:28:16', '2012-03-25 12:28:16'),
(11, 'title 22', '<p>title 22</p>', 'title 22', 2012, 5, '4f6ee5980c3f3_test5.jpg', '82638_Principal_11_single_page.pdf', NULL, 1, 'title-22', 0, '2012-03-25 12:30:00', '2012-03-25 12:30:00'),
(12, 'my title', '<p>full description</p>', 'short description', 2009, 1, '502b709460613_333.gif', 'ae02e_testpdf.pdf', NULL, 1, 'my-title', 0, '2012-08-15 11:49:08', '2012-08-15 11:49:08'),
(13, 'my title', '<p>asdasdasd</p>', 'asdasd', 2012, 1, '502cc13120f62_333.gif', '246d2_6350e_Lorem_Ipsum.pdf', NULL, 1, 'my-title', 0, '2012-08-16 11:45:21', '2012-08-16 11:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `magazine_pages`
--

CREATE TABLE IF NOT EXISTS `magazine_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `magazine_id` int(11) NOT NULL,
  `pdf` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=343 ;

--
-- Dumping data for table `magazine_pages`
--

INSERT INTO `magazine_pages` (`id`, `title`, `description`, `magazine_id`, `pdf`, `image`, `url`, `active`, `display_order`, `hits`, `created`, `modified`) VALUES
(1, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 1, 'a2573_Lorem_Ipsum.pdf', '4cadb8da045c5_chapter.gif', NULL, 1, 1, 1, '2010-10-06 20:48:01', '2010-10-19 15:15:32'),
(2, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, 'a6948_Lorem_Ipsum.pdf', '4cadb8e735231_chapter.gif', NULL, 1, 2, 2, '2010-10-06 20:48:30', '2010-10-06 20:51:23'),
(3, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, '56107_Lorem_Ipsum.pdf', '4cadb9031a468_chapter.gif', NULL, 1, 1, 1, '2010-10-07 15:30:04', '2010-10-07 15:31:13'),
(4, ' quis luctus metus. In hac habitasse', 'quam eget suscipit. Nam nec tellus dolor, quis luctus metus. In hac habitasse platea dictumst. Nam quis neque in justo rutrum ultricies. Etiam ', 2, '094c6_Lorem_Ipsum.pdf', '4cadb8f926b81_chapter.gif', NULL, 1, 2, 0, '2010-10-07 15:31:03', '2010-10-07 15:31:13'),
(38, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', 'http://www.sold-magazine.com.au', 1, 10, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(36, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', 'http://www.sold-magazine.com.au', 1, 8, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(37, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', 'http://www.sold-magazine.com.au', 1, 9, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(35, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(34, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(33, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(32, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(31, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(30, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(29, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', 'http://www.sold-magazine.com.au', 1, 1, 3, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(39, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:15:49', '2011-01-12 09:08:21'),
(40, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', 'http://www.sold-magazine.com.au', 1, 1, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(41, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(42, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(43, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(44, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', 'http://www.sold-magazine.com.au', 1, 5, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(45, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(46, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(47, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(48, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(49, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(50, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:31:05', '2011-01-12 09:08:21'),
(51, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', '', 1, 1, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(52, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(53, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(54, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(55, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(56, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(57, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(58, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(59, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(60, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(61, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:45:13', '2011-01-12 09:08:21'),
(62, 'Page_0001', 'Page_0001', 4, '3f2e9_SOld_Section_Blank_Final_0001.pdf', '3f2e9_SOld_Section_Blank_Final_0001.jpg', '', 1, 1, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(63, 'Page_0002', 'Page_0002', 4, '3f2e9_SOld_Section_Blank_Final_0002.pdf', '3f2e9_SOld_Section_Blank_Final_0002.jpg', '', 1, 2, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(64, 'Page_0003', 'Page_0003', 4, '3f2e9_SOld_Section_Blank_Final_0003.pdf', '3f2e9_SOld_Section_Blank_Final_0003.jpg', '', 1, 3, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(65, 'Page_0004', 'Page_0004', 4, '3f2e9_SOld_Section_Blank_Final_0004.pdf', '3f2e9_SOld_Section_Blank_Final_0004.jpg', '', 1, 4, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(66, 'Page_0005', 'Page_0005', 4, '3f2e9_SOld_Section_Blank_Final_0005.pdf', '3f2e9_SOld_Section_Blank_Final_0005.jpg', '', 1, 5, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(67, 'Page_0006', 'Page_0006', 4, '3f2e9_SOld_Section_Blank_Final_0006.pdf', '3f2e9_SOld_Section_Blank_Final_0006.jpg', '', 1, 6, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(68, 'Page_0007', 'Page_0007', 4, '3f2e9_SOld_Section_Blank_Final_0007.pdf', '3f2e9_SOld_Section_Blank_Final_0007.jpg', '', 1, 7, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(69, 'Page_0008', 'Page_0008', 4, '3f2e9_SOld_Section_Blank_Final_0008.pdf', '3f2e9_SOld_Section_Blank_Final_0008.jpg', '', 1, 8, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(70, 'Page_0009', 'Page_0009', 4, '3f2e9_SOld_Section_Blank_Final_0009.pdf', '3f2e9_SOld_Section_Blank_Final_0009.jpg', '', 1, 9, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(71, 'Page_0010', 'Page_0010', 4, '3f2e9_SOld_Section_Blank_Final_0010.pdf', '3f2e9_SOld_Section_Blank_Final_0010.jpg', '', 1, 10, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(72, 'Page_0011', 'Page_0011', 4, '3f2e9_SOld_Section_Blank_Final_0011.pdf', '3f2e9_SOld_Section_Blank_Final_0011.jpg', '', 1, 11, 0, '2010-10-07 22:45:59', '2011-01-12 09:08:21'),
(73, 'test', 'test', 1, '3bd7e_SOld_Section_Blank_Final_4.pdf', '4cb40caf9977c_image_0001.jpg', NULL, 1, 2, 0, '2010-10-12 15:22:23', '2010-10-19 15:15:32'),
(74, 'new test 2', 'new test 2', 1, '057bd_Lorem_Ipsum.pdf', '4cb41baf52e69_image_0001.jpg', NULL, 1, 3, 0, '2010-10-12 16:26:23', '2010-10-19 15:15:32'),
(117, 'Page 1', ' ', 9, '26267_Principal_11_single_page_0001.pdf', '26267_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(118, 'Page 2', ' ', 9, '26267_Principal_11_single_page_0002.pdf', '26267_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(119, 'Page 3', ' ', 9, '26267_Principal_11_single_page_0003.pdf', '26267_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(120, 'Page 4', ' ', 9, '26267_Principal_11_single_page_0004.pdf', '26267_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(121, 'Page 5', ' ', 9, '26267_Principal_11_single_page_0005.pdf', '26267_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(122, 'Page 6', ' ', 9, '26267_Principal_11_single_page_0006.pdf', '26267_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(123, 'Page 7', ' ', 9, '26267_Principal_11_single_page_0007.pdf', '26267_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(124, 'Page 8', ' ', 9, '26267_Principal_11_single_page_0008.pdf', '26267_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(125, 'Page 9', ' ', 9, '26267_Principal_11_single_page_0009.pdf', '26267_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(126, 'Page 10', ' ', 9, '26267_Principal_11_single_page_0010.pdf', '26267_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(127, 'Page 11', ' ', 9, '26267_Principal_11_single_page_0011.pdf', '26267_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(128, 'Page 12', ' ', 9, '26267_Principal_11_single_page_0012.pdf', '26267_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(129, 'Page 13', ' ', 9, '26267_Principal_11_single_page_0013.pdf', '26267_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(130, 'Page 14', ' ', 9, '26267_Principal_11_single_page_0014.pdf', '26267_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(131, 'Page 15', ' ', 9, '26267_Principal_11_single_page_0015.pdf', '26267_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(132, 'Page 16', ' ', 9, '26267_Principal_11_single_page_0016.pdf', '26267_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(133, 'Page 17', ' ', 9, '26267_Principal_11_single_page_0017.pdf', '26267_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(134, 'Page 18', ' ', 9, '26267_Principal_11_single_page_0018.pdf', '26267_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(135, 'Page 19', ' ', 9, '26267_Principal_11_single_page_0019.pdf', '26267_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(136, 'Page 20', ' ', 9, '26267_Principal_11_single_page_0020.pdf', '26267_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:27:37', '2012-03-25 12:27:37'),
(137, 'Page 1', ' ', 10, '6fe3e_Principal_11_single_page_0001.pdf', '6fe3e_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(138, 'Page 2', ' ', 10, '6fe3e_Principal_11_single_page_0002.pdf', '6fe3e_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(139, 'Page 3', ' ', 10, '6fe3e_Principal_11_single_page_0003.pdf', '6fe3e_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(140, 'Page 4', ' ', 10, '6fe3e_Principal_11_single_page_0004.pdf', '6fe3e_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(141, 'Page 5', ' ', 10, '6fe3e_Principal_11_single_page_0005.pdf', '6fe3e_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(142, 'Page 6', ' ', 10, '6fe3e_Principal_11_single_page_0006.pdf', '6fe3e_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(143, 'Page 7', ' ', 10, '6fe3e_Principal_11_single_page_0007.pdf', '6fe3e_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(144, 'Page 8', ' ', 10, '6fe3e_Principal_11_single_page_0008.pdf', '6fe3e_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(145, 'Page 9', ' ', 10, '6fe3e_Principal_11_single_page_0009.pdf', '6fe3e_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(146, 'Page 10', ' ', 10, '6fe3e_Principal_11_single_page_0010.pdf', '6fe3e_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(147, 'Page 11', ' ', 10, '6fe3e_Principal_11_single_page_0011.pdf', '6fe3e_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(148, 'Page 12', ' ', 10, '6fe3e_Principal_11_single_page_0012.pdf', '6fe3e_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(149, 'Page 13', ' ', 10, '6fe3e_Principal_11_single_page_0013.pdf', '6fe3e_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(150, 'Page 14', ' ', 10, '6fe3e_Principal_11_single_page_0014.pdf', '6fe3e_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(151, 'Page 15', ' ', 10, '6fe3e_Principal_11_single_page_0015.pdf', '6fe3e_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(152, 'Page 16', ' ', 10, '6fe3e_Principal_11_single_page_0016.pdf', '6fe3e_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(153, 'Page 17', ' ', 10, '6fe3e_Principal_11_single_page_0017.pdf', '6fe3e_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(154, 'Page 18', ' ', 10, '6fe3e_Principal_11_single_page_0018.pdf', '6fe3e_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(155, 'Page 19', ' ', 10, '6fe3e_Principal_11_single_page_0019.pdf', '6fe3e_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(156, 'Page 20', ' ', 10, '6fe3e_Principal_11_single_page_0020.pdf', '6fe3e_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:28:28', '2012-03-25 12:28:28'),
(236, 'Page 20', ' ', 11, '82638_Principal_11_single_page_0020.pdf', '82638_Principal_11_single_page_0020.jpg', NULL, 1, 20, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(235, 'Page 19', ' ', 11, '82638_Principal_11_single_page_0019.pdf', '82638_Principal_11_single_page_0019.jpg', NULL, 1, 19, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(234, 'Page 18', ' ', 11, '82638_Principal_11_single_page_0018.pdf', '82638_Principal_11_single_page_0018.jpg', NULL, 1, 18, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(233, 'Page 17', ' ', 11, '82638_Principal_11_single_page_0017.pdf', '82638_Principal_11_single_page_0017.jpg', NULL, 1, 17, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(232, 'Page 16', ' ', 11, '82638_Principal_11_single_page_0016.pdf', '82638_Principal_11_single_page_0016.jpg', NULL, 1, 16, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(231, 'Page 15', ' ', 11, '82638_Principal_11_single_page_0015.pdf', '82638_Principal_11_single_page_0015.jpg', NULL, 1, 15, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(230, 'Page 14', ' ', 11, '82638_Principal_11_single_page_0014.pdf', '82638_Principal_11_single_page_0014.jpg', NULL, 1, 14, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(229, 'Page 13', ' ', 11, '82638_Principal_11_single_page_0013.pdf', '82638_Principal_11_single_page_0013.jpg', NULL, 1, 13, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(228, 'Page 12', ' ', 11, '82638_Principal_11_single_page_0012.pdf', '82638_Principal_11_single_page_0012.jpg', NULL, 1, 12, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(227, 'Page 11', ' ', 11, '82638_Principal_11_single_page_0011.pdf', '82638_Principal_11_single_page_0011.jpg', NULL, 1, 11, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(226, 'Page 10', ' ', 11, '82638_Principal_11_single_page_0010.pdf', '82638_Principal_11_single_page_0010.jpg', NULL, 1, 10, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(225, 'Page 9', ' ', 11, '82638_Principal_11_single_page_0009.pdf', '82638_Principal_11_single_page_0009.jpg', NULL, 1, 9, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(224, 'Page 8', ' ', 11, '82638_Principal_11_single_page_0008.pdf', '82638_Principal_11_single_page_0008.jpg', NULL, 1, 8, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(223, 'Page 7', ' ', 11, '82638_Principal_11_single_page_0007.pdf', '82638_Principal_11_single_page_0007.jpg', NULL, 1, 7, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(222, 'Page 6', ' ', 11, '82638_Principal_11_single_page_0006.pdf', '82638_Principal_11_single_page_0006.jpg', NULL, 1, 6, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(221, 'Page 5', ' ', 11, '82638_Principal_11_single_page_0005.pdf', '82638_Principal_11_single_page_0005.jpg', NULL, 1, 5, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(220, 'Page 4', ' ', 11, '82638_Principal_11_single_page_0004.pdf', '82638_Principal_11_single_page_0004.jpg', NULL, 1, 4, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(219, 'Page 3', ' ', 11, '82638_Principal_11_single_page_0003.pdf', '82638_Principal_11_single_page_0003.jpg', NULL, 1, 3, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(218, 'Page 2', ' ', 11, '82638_Principal_11_single_page_0002.pdf', '82638_Principal_11_single_page_0002.jpg', NULL, 1, 2, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(217, 'Page 1', ' ', 11, '82638_Principal_11_single_page_0001.pdf', '82638_Principal_11_single_page_0001.jpg', NULL, 1, 1, 0, '2012-03-25 12:45:51', '2012-03-25 12:45:51'),
(331, 'Page 11', ' ', 12, 'ae02e_testpdf_0011.pdf', 'ae02e_testpdf_0011.jpg', NULL, 1, 11, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(330, 'Page 10', ' ', 12, 'ae02e_testpdf_0010.pdf', 'ae02e_testpdf_0010.jpg', NULL, 1, 10, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(329, 'Page 9', ' ', 12, 'ae02e_testpdf_0009.pdf', 'ae02e_testpdf_0009.jpg', NULL, 1, 9, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(328, 'Page 8', ' ', 12, 'ae02e_testpdf_0008.pdf', 'ae02e_testpdf_0008.jpg', NULL, 1, 8, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(327, 'Page 7', ' ', 12, 'ae02e_testpdf_0007.pdf', 'ae02e_testpdf_0007.jpg', NULL, 1, 7, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(326, 'Page 6', ' ', 12, 'ae02e_testpdf_0006.pdf', 'ae02e_testpdf_0006.jpg', NULL, 1, 6, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(325, 'Page 5', ' ', 12, 'ae02e_testpdf_0005.pdf', 'ae02e_testpdf_0005.jpg', NULL, 1, 5, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(324, 'Page 4', ' ', 12, 'ae02e_testpdf_0004.pdf', 'ae02e_testpdf_0004.jpg', NULL, 1, 4, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(323, 'Page 3', ' ', 12, 'ae02e_testpdf_0003.pdf', 'ae02e_testpdf_0003.jpg', NULL, 1, 3, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(321, 'Page 1', ' ', 12, 'ae02e_testpdf_0001.pdf', 'ae02e_testpdf_0001.jpg', NULL, 1, 1, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(322, 'Page 2', ' ', 12, 'ae02e_testpdf_0002.pdf', 'ae02e_testpdf_0002.jpg', NULL, 1, 2, 0, '2012-08-15 15:41:15', '2012-08-15 15:41:15'),
(332, 'Page 1', ' ', 13, '246d2_6350e_Lorem_Ipsum_0001.pdf', '246d2_6350e_Lorem_Ipsum_0001.jpg', NULL, 1, 1, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(333, 'Page 2', ' ', 13, '246d2_6350e_Lorem_Ipsum_0002.pdf', '246d2_6350e_Lorem_Ipsum_0002.jpg', NULL, 1, 2, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(334, 'Page 3', ' ', 13, '246d2_6350e_Lorem_Ipsum_0003.pdf', '246d2_6350e_Lorem_Ipsum_0003.jpg', NULL, 1, 3, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(335, 'Page 4', ' ', 13, '246d2_6350e_Lorem_Ipsum_0004.pdf', '246d2_6350e_Lorem_Ipsum_0004.jpg', NULL, 1, 4, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(336, 'Page 5', ' ', 13, '246d2_6350e_Lorem_Ipsum_0005.pdf', '246d2_6350e_Lorem_Ipsum_0005.jpg', NULL, 1, 5, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(337, 'Page 6', ' ', 13, '246d2_6350e_Lorem_Ipsum_0006.pdf', '246d2_6350e_Lorem_Ipsum_0006.jpg', NULL, 1, 6, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(338, 'Page 7', ' ', 13, '246d2_6350e_Lorem_Ipsum_0007.pdf', '246d2_6350e_Lorem_Ipsum_0007.jpg', NULL, 1, 7, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(339, 'Page 8', ' ', 13, '246d2_6350e_Lorem_Ipsum_0008.pdf', '246d2_6350e_Lorem_Ipsum_0008.jpg', NULL, 1, 8, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(340, 'Page 9', ' ', 13, '246d2_6350e_Lorem_Ipsum_0009.pdf', '246d2_6350e_Lorem_Ipsum_0009.jpg', NULL, 1, 9, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(341, 'Page 10', ' ', 13, '246d2_6350e_Lorem_Ipsum_0010.pdf', '246d2_6350e_Lorem_Ipsum_0010.jpg', NULL, 1, 10, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34'),
(342, 'Page 11', ' ', 13, '246d2_6350e_Lorem_Ipsum_0011.pdf', '246d2_6350e_Lorem_Ipsum_0011.jpg', NULL, 1, 11, 0, '2012-08-16 11:46:34', '2012-08-16 11:46:34');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `publish_at` date DEFAULT NULL,
  `keywords` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `permalink`, `short_description`, `content`, `publish_at`, `keywords`, `image`, `display_order`, `active`, `modified`) VALUES
(1, 'First News', 'first-news', 'this is some stuff about this news item', '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left"><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/184x284/888/fff&amp;text=3:4" alt=""></td>\r\n            <td width="40">&nbsp;</td>\r\n            <td valign="top" align="left">\r\n            <h2 class="helvetica">Product</h2>\r\n            <h4 class="helvetica">Lorem ipsum joh lorem.</h4>\r\n            <p>Our Company was established in 1990 by Some Person, who prior 																worked for the Jorgenson-Waring Group which was founded by his 																father John in 1965. Our Company is part of the Waring Group in 																Melbourne Australia and is a family business where the John and 																both brothers Chris and Andrew work with Michael. <br>\r\n            <br>\r\n            Travelling extensively in the 																early to mid 90&rsquo;s Michael identified that the future in raw material 																surviving was to be &lsquo;Food Safety&rsquo;. In early 1992 Michael coined 																the phrase on which all Our Company growth would rest &ldquo;Best Source Safe 																Foods&rdquo;.<br>\r\n            <br>\r\n            Our Company has an exceptional 																reputation within the Nut and Dried Fruit industry. In addition 																to the Our Company business, the Waring Group have investments 																in Macadamia farms in Australia, nut processing facilities in 																Vietnam and other associated entitles.</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan="3">\r\n            <h2 class="helvetica">Small Header</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source titive suppliers.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="highlight-table">\r\n    <tbody>\r\n        <tr>\r\n            <td colspan="3">\r\n            <h3 class="helvetica">Lorem Ipsum (Head Office)</h3>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td valign="top">\r\n            <p>Pelaco Building</p>\r\n            <p>Ground Floor Building 2</p>\r\n            <p>21 &ndash; 31 Goodwood Street</p>\r\n            </td>\r\n            <td valign="top">\r\n            <p>PO Box 7088</p>\r\n            <p>Richmond VIC 2131</p>\r\n            <p><strong>Tel:</strong> +61 3 9420 2900</p>\r\n            <p><strong>Fax:</strong> +61 3 9421 0507</p>\r\n            </td>\r\n            <td valign="top">\r\n            <p><strong>Email:</strong> info@mwtfoods.com</p>\r\n            <p><strong>Website:</strong> www.mwtfoods.com</p>\r\n            <a href="#" class="bbtn"><span>Contact Our Team</span></a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Natus error sit 																voluptatem accusantium dolore.</h4>\r\n            <p>hasellus non sapien interdum metus rhoncus blandit sit amet 																vel nunc. Ut nec nisl lacus. Proin consectetur tristique libero. 																Nunrhoncus sodales faucibus. Quisque justo mi, ullamcorper accumsan 																cursus quis, lobortis vitae neque. Phasellus porttitor, diam 																sed posuere molestie, neque ante porttitor ipsum, quis sodales 																lacus neque pharetra ligula. Ut nec odio ac libero bibendum pharetra 																quis ut nulla. Maecenas vitae nisl odio. Nunc ac dui turpis, 																at sodales ipsum. Suspendisse a sagittis purus. Vivamus porttitor 																tincidunt nisi, vel viverra tortor facilisis sit amet. Maecenas 																feugiat posuere felis vel dignissim. Proin id dolor sem, quis 																vestibulum lectus. Suspendisse pellentesque faucibus nisl, non 																pellentesque justo feugiat at. Donec risus erat, gravida sed 																sollicitudin a, sodales nec velit. Cras cursus volutpat egestas.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h2 class="helvetica">More Products</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source titive suppliers.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top">\r\n            <p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&amp;text=4:3" alt=""></p>\r\n            <h4 class="helvetica">Pepitas.</h4>\r\n            <p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit.  												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur</p>\r\n            </td>\r\n            <td width="85">&nbsp;</td>\r\n            <td valign="top">\r\n            <p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&amp;text=4:3" alt=""></p>\r\n            <h4 class="helvetica">Pepitas.</h4>\r\n            <p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit.  												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur</p>\r\n            </td>\r\n            <td width="85">&nbsp;</td>\r\n            <td valign="top">\r\n            <p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&amp;text=4:3" alt=""></p>\r\n            <h4 class="helvetica">Pepitas.</h4>\r\n            <p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit.  												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;</p>', '2012-08-15', 'news, test, beta', '502ba7525bb33_Hydrangeas.jpg', 4, 1, '2012-08-15 16:24:32'),
(2, 'News without image', 'news-without-image', 'some short description', '<p>all contents.. </p>', '2012-08-15', '', '', NULL, 1, '2012-08-15 15:39:17'),
(3, 'another news', 'another-news', 'some another news details', '<p>entire story</p>', '2012-08-15', '', '502ba7c7d600f_Lighthouse.jpg', NULL, 1, '2012-08-15 15:44:39'),
(4, 'tomorrow news', 'tomorrow-news', 'iSA ', '<p>ISA</p>', '2012-08-16', '', '502ba7fed2eb3_Tulips.jpg', NULL, 1, '2012-08-15 15:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created`, `modified`) VALUES
(1, 'amrelsaqqa@hotmail.com', '2012-08-12 15:10:20', '2012-08-12 15:10:20'),
(2, 'ahmed@s.com', '2012-08-12 15:15:28', '2012-08-12 15:15:28'),
(3, 'z@gfr.com', '2012-08-12 15:17:22', '2012-08-12 15:17:22'),
(4, 'abc@hytp.com', '2012-08-12 15:18:20', '2012-08-12 15:18:20'),
(5, '34534@ewr.wer', '2012-08-14 10:06:59', '2012-08-14 10:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `telephone` varchar(50) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `total` float NOT NULL,
  `gst` float NOT NULL,
  `paid` float DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `responsecode` varchar(50) NOT NULL,
  `pay_method` varchar(255) DEFAULT NULL,
  `order_status` int(11) NOT NULL,
  `freight_id` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `first_name`, `last_name`, `email`, `address1`, `address2`, `city`, `state`, `country`, `postcode`, `telephone`, `mobile`, `fax`, `total`, `gst`, `paid`, `transaction_id`, `responsecode`, `pay_method`, `order_status`, `freight_id`, `created`, `modified`) VALUES
(1, 'asd', 'asdf', 'fasd@asasd.com', 'asdf', 'asdas', 'asdf', 'asd', 'AS', '0231', '1231546979851321', '454654654', '4654-6512-56', 0, 0, NULL, ';', 'N/A', 'offline', 0, 0, '2012-08-14 16:22:50', '2012-08-14 16:22:50'),
(2, 'asd', 'asdf', 'fasd@asasd.com', 'asdf', 'sdf', 'asdf', 'asdfasdf', 'DZ', '0231', '1231546979851321', '454654654', '4654-6512-56', 1270, 25, NULL, NULL, '', 'paypal', 0, 0, '2012-08-14 16:25:39', '2012-08-14 16:25:39'),
(3, 'ahmed', 'tarek', 'ahmed@yahoo.com', 'street1', 'street2', 'cairo', 'gizza', 'AS', '02311231', '1231546979851321', '454-654654', '4654-6512-56', 1500, 24, NULL, NULL, '', 'paymate', 0, 0, '2012-08-15 09:30:56', '2012-08-15 09:30:56'),
(4, 'ahmed', 'tarek', 'ahmed@yahoo.com', 'street1', 'street2', 'cairo', 'gizza', 'AS', '02311231', '1231546979851321', '454-654654', '4654-6512-56', 1500, 24, NULL, NULL, '', 'paypal', 0, 0, '2012-08-15 09:32:00', '2012-08-15 09:32:00'),
(5, 'ahmed', 'tarek', 'ahmed@yahoo.com', 'street1', 'street2', 'cairo', 'gizza', 'AS', '02311231', '1231546979851321', '454-654654', '4654-6512-56', 1500, 24, NULL, NULL, '', 'offline2', 0, 0, '2012-08-15 09:32:28', '2012-08-15 09:32:28'),
(6, 'ahmed', 'tarek', 'ahmed@yahoo.com', 'street1', 'street2', 'cairo', 'gizza', 'AS', '02311231', '1231546979851321', '454-654654', '4654-6512-56', 1500, 24, NULL, ';', 'N/A', 'offline', 0, 0, '2012-08-15 09:32:51', '2012-08-15 09:32:51'),
(7, 'ahmed1', 'saeed', 'ahmed3@yahoo.com', 'street1', 'street2', 'cairo', 'afsd', 'DZ', '02311231', '1231546979851321', '454654654', 'asf', 10000, 1, NULL, NULL, '', 'offline2', 0, 0, '2012-08-15 09:36:37', '2012-08-15 09:36:37'),
(8, 'ahmed', 'tarek', 'ahmed3@yahoo.com', 'street1', 'street2', 'cairo', 'gizza', 'CO', '02311231', '1231546979851321', '454-654654', '4654-6512-56', 10100, 1, NULL, NULL, '', 'offline', 0, 0, '2012-08-15 09:39:51', '2012-08-15 09:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `subtotal` float NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `orders_products`
--

INSERT INTO `orders_products` (`id`, `order_id`, `product_id`, `price`, `subtotal`, `qty`) VALUES
(1, 2, 1, 10, 70, 7),
(2, 2, 2, 240, 1200, 5),
(3, 3, 2, 240, 1200, 5),
(4, 3, 3, 100, 300, 3),
(5, 4, 2, 240, 1200, 5),
(6, 4, 3, 100, 300, 3),
(7, 5, 2, 240, 1200, 5),
(8, 5, 3, 100, 300, 3),
(9, 6, 2, 240, 1200, 5),
(10, 6, 3, 100, 300, 3),
(11, 7, 1, 10, 10000, 1000),
(12, 8, 1, 10, 10000, 1000),
(13, 8, 3, 100, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `keywords` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `template_id` int(11) DEFAULT '0',
  `menu_id` int(11) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT '0',
  `add_to_main_menu` tinyint(1) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_url` tinyint(1) NOT NULL,
  `access` int(11) DEFAULT '0' COMMENT '0=>Public,1=>members only,2=>Both',
  `attachments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) DEFAULT NULL,
  `default` tinyint(1) NOT NULL,
  `has_dynamic_children` tinyint(1) NOT NULL DEFAULT '0',
  `dynamic_children` mediumtext COLLATE utf8_unicode_ci,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_on_table` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `perma_link` (`permalink`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `permalink`, `content`, `keywords`, `description`, `template_id`, `menu_id`, `submenu_id`, `add_to_main_menu`, `url`, `is_url`, `access`, `attachments`, `active`, `display_order`, `default`, `has_dynamic_children`, `dynamic_children`, `created`, `modified`, `type`, `id_on_table`) VALUES
(2, 'About Us', 'About-Us', '<h1 class="helvetica">About Us</h1>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Natus error sit 																voluptatem accusantium dolore.</h4>\r\n            <p>Our Company was established in 1990 by Some Person, who prior 																worked for the Jorgenson-Waring Group which was founded by his 																father John in 1965. Our Company is part of the Waring Group in 																Melbourne Australia and is a family business where the John and 																both brothers Chris and Andrew work with Michael. <br>\r\n            <br>\r\n            Travelling extensively in the 																early to mid 90&rsquo;s Michael identified that the future in raw material 																surviving was to be &lsquo;Food Safety&rsquo;. In early 1992 Michael coined 																the phrase on which all Our Company growth would rest &ldquo;Best Source Safe 																Foods&rdquo;.<br>\r\n            <br>\r\n            Our Company has an exceptional 																reputation within the Nut and Dried Fruit industry. In addition 																to the Our Company business, the Waring Group have investments 																in Macadamia farms in Australia, nut processing facilities in 																Vietnam and other associated entitles.</p>\r\n            </td>\r\n            <td width="23">&nbsp;</td>\r\n            <td valign="top" align="right"><img alt="" src="http://test.silvertrees.net/test-mm/dummyimage/235x176/888/fff&amp;text=4:3"></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left" colspan="3">\r\n            <h2 class="helvetica">Mission 																Statement</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source of quality product ensuring maximum 																differentiation from its competitive suppliers. <br>\r\n            <br>\r\n            Our Company will achieve this goal 																through the formation of &ldquo;Partnership&rdquo; relationships with its 																suppliers and customers. The criteria for these relationships 																will revolve around its mission, to always strive under the edict 																of &ldquo;Best Source Safe Food&rdquo;.<br>\r\n            The &ldquo;Supplier Partnership&rdquo; relationship 																will be two way orientated. Information transfer between supplier 																and Our Company will facilitate correct servicing of the customer 																in all respects. Our Company will endeavor to assist and influence 																the investment by its suppliers in best practice systems and 																infrastructure. Such influence may be borne through direct investment 																in such supplier businesses, formation of joint ventures or exclusive 																supply agreements. <br>\r\n            <br>\r\n            The &ldquo;Customer Partnership&rdquo; relationship 																will involve Our Company establishing a close and confidential 																working relationship in which Our Company will directly assist 																in establishing new products and better supply channels for its 																customers. <br>\r\n            <br>\r\n            Our Company will be renowned as 																a high service supplier to its customers&rsquo; needs. Service to customers&rsquo; 																includes correct and accurate detail on market information and 																proven and sound logistics options. Such will culminate in &ldquo;on-time 																delivery&rdquo;, warranty in quality and performance, and</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td valign="top" align="left"><img alt="" src="http://test.silvertrees.net/test-mm/dummyimage/318x240/888/fff&amp;text=4:3"></td>\r\n            <td width="23">&nbsp;</td>\r\n            <td valign="top" align="left">\r\n            <p>Our Company was established in 1990 																by Some Person, who prior worked for the Jorgenson-Waring 																Group which was founded by his father John in 1965. Our Company 																is part of the Waring Group in Melbourne Australia and is a family 																business where the John and both brothers Chris and Andrew work 																with Michael. <br>\r\n            <br>\r\n            Travelling extensively in the 																early to mid 90&rsquo;s Michael identified that the future in raw material 																surviving was to be &lsquo;Food Safety&rsquo;. In early 1992 Michael coined 																the phrase on which all Our Company growth would rest &ldquo;Best Source Safe 																Foods&rdquo;.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h2 class="helvetica">Our Commitment</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source of quality product ensuring maximum 																differentiation from its competitive suppliers.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '', '', 0, NULL, 0, 1, '', 1, 0, '{"documents":["1","3"]}', 1, 2, 1, 0, 'a:7:{s:8:"dc_model";s:8:"Category";s:12:"dc_condition";s:0:"";s:8:"dc_limit";s:1:"3";s:13:"dc_controller";s:10:"categories";s:9:"dc_action";s:4:"view";s:14:"dc_title_field";s:5:"title";s:12:"dc_uri_field";s:9:"permalink";}', '2011-10-04 18:13:59', '2012-08-16 14:07:08', NULL, NULL),
(3, 'Contact Us', 'Contact-Us', '', '', '', 0, NULL, 22, 1, '/contact', 1, 0, '{"documents":["1","2","3","4","5"],"links":["2","3","4"]}', 1, 7, 1, 0, NULL, '2011-10-04 18:15:28', '2012-08-16 14:06:36', NULL, NULL),
(4, 'Privacy Policy', 'Privacy-Policy', '<h1 class="helvetica">Privacy Policy</h1>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h3 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h3>\r\n            <p>und the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,  but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&amp;text=3:4" alt=""></td>\r\n            <td width="40">&nbsp;</td>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h4>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br>\r\n            <br>\r\n            Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h4>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br>\r\n            <br>\r\n            Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n            </td>\r\n            <td width="40">&nbsp;</td>\r\n            <td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&amp;text=3:4" alt=""></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&amp;text=3:4" alt=""></td>\r\n            <td width="40">&nbsp;</td>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h4>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br>\r\n            <br>\r\n            Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h3 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h3>\r\n            <p>und the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,  but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '', '', 0, NULL, NULL, 0, '', 0, 0, '{"documents":["1","3","4","5"],"links":["1","2","3"],"videos":["9","10"]}', 0, 0, 1, 0, NULL, '2011-10-04 18:15:58', '2012-08-14 14:27:46', NULL, NULL),
(6, 'Home', 'Home', '<h1 class="helvetica">Welcome to Bridal Belle Boutique</h1>\r\n<h3 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h3>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien.<br>\r\n            <br>\r\n            Auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congueto the Our Company business, the Waring Group have investments 																in Macadamia farms in Australia, nut processing facilities in 																Vietnam and other associated entitles.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left" colspan="3">\r\n            <h3 class="helvetica">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis.</h3>\r\n            <p>Auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue. lectus. Lorem ipsum dolor. <br>\r\n            of itself, because it is pain, but because occasionally circumstances occur in whslectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh aclectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac.</p>\r\n            </td>\r\n            <td width="23">&nbsp;</td>\r\n            <td valign="top" align="left"><img alt="" src="http://test.silvertrees.net/test-mm/dummyimage/318x240/888/fff&amp;text=4:3"></td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '', '', 0, NULL, 0, 1, '/', 1, 0, '{"documents":["2","3","5","6"],"links":["1","2","3","4"],"videos":["9","10"]}', 1, 1, 1, 0, NULL, '2011-10-05 13:58:11', '2012-08-14 14:27:46', NULL, NULL),
(22, 'Quality', 'quality', '<h1 class="helvetica">Quality</h1>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h3><span style="font-family: Tahoma;"><font face="Times New Roman"><span style="font-family: Verdana;"><span style="font-size: larger;">Industry Leading Quality DIY Framelss, Semi Frameless Pool Fencing and Balustrade Kits</span></span></font></span></h3>\r\n<p>&nbsp;</p>\r\n<h3>&nbsp;</h3>\r\n<h3><span style="font-family: Tahoma;"><font face="Times New Roman"><span style="font-family: Verdana;"><span style="font-size: larger;">Standard Glass sizes are as follows&nbsp;</span></span></font></span></h3>\r\n<h3>&nbsp;</h3>\r\n<p>&nbsp;<b><span style="font-family: Verdana;"><span style="font-size: larger;">10MM- SEMI FRAMELESS-&nbsp;</span></span></b><span style="font-family: Verdana;"><span style="font-size: larger;"><a href="http://www.technoglassdesigns.com/category.php?cat_id=66&amp;is_new="><b>CLICK </b></a></span><b><span style="font-size: larger;">TO SEE&nbsp;COMPONENTS&nbsp;</span></b></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1706MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1646MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1586MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1466MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1346MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1226MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1106MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">986MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">866MM X 1170MM</span></span></p>\r\n<p>&nbsp;</p>\r\n<p><b><span style="font-family: Verdana;"><span style="font-size: larger;">10MM BALUSTRADE GLASS- </span></span></b><span style="font-family: Verdana;"><span style="font-size: larger;"><a href="http://www.technoglassdesigns.com/category.php?cat_id=67&amp;is_new="><b>CLICK</b></a></span><b><span style="font-size: larger;"> TO SEE COMPONENTS</span></b></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1646MM X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1586MM X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1466MM X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1346MM&nbsp;X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1226MM X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1106MM X 920MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">986MM X 920MM&nbsp;</span></span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><b><span style="font-family: Verdana;"><span style="font-size: larger;">12MM FRAMELESS GLASS WITH HOLES- </span></span></b><span style="font-family: Verdana;"><span style="font-size: larger;"><a href="http://www.technoglassdesigns.com/category.php?cat_id=68&amp;is_new="><b>CLICK</b></a></span><b><span style="font-size: larger;"> TO SEE COMPONENTS</span></b></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">2080MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1960MM x 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1840MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1720MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1600MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1480MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1240MM X 1170MM</span></span></p>\r\n<p>&nbsp;</p>\r\n<p><b><span style="font-family: Verdana;"><span style="font-size: larger;">12MM FRAMELESS GLASS WITHOUT HOLES</span></span></b></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">2080MM X 1170MM</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">1960MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1840MM X 1170MM</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">1720MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1600MM X 1170MM</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">1480MM X 1170MM</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1240MM X 1170MM</span></span></p>\r\n<p>&nbsp;</p>\r\n<p><b><span style="font-size: larger;"><span style="font-family: Verdana;">GATE SIZES- </span></span></b><span style="font-size: larger;"><span style="font-family: Verdana;"><a href="http://www.technoglassdesigns.com/category.php?cat_id=69&amp;is_new="><b>CLICK</b></a><b> TO SEE FRAMELESS GATE COMPONENTS, </b><a href="http://www.technoglassdesigns.com/category.php?cat_id=71&amp;is_new="><b>CLICK</b></a></span><b><span style="font-family: Verdana;"> TO SEE SEMI FRAMELESS GATE COMPONENTS</span></b></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1480MM 1170MM- (5 HOLES) SUITS&nbsp;TGAT100&nbsp;USING GATE HINGE</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">1480MM X 1170MM- (5 HOLES) SUITS TGAT160&nbsp;USED WITH GATE HINGE</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">1480MM X 1170MM (6 HOLES) USED WITH GATE LATCH</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">820MM X 1170MM 6MM- SEMI FRAMELESS</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">820MM X 1170MM 8MM</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">900 X 1218MM 12 MM GATE SPECIAL SHAPE</span></span></p>\r\n<p><span style="font-size: large;"><span style="font-family: Verdana;">&nbsp;</span></span></p>\r\n<p><span style="font-size: larger;"><b><span style="font-family: Verdana;">Spigots and other glass clamps</span></b></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">FIXING TO CONCRETE, TIMBER OR STEEL SURFACE MOUNT <a href="http://www.technoglassdesigns.com/category.php?cat_id=64&amp;page=2&amp;is_new=">CLICK</a> HERE</span></span></p>\r\n<p><span style="font-family: Verdana;"><span style="font-size: larger;">FIXING TO CONCRETE CORE DRILL <a href="http://www.technoglassdesigns.com/category.php?cat_id=64&amp;is_new=">CLICK</a> HERE</span></span></p>\r\n<p><span style="font-size: larger;"><span style="font-family: Verdana;">FIXING TO SIDE OF&nbsp;STRUCTURE TIMBER,&nbsp;STEEL OR CONCRETE <a href="http://www.technoglassdesigns.com/category.php?cat_id=65&amp;is_new=">CLICK</a> HERE</span></span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;">Components</span></span></h2>\r\n<p><span style="font-family: Verdana;">To meet the needs of the most  discerning buyer we have designed our components to meet or exceed all  of the requirements for the Australiasian market as well as exceed some  of the most stringent European standards for glass fencing. Our latest  tool to ensure we continue to exceed these requirements is a load test  apparatus that can&nbsp;be seen by&nbsp;clicking on the <a href="http://www.technoglassdesigns.com/contents_uploads/file/Techno_test%20apparatus_050310.jpg"><span style="color: rgb(0, 51, 102);"><u>link</u></span></a><span style="color: rgb(0, 51, 102);">.</span>  This device has been designed in house to allow our products to be  tested in accordance with the Australasian Standards for both swimming  pool fencing and balustrading applications.</span></p>\r\n<p>&nbsp;</p>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;"><b><i>Glass</i></b></span></span></h2>\r\n<p><span style="font-family: Verdana;">Our glass is manufactured to a  level that allows each glass sheet to proudly carry the Australian &amp;  New Zealand Standard: &quot;AS/NZS2208 Grade A ID 51039&quot; for glass sizes  8TF, 10TF &amp; 12TF.</span></p>\r\n<p>&nbsp;</p>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;"><b><i>Frameless Clamps</i></b></span></span></h2>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: Verdana;">The range of TECHNO branded  Stainless Steel clamps are firstly cast from Marine Grade 316L or Duplex  2205&nbsp;stainless steel then machined to close tolerances.&nbsp;The clamps are  then annealed, electro plated followed by satin linishing or mirror  polishing, depending&nbsp;on the finish required.&nbsp;This manufacturing process  makes our clamps ideal for applications where exposure includes salt air  or salt water. We now produce all our clamps from&nbsp;either the&nbsp;316L  version of&nbsp;316 high quality marine grade stainless steel or Duplex 2205.  Due to a higher Molybdenum content present in some of the other grades  of stainless steel,&nbsp;both 316L &amp; Duplex 2205&nbsp;grades provides better  overall corrosion resistance with particularly good resistance to  surface pitting &amp; crevise corrosion in chloride enviroments. The &quot;L&quot;  version of 316&nbsp;typically has a lower Carbon content than the standard  316 grade. Duplex 2205 is higher in tensile strength&nbsp;than 316L thus  allowing&nbsp;us to reduce dimensions while&nbsp;still maintaining structural  integrity.&nbsp;Both grades offer the benefit of greater resistance&nbsp; to brown  staining of the surface (also known as Tea staining) than other grades.  The electro plating process ensures excellent corrosion protection  on&nbsp;edges &amp; threaded areas. These two areas are generally the first  to start&nbsp;brown staining.&nbsp;The Mirror version of our clamps offers  additional resistance to brown staining over the Satin finish due to  the&nbsp; surface being more highly polished. Both grades are excellent in  chlorine or salt water swimming pool environments.</span></p>\r\n<p><span style="font-family: Verdana;">The WOTA clamp range is  manufactured from 2205 Duplex Stainless Steel using the same process as  described for the TECHNO branded clamps. Again ideally suited for salt  or chlorinated swimming pool environments.</span></p>\r\n<p>&nbsp;</p>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;"><i><b>Semi Frameless</b></i></span></span></h2>\r\n<p><span style="font-family: Verdana;">The anodised aluminium used in  the semi frameless system has a thickness of 20-25 microns. This coating  depth achieves a pleasant sheen while still ensuring strong corrosion  resistance in salt environments. By way of example a typical shower  screen frame has a 10 micron finish. We export our product to Europe. In  order to meet the standards required for pool fencing we submitted our  product to the No 1 rated testing authority for France called LNE  (Laboratoire national de m&eacute;trologie et d''essais) located in Paris. In  2007 the frameless system passed all the relevant tests, some of which  are more stringent than required in Australasia such as the gate locking  mechanism.</span></p>\r\n<h2>&nbsp;</h2>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;">Installed Components</span></span></h2>\r\n<p>&nbsp;<span style="font-family: Verdana;">The strength of a each glass  fence panel and therefore ultimately the total fence is influenced by  many factors some of which are beyond our control eg how the components  are installed. We can confirm that we have subjected a frameless panel  with two stainless steel clamps from our three different options&nbsp;- one  two or no holes in the glass&nbsp;range to the Australasian Standard  AS/NZ1107.1 : 2002 Table 3.3 A, B, E, C3 Appendix B &amp; C . This test  is to ensure the panel can sustain a load figure of 0.75Kn. The Techno  Glass Designs panel exceeded that figure by over 50% with no sign of  breakage, fracture or loosening of any component. The deformation was  1mm or less on all tests.</span></p>\r\n<p>&nbsp;</p>\r\n<h2><span style="color: rgb(0, 51, 102);"><span style="font-family: Verdana;">Warranty Statement</span></span></h2>\r\n<p>we will repair or replace any part that we deem to be faulty during the warranty period.</p>\r\n<p>(b) The warranty period by product group is as follows:</p>\r\n<p>&nbsp;Frameless glass clamps - Techno &amp; WOTA brands - LIFETIME STRUCTURAL WARRANTY.</p>\r\n<p>All other stainless steel components - Three (3) years.</p>\r\n<p>All aluminium products - Ten (10) years.</p>\r\n<p>(c) The warranty commences from the date the goods are invoiced by us  to the purchaser regardless of when the goods are used or installed.</p>\r\n<p>(d) Our warranty does&nbsp;NOT cover labour&nbsp; costs incurred in removing or replacing components.</p>\r\n<p>(e) Our warranty does NOT cover transportation costs associated with  any warranty claim. If the goods are deemed to be a warranty claim we  will pay the cost to return the goods to the purchaser.</p>\r\n<p>(f) The warranty does not cover goods used or installed in a manner not approved by the company.</p>\r\n<p>(g) Our warranty does NOT cover glass product.</p>\r\n<p>(h) Non Techno Glass Designs product sold by us such as the Magna Latch&reg; is covered by the product manufacturers own warranty.</p>\r\n<p>(i) Transit damage is not covered by our warranty.</p>\r\n<p>(j) The warranty excludes fair wear &amp; tear to surface finishes.</p>\r\n<p>(k) This warranty is in addition to any legal rights the customer may have in common law.</p>\r\n<p>(l) If a product is considered subject of a warranty claim then the  goods should be returned to our office with proof of purchase. If this  is not possible we will find an alternative to suit both parties such as  photos or a site visit by a member of our team.</p>\r\n<p>THIS WARRANTY IS&nbsp; EFFECTIVE FROM 18TH NOVEMBER 2009.</p>', '', '', 0, 6, 0, 1, '', 0, 0, NULL, 0, 6, 0, 0, NULL, '2012-06-14 15:38:30', '2012-08-16 14:08:17', NULL, NULL),
(23, 'Products & Services', 'products-services', '', '', '', 0, NULL, 0, 1, '/products/productlist', 1, 0, NULL, 1, 3, 0, 1, 'a:7:{s:8:"dc_model";s:8:"Category";s:12:"dc_condition";s:0:"";s:8:"dc_limit";s:1:"5";s:13:"dc_controller";s:10:"categories";s:9:"dc_action";s:4:"view";s:14:"dc_title_field";s:5:"title";s:12:"dc_uri_field";s:9:"permalink";}', '2012-06-14 16:09:06', '2012-08-16 14:09:54', NULL, NULL),
(24, 'News', 'news', '', '', '', 0, NULL, 0, 1, '/news', 1, 0, NULL, 1, 5, 0, 0, NULL, '2012-06-14 16:19:31', '2012-08-15 16:26:07', NULL, NULL),
(25, 'Photo Gallery', 'photo-gallery', '', '', '', 0, NULL, 0, 1, '/galleries/view/2', 1, 0, NULL, 1, 4, 0, 0, NULL, '2012-06-17 11:01:38', '2012-08-16 14:04:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateways`
--

CREATE TABLE IF NOT EXISTS `payment_gateways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_gateway` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option1` text COLLATE utf8_unicode_ci,
  `option2` text COLLATE utf8_unicode_ci,
  `option3` text COLLATE utf8_unicode_ci,
  `default` tinyint(1) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `payment_gateways`
--

INSERT INTO `payment_gateways` (`id`, `payment_gateway`, `username`, `option1`, `option2`, `option3`, `default`, `active`, `created`, `modified`) VALUES
(1, 'offline', '', '', '', '', 0, 1, '2011-09-22 15:59:08', '2012-08-14 16:16:01'),
(2, 'paypal', 'stdev4_1271748947_biz@gmail.com', '', '', '', 0, 1, '2011-09-22 15:59:08', '2012-08-14 16:16:01'),
(3, 'paymate', 'g3rardofarr3ll', '', '', '', 0, 1, '2011-09-22 15:59:08', '2012-08-14 16:16:01'),
(4, 'eway', '', '', '', '', 0, 0, '2011-09-22 15:59:08', '2011-09-22 16:28:02'),
(5, 'giftCard', '', '', '', '', 0, 1, '2011-09-25 09:57:05', '2011-09-26 14:54:42'),
(8, 'offline2', '', '', '', '', 0, 1, '2012-06-26 11:45:36', '2012-08-14 16:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permalink` varchar(255) DEFAULT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `offline` tinyint(1) DEFAULT NULL,
  `gst` tinyint(1) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `related` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `is_new` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `permalink`, `short_description`, `description`, `price`, `image`, `category_id`, `display_order`, `active`, `offline`, `gst`, `rating`, `created`, `updated`, `related`, `quantity`, `is_new`) VALUES
(1, ' Number One Product', 'number-one-product', 'uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque, fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In tristique tincidunt est.Consequat non tortor. ', '<p>uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor.</p>\r\n<p>uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor.</p>\r\n<p>uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor.</p>', 10, '502a0843417e4_Koala.jpg', 1, 1, 0, NULL, 1, 3, '2012-08-14 09:53:59', '2012-08-14 09:53:59', '', 0, 0),
(2, 'At vero', 'at-vero', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum.', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus 								qui  blanditiis praesentium voluptatum deleniti atque corrupti quos dolores 	 							et quas molestias excepturi sint occaecati cupiditate non  provident, 								similique sunt in culpa qui officia deserunt mollitia  animi, id est laborum 								et dolorum fuga. Et harum quidem rerum.</p>', 240, '502a08670cd9c_Penguins.jpg', 1, 1, 1, NULL, 1, 3, '2012-08-14 10:05:59', '2012-08-14 10:05:59', '', 0, 0),
(3, 'Application Server', 'application-server', 'uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque, fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In tristique tincidunt est.Consequat non tortor. ', '<p><span style="color: rgb(0, 51, 102);">uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor. </span></p>\r\n<p><span style="color: rgb(128, 0, 128);">uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor. </span></p>\r\n<p><span style="color: rgb(153, 204, 0);">uat non tortor. Donec a elit diam, ut interdum purus. Nam fringilla  dapibus tellus, at sollicitudin lectus aliquet vel. Maecenas eros neque,  fringilla a cursus et, volutpat ac lacus. Aliquam ac mauris libero. In  tristique tincidunt est.Consequat non tortor.</span></p>', 100, '502a0c7b6d308_Chrysanthemum.jpg', 1, 12, 1, NULL, 0, 3, '2012-08-14 10:27:46', '2012-08-14 10:27:46', '', 0, 0),
(4, 'Access Management', 'access-management', 'The figure entered is the full product price. If GST is not chargeable, please untick the GST box\r\nShort Description', '<p class="hint">The figure entered is the full product price. If GST is not chargeable, please untick the GST box</p>\r\n<p><label for="ProductShortDescription">Short Description</label></p>\r\n<p class="hint">The figure entered is the full product price. If GST is not chargeable, please untick the GST box</p>\r\n<p><label for="ProductShortDescription">Short Description</label></p>\r\n<p class="hint">The figure entered is the full product price. If GST is not chargeable, please untick the GST box</p>\r\n<p><label for="ProductShortDescription">Short Description</label></p>\r\n<p class="hint">The figure entered is the full product price. If GST is not chargeable, please untick the GST box</p>\r\n<p><label for="ProductShortDescription">Short Description</label></p>', 0, '502a0e1e49fe2_Hydrangeas.jpg', 1, 15, 1, NULL, 1, 0, '2012-08-14 10:35:53', '2012-08-14 10:35:53', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `title`, `image`, `display_order`, `product_id`) VALUES
(1, 'Title', '502a0417ac44c_4page_img1.jpg', 12, 1),
(2, 'tazcd', '502a0417c07f7_2page_img1.jpg', 13, 1),
(3, 'At vero', '502a06e86e843_Koala.jpg', 12, 2),
(4, 'asdasd', '502b96f963a96_Hydrangeas.jpg', 23, 2),
(5, 'asdasd', '502b96f9ab66b_tfile_01_image8.jpg', 12, 2),
(6, 'qe', '502b96f9c437e_tfile_01_image11.jpg', 12, 2),
(7, '12', '502b96f9dc743_tfile_01_image11.jpg', 234, 2),
(8, 'qwerew', '502b96fa005ed_tfile_01_image14.jpg', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criteria` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `snippets`
--

CREATE TABLE IF NOT EXISTS `snippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `content` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `snippets`
--

INSERT INTO `snippets` (`id`, `name`, `content`) VALUES
(1, 'contact-us', '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <h3 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h3>\r\n            <p>und the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,  <br>\r\n            <br>\r\n            but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table border="0" cellspacing="0" cellpadding="0" class="contact-info">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <h4 class="helvetica">Corporate</h4>\r\n            <p>012 Some Street<br>\r\n            Lorem Ipsum, VIC, 4135</p>\r\n            <p><strong>Phone:</strong> (123) 456-7890</p>\r\n            <p><strong>FAX:</strong>     (123) 456-7890</p>\r\n            <p><strong>Email:</strong>  <u>sales@domain.com</u></p>\r\n            <u> 											</u></td>\r\n            <td>\r\n            <h4 class="helvetica">Sales Enquire</h4>\r\n            <p><strong>Phone:</strong> (123) 456-7890</p>\r\n            <p><strong>FAX:</strong>     (123) 456-7890</p>\r\n            <p><strong>Email:</strong> <u>sales@domain.com</u></p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>'),
(2, 'home-page', '<p>Home page snippet </p>'),
(3, 'news_header', '<p>this text can be edited from Snippets in admin panel</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tabs`
--

CREATE TABLE IF NOT EXISTS `tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tabs`
--

INSERT INTO `tabs` (`id`, `title`, `content`, `active`, `display_order`, `created`, `modified`) VALUES
(1, 'Tab1 (first)', '<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n<p>this is the tab1 with the dummy lorem ipsum dolor text</p>\r\n\r\n', 1, 1, '2012-08-13 13:13:46', '2012-08-13 13:13:46'),
(2, 'Tab2', 'Another tab with the same lorem ipsum dolor dummy ', 1, 3, '2012-08-13 13:14:26', '2012-08-13 13:14:26'),
(3, 'Tab3', 'hidden tab', 0, 4, '2012-08-13 13:14:53', '2012-08-13 13:14:53'),
(4, 'tab 3', 'LOREM IPSUM DOLOR ', 1, NULL, '2012-08-14 14:18:11', '2012-08-14 14:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `html` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `title`, `image`, `html`, `created`, `modified`) VALUES
(2, 'about-us', '4e940f00c2296_Project_Name_1318249224415.png', '<h1 class="helvetica">About Us</h1>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h4 class="helvetica">Natus error sit 																voluptatem accusantium dolore.</h4>\r\n            <p>Our Company was established in 1990 by Some Person, who prior 																worked for the Jorgenson-Waring Group which was founded by his 																father John in 1965. Our Company is part of the Waring Group in 																Melbourne Australia and is a family business where the John and 																both brothers Chris and Andrew work with Michael. <br>\r\n            <br>\r\n            Travelling extensively in the 																early to mid 90&rsquo;s Michael identified that the future in raw material 																surviving was to be &lsquo;Food Safety&rsquo;. In early 1992 Michael coined 																the phrase on which all Our Company growth would rest &ldquo;Best Source Safe 																Foods&rdquo;.<br>\r\n            <br>\r\n            Our Company has an exceptional 																reputation within the Nut and Dried Fruit industry. In addition 																to the Our Company business, the Waring Group have investments 																in Macadamia farms in Australia, nut processing facilities in 																Vietnam and other associated entitles.</p>\r\n            </td>\r\n            <td width="23">&nbsp;</td>\r\n            <td valign="top" align="right"><img alt="" src="http://test.silvertrees.net/test-mm/dummyimage/235x176/888/fff&amp;text=4:3"></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left" colspan="3">\r\n            <h2 class="helvetica">Mission 																Statement</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source of quality product ensuring maximum 																differentiation from its competitive suppliers. <br>\r\n            <br>\r\n            Our Company will achieve this goal 																through the formation of &ldquo;Partnership&rdquo; relationships with its 																suppliers and customers. The criteria for these relationships 																will revolve around its mission, to always strive under the edict 																of &ldquo;Best Source Safe Food&rdquo;.<br>\r\n            The &ldquo;Supplier Partnership&rdquo; relationship 																will be two way orientated. Information transfer between supplier 																and Our Company will facilitate correct servicing of the customer 																in all respects. Our Company will endeavor to assist and influence 																the investment by its suppliers in best practice systems and 																infrastructure. Such influence may be borne through direct investment 																in such supplier businesses, formation of joint ventures or exclusive 																supply agreements. <br>\r\n            <br>\r\n            The &ldquo;Customer Partnership&rdquo; relationship 																will involve Our Company establishing a close and confidential 																working relationship in which Our Company will directly assist 																in establishing new products and better supply channels for its 																customers. <br>\r\n            <br>\r\n            Our Company will be renowned as 																a high service supplier to its customers&rsquo; needs. Service to customers&rsquo; 																includes correct and accurate detail on market information and 																proven and sound logistics options. Such will culminate in &ldquo;on-time 																delivery&rdquo;, warranty in quality and performance, and</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td valign="top" align="left"><img alt="" src="http://test.silvertrees.net/test-mm/dummyimage/318x240/888/fff&amp;text=4:3"></td>\r\n            <td width="23">&nbsp;</td>\r\n            <td valign="top" align="left">\r\n            <p>Our Company was established in 1990 																by Some Person, who prior worked for the Jorgenson-Waring 																Group which was founded by his father John in 1965. Our Company 																is part of the Waring Group in Melbourne Australia and is a family 																business where the John and both brothers Chris and Andrew work 																with Michael. <br>\r\n            <br>\r\n            Travelling extensively in the 																early to mid 90&rsquo;s Michael identified that the future in raw material 																surviving was to be &lsquo;Food Safety&rsquo;. In early 1992 Michael coined 																the phrase on which all Our Company growth would rest &ldquo;Best Source Safe 																Foods&rdquo;.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h2 class="helvetica">Our Commitment</h2>\r\n            <h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n            <p>Some Person Trading Pty Ltd, trading as Our Company is to be 																recognised in the world market for Edible Nuts, Dried Fruit, 																Desiccated Coconut and ancillary items, as a market leader. Our Company 																Foods will be a reliable source of quality product ensuring maximum 																differentiation from its competitive suppliers. </p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '2011-10-11 12:40:16', '2011-10-11 12:40:16'),
(3, 'Products', '4e941258d8119_Project_Name_1318248832634.png', '								<h1 class="helvetica">Blank Page</h1>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n										<tr>\r\n												<td valign="top" align="left"><h3 class="helvetica">Natus error sit\r\n																voluptatem accusantium dolore.</h3>\r\n														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean\r\n																congue pulvinar augue, vitae semper risus mattis nec. Vivamus\r\n																quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor\r\n																sit amet, consectetur adipiscing elit. Vivamus consequat nunc\r\n																sed neque consectetur tristique. Cras porttitor dictum lorem\r\n																ut aliquam. </p></td>\r\n										</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n										<tr>\r\n												<td valign="top" align="left"><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/184x284/888/fff&text=3:4" alt="" /> </td>\r\n												<td width="40">&nbsp;</td>\r\n												<td valign="top" align="left"><h2 class="helvetica">Product</h2>\r\n														<h4 class="helvetica">Lorem ipsum joh lorem.</h4>\r\n														<p>Our Company was established in 1990 by Some Person, who prior\r\n																worked for the Jorgenson-Waring Group which was founded by his\r\n																father John in 1965. Our Company is part of the Waring Group in\r\n																Melbourne Australia and is a family business where the John and\r\n																both brothers Chris and Andrew work with Michael. <br />\r\n																<br />\r\n																Travelling extensively in the\r\n																early to mid 90â€™s Michael identified that the future in raw material\r\n																surviving was to be â€˜Food Safetyâ€™. In early 1992 Michael coined\r\n																the phrase on which all Our Company growth would rest â€œBest Source Safe\r\n																Foodsâ€.<br />\r\n																<br />\r\n																Our Company has an exceptional\r\n																reputation within the Nut and Dried Fruit industry. In addition\r\n																to the Our Company business, the Waring Group have investments\r\n																in Macadamia farms in Australia, nut processing facilities in\r\n																Vietnam and other associated entitles. </p></td>\r\n										</tr>\r\n										<tr>\r\n												<td colspan="3"><h2 class="helvetica">Small Header</h2>\r\n														<h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n														<p>Some Person Trading Pty Ltd, trading as Our Company is to be\r\n																recognised in the world market for Edible Nuts, Dried Fruit,\r\n																Desiccated Coconut and ancillary items, as a market leader. Our Company\r\n																Foods will be a reliable source titive suppliers. <br />\r\n														</p></td>\r\n										</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="highlight-table">\r\n										<tr>\r\n												<td colspan="3"><h3 class="helvetica">Lorem Ipsum (Head Office)</h3></td>\r\n										</tr>\r\n										<tr>\r\n												<td valign="top"><p>Pelaco Building</p>\r\n														<p>Ground Floor Building 2</p>\r\n														<p>21 â€“ 31 Goodwood Street</p></td>\r\n												<td valign="top"><p>PO Box 7088</p>\r\n														<p>Richmond VIC 2131</p>\r\n														<p><strong>Tel:</strong> +61 3 9420 2900</p>\r\n														<p><strong>Fax:</strong> +61 3 9421 0507</p></td>\r\n												<td valign="top"><p><strong>Email:</strong> info@mwtfoods.com</p>\r\n														<p><strong>Website:</strong> www.mwtfoods.com</p>\r\n														<a href="#" class="bbtn"><span>Contact Our Team</span></a> </td>\r\n										</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n										<tr>\r\n												<td valign="top" align="left"><h4 class="helvetica">Natus error sit\r\n																voluptatem accusantium dolore.</h4>\r\n														<p>hasellus non sapien interdum metus rhoncus blandit sit amet\r\n																vel nunc. Ut nec nisl lacus. Proin consectetur tristique libero.\r\n																Nunrhoncus sodales faucibus. Quisque justo mi, ullamcorper accumsan\r\n																cursus quis, lobortis vitae neque. Phasellus porttitor, diam\r\n																sed posuere molestie, neque ante porttitor ipsum, quis sodales\r\n																lacus neque pharetra ligula. Ut nec odio ac libero bibendum pharetra\r\n																quis ut nulla. Maecenas vitae nisl odio. Nunc ac dui turpis,\r\n																at sodales ipsum. Suspendisse a sagittis purus. Vivamus porttitor\r\n																tincidunt nisi, vel viverra tortor facilisis sit amet. Maecenas\r\n																feugiat posuere felis vel dignissim. Proin id dolor sem, quis\r\n																vestibulum lectus. Suspendisse pellentesque faucibus nisl, non\r\n																pellentesque justo feugiat at. Donec risus erat, gravida sed\r\n																sollicitudin a, sodales nec velit. Cras cursus volutpat egestas. </p></td>\r\n										</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n										<tr>\r\n												<td valign="top" align="left"><h2 class="helvetica">More Products</h2>\r\n														<h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n														<p>Some Person Trading Pty Ltd, trading as Our Company is to be\r\n																recognised in the world market for Edible Nuts, Dried Fruit,\r\n																Desiccated Coconut and ancillary items, as a market leader. Our Company\r\n																Foods will be a reliable source titive suppliers. <br />\r\n														</p></td>\r\n										</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n										<tr>\r\n												<td valign="top">\r\n												<p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&text=4:3" alt="" /></p>\r\n												<h4 class="helvetica">Pepitas.</h4>\r\n												<p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit. \r\n												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur </p>\r\n												</td>\r\n												<td width="85">&nbsp;</td>\r\n												<td valign="top">\r\n												<p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&text=4:3" alt="" /></p>\r\n												<h4 class="helvetica">Pepitas.</h4>\r\n												<p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit. \r\n												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur </p>\r\n												</td>\r\n												<td width="85">&nbsp;</td>\r\n												<td valign="top">\r\n												<p><img class="img-frame" src="http://test.silvertrees.net/test-mm/dummyimage/133x97/888/fff&text=4:3" alt="" /></p>\r\n												<h4 class="helvetica">Pepitas.</h4>\r\n												<p>Aliquam ornare lacus id quam semper consequat. Maecenas sit amet libero velit, non mattis odio. Nam at diam velit. \r\n												Suspendisse potenti. In nec nisl neque, eget cursus erat. Quisque consectetur </p>\r\n												</td>\r\n										</tr>\r\n								</table>\r\n\r\n', '2011-10-11 12:54:32', '2011-10-11 12:54:32'),
(4, 'Blank Page', '4e943e1f69234_Project_Name_1318249237710.png', '								<h1 class="helvetica">Blank Page</h1>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n									<tr>\r\n										<td valign="top" align="left"><h4 class="helvetica">Natus error sit\r\n												voluptatem accusantium dolore.</h4>\r\n											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean\r\n												congue pulvinar augue, vitae semper risus mattis nec. Vivamus\r\n												quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor\r\n												sit amet, consectetur adipiscing elit. Vivamus consequat nunc\r\n												sed neque consectetur tristique. Cras porttitor dictum lorem\r\n												ut aliquam. </p></td>\r\n									</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n									<tr>\r\n										<td valign="top" align="left">\r\n											<h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br /><br />\r\n												Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n											<a href="#" class="gbtn"><span><b class="arrow-right">Download PDF</b></span></a>\r\n										</td>\r\n										<td width="15">&nbsp;</td>\r\n										<td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&text=3:4" alt="" /> </td>\r\n									</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n									<tr>\r\n										<td valign="top" align="left">\r\n											<h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br /><br />\r\n												Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n											<a href="#" class="gbtn"><span><b class="arrow-right">Download PDF</b></span></a>\r\n										</td>\r\n										<td width="15">&nbsp;</td>\r\n										<td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&text=3:4" alt="" /> </td>\r\n									</tr>\r\n								</table>\r\n								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="text-snippets">\r\n									<tr>\r\n										<td valign="top" align="left">\r\n											<h4 class="helvetica">Natus error sit voluptatem accusantium dolore.</h4>\r\n											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue pulvinar augue, vitae semper risus mattis nec. Vivamus quis cursus lacus. Aliquam vel sodales dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consequat nunc sed neque consectetur tristique. Cras porttitor dictum lorem ut aliquam. <br /><br />\r\n												Quisque a scelerisque sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curaeelis.</p>\r\n											<a href="#" class="gbtn"><span><b class="arrow-right">Download PDF</b></span></a>\r\n										</td>\r\n										<td width="15">&nbsp;</td>\r\n										<td valign="top" align="right"><img class="img-border" src="http://test.silvertrees.net/test-mm/dummyimage/313x180/888/fff&text=3:4" alt="" /> </td>\r\n									</tr>\r\n								</table>\r\n', '2011-10-11 16:01:19', '2011-10-11 16:01:19'),
(5, 'Home', '4e95879ee551a_Project_Name_1318248819444.png', '<h1 class="helvetica">Welcome to NSW Dental &amp; Oral</h1>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td valign="top" align="left">\r\n            <h3 class="helvetica">Lorem Association Inc. Lorem Ipsum Lorem.</h3>\r\n            <p>und the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure,  <br>\r\n            <br>\r\n            but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets home-cols">\r\n    <tbody>\r\n        <tr>\r\n            <td width="330" valign="top">\r\n            <h3 class="helvetica">Our Ipsum</h3>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue ut gravida eu, consectetur sed lectus. Lorem ipsum dolor.</p>\r\n            <blockquote>Consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. </blockquote>\r\n            <p>Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue ut gravida eu, consectetur sed lectus.</p>\r\n            </td>\r\n            <td width="35">&nbsp;</td>\r\n            <td width="330" valign="top">\r\n            <h3 class="helvetica">Our Ipsum</h3>\r\n            <table cellspacing="10" cellpadding="0" border="0" class="content-thumbs">\r\n                <tbody>\r\n                    <tr>\r\n                        <td valign="top" align="left"><img src="http://test.silvertrees.net/test-mm/dummyimage/135x100/888/fff&amp;text=3:4" alt=""></td>\r\n                        <td valign="top" align="left"><img src="http://test.silvertrees.net/test-mm/dummyimage/135x100/888/fff&amp;text=3:4" alt=""></td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi.  <br>\r\n            <br>\r\n            Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque.  <br>\r\n            <br>\r\n            Ultricies orci lacinia. Aenean ipsum lacus. Curabitur ultricies tincidunt lorem, et imperdiet mauris suscipit nec.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0" class="text-snippets">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <h3 class="helvetica">Our Ipsum</h3>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue ut gravida eu, consectetur sed lectus. Lorem ipsum dolor.</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td align="center"><img src="http://test.silvertrees.net/test-mm/dummyimage/320x120/888/fff&amp;text=3:1" alt=""></td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue ut gravida eu, consectetur sed lectus. Lorem ipsum dolor.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<table width="100%" cellspacing="0" cellpadding="0" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n            <h3 class="helvetica">Our Ipsum</h3>\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id augue eget mauris sollicitudin malesuada. Nulla facilisi. Nam eleifend facilisis dui at cursus. Nunc lorem sapien, auctor a vestibulum nec, viverra a orci. Vestibulum fringilla lectus et enim ultricies eu ultrices ligula scelerisque. Integer ullamcorper nibh ac diam pulvinar eget ultricies orci lacinia. Aenean ipsum lacus, congue ut gravida eu, consectetur sed lectus. Lorem ipsum dolor.</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '2011-10-11 16:15:08', '2011-10-11 16:15:08');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
