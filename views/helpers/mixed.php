<?

class MixedHelper extends AppHelper {

	var $name = 'Mixed';
	var $helpers = array('Javascript', 'Html', 'Time', 'Xml');

	function validateForm($formID = false) {
		if (!$formID) {
			$formID = 'form';
		} else {
			$formID = "#$formID";
		}
		$this->Javascript->link(array('jquery.validate'), false);
		$script = "$(function(){\$('$formID').validate();});";
		$this->Javascript->codeBlock($script, array('inline' => false));
	}

	function map_token($loc, $lastmod = false, $priority = 0.8, $changefreq = 'daily') {
		$this->helpers = array('Time');
		if (!$lastmod)
			$lastmod = date('y-m-d');
		$lastmod = $this->Time->toAtom($lastmod);
		$out = <<<TOKEN
<url>
	<loc> $loc </loc>
	<lastmod> $lastmod </lastmod>
	<priority> $priority </priority>
	<changefreq> $changefreq </changefreq>
</url>
TOKEN;
		return $out;
	}

	/**
	 * cut the long texts to not exceed than a specific chars count
	 * @param string $string
	 * @param int $limit
	 * @param string $etc
	 * @return <type>
	 */
	function strLimit($string, $limit=255, $etc='...') {
		if (strlen($string) < $limit)
			return $string;
		return substr($string, 0, max(array(strripos(substr($string, 0, $limit), ' '), strripos(substr($string, 0, $limit), "\n")))) . $etc;
	}

}

?>