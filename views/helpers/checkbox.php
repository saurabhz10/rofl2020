<?php
class CheckboxHelper extends HtmlHelper {

    /**
     * Returns a list of checkboxes.
     *
     * @param string $fieldName Name attribute of the SELECT
     * @param array $options Array of the elements (as 'value'=>'Text'
     pairs)
     * @param array $selected Selected checkboxes
     * @param string $inbetween String that separates the checkboxes.
     * @param array $htmlAttributes Array of HTML options
     * @param  boolean $return         Whether this method should
     return a value
     * @return string List of checkboxes
     */
    function checkboxMultiple($fieldName, $options, $selected = null,$inbetween = null, $htmlAttributes = null, $return = false) {
        $this->tags['checkboxmultiple'] = '<input id="%s" type="checkbox" name="%s" %s/><label for="%s">%s</label>';
        $this->tags['hiddenmultiple'] = '<input type="hidden" name="%s" %s/>';
		
		$InputName="data[{$this->model()}][$fieldName][]";
		if(!empty ($htmlAttributes['name'])){
			$InputName="{$htmlAttributes['name']}[]";
		}

        if ($this->tagIsInvalid($this->model(), $this->field())) {
            if (isset($htmlAttributes['class']) &&
                    trim($htmlAttributes['class']) != "") {
                $htmlAttributes['class'] .= ' form_error';
            } else {
                $htmlAttributes['class'] = 'form_error';
            }
        }
        if (!is_array($options)) {
            return null;
        }
        if (!isset($selected)) {
            $selected = $this->value(null, $fieldName);
        }
        else {
            $selected= array_flip($selected);
        }

        foreach($options as $name => $title) {
            $optionsHere = $htmlAttributes;
            if (($selected !== null) && ($selected == $name)) {
                $optionsHere['checked'] = 'checked';
            } else if (is_array($selected) && array_key_exists($name,$selected)) {
                $optionsHere['checked'] = 'checked';
            }
            $optionsHere['value'] = $name;
            $checkbox[] = "<div class='checkbox'>" . sprintf($this->tags['checkboxmultiple'],Inflector::slug($title),$InputName, $this->_parseAttributes($optionsHere),Inflector::slug($title), $title) . "</div>\n";
        }
        return "\n" . sprintf($this->tags['hiddenmultiple'],$InputName, null, $title) .$this->output(implode($checkbox),$return);
    }

	function checkboxCategories($fieldName, $options, $selected = null,$inbetween = null, $htmlAttributes = null, $return = false) {
        $this->tags['checkboxmultiple'] = '<input id="%s" type="checkbox" name="data[%s][%s][]" %s/><label for="%s">%s</label>';
        $this->tags['hiddenmultiple'] = '<input type="hidden" name="data[%s][%s][]" %s/>';


        if ($this->tagIsInvalid($this->model(), $this->field())) {
            if (isset($htmlAttributes['class']) &&
                    trim($htmlAttributes['class']) != "") {
                $htmlAttributes['class'] .= ' form_error';
            } else {
                $htmlAttributes['class'] = 'form_error';
            }
        }
        if (!is_array($options)) {
            return null;
        }
        if (!isset($selected)) {
            $selected = $this->value(null, $fieldName);
        }
        else {
            //$selected= array_flip($selected);
        }
		
        foreach($options as $key=>$option) {
            $optionsHere = $htmlAttributes;
			if (($selected !== null) && ($selected == $key)) {
                $optionsHere['checked'] = 'checked';
            }
            else if (is_array($selected) && array_key_exists($key,
					$selected)) {
                $optionsHere['checked'] = 'checked';
            }
            $optionsHere['value'] = $key;
			$title=$option;
			$modelName=empty ($htmlAttributes['model'])?$this->model():$htmlAttributes['model'];
            $checkbox[] = "<div class='checkbox'>" . sprintf($this->tags['checkboxmultiple'],Inflector::slug($title),$modelName, $fieldName, $this->_parseAttributes($optionsHere),Inflector::slug($title), $title) . "</div>\n";
        }
        return "\n" . sprintf($this->tags['hiddenmultiple'],$modelName,$fieldName, null, $title) .$this->output(implode($checkbox),$return);
    }

}
?>
