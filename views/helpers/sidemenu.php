<?php

class SidemenuHelper extends AppHelper {

    var $helpers = array('Html');

    function outputAdminMenu($selected = null) {
        $adminMenu = array(
            __('Configurations', true) => array(
                'edit' => array('title' => __('Edit Configurations', true), 'url' => array('controller' => 'configurations', 'action' => 'edit')
                )
            ),
            __('Enquiries', true) => array(
                'list' => array('title' => __('List Enquiries', true), 'url' => array('controller' => 'contacts', 'action' => 'index')),
            ),
            __('Newsletters Subscribers', true) => array(
                'list' => array('title' => __('List Newsletters Subscribers', true), 'url' => array('controller' => 'newsletters', 'action' => 'index')),
                'export' => array('title' => __('Export Subscribers', true), 'url' => array('controller' => 'newsletters', 'action' => 'export')),
            ),
            __('Newsletter Templates', true) => array(
                'list' => array('title' => __('List Newsletter Templates', true), 'url' => array('controller' => 'newsletter_templates', 'action' => 'index')),
                'add' => array('title' => __('Add Newsletter Template', true), 'url' => array('controller' => 'newsletter_templates', 'action' => 'add'))
            ),
            __('Newsletter Messages', true) => array(
                'list' => array('title' => __('List Newsletter Messages', true), 'url' => array('controller' => 'news_messages', 'action' => 'index')),
                'add' => array('title' => __('Add Newsletter Message', true), 'url' => array('controller' => 'news_messages', 'action' => 'add'))
            ),
            __('Scheduled Jobs', true) => array(
                'list' => array('title' => __('List Scheduled Jobs', true), 'url' => array('controller' => 'mail_jobs', 'action' => 'index')),
                'add' => array('title' => __('Add Scheduled Job', true), 'url' => array('controller' => 'news_messages', 'action' => 'add_schedule'))
            ),
            __('Pages', true) => array(
                'list' => array('title' => __('List Pages', true), 'url' => array('controller' => 'pages', 'action' => 'index')),
                'add' => array('title' => __('Add Page', true), 'url' => array('controller' => 'pages', 'action' => 'add')),
                'list-snippets' => array('title' => __('List Snippets', true), 'url' => array('controller' => 'snippets', 'action' => 'index'))
            ),
            __('Site Logos', true) => array(
                'list' => array('title' => __('List Logos', true), 'url' => array('controller' => 'logos', 'action' => 'index')),
                'add' => array('title' => __('Add Logo', true), 'url' => array('controller' => 'logos', 'action' => 'add'))
            ),
            __('Home Gallery', true) => array(
                'list' => array('title' => __('List Home Gallery images', true), 'url' => array('controller' => 'ads', 'action' => 'index')),
                'add' => array('title' => __('Add Home Gallery image', true), 'url' => array('controller' => 'ads', 'action' => 'add'))
            ),
            __('News', true) => array(
                'list' => array('title' => __('List News', true), 'url' => array('controller' => 'news', 'action' => 'index')),
                'add' => array('title' => __('Add News', true), 'url' => array('controller' => 'news', 'action' => 'add'))
            ),
            __('Portraits', true) => array(
                'list' => array('title' => __('List Portraits', true), 'url' => array('controller' => 'portraits', 'action' => 'index')),
                'add' => array('title' => __('Add Portrait', true), 'url' => array('controller' => 'portraits', 'action' => 'add'))
            ),
            __('Links', true) => array(
                'list' => array('title' => __('List Links', true), 'url' => array('controller' => 'links', 'action' => 'index')),
                'add' => array('title' => __('Add Link', true), 'url' => array('controller' => 'links', 'action' => 'add'))
            ),
            __('Testimonials', true) => array(
                'list' => array('title' => __('List Testimonials', true), 'url' => array('controller' => 'testimonials', 'action' => 'index')),
                'add' => array('title' => __('Add Testimonial', true), 'url' => array('controller' => 'testimonials', 'action' => 'add'))
            ),
            __('Education', true) => array(
                'list' => array('title' => __('List Courses', true), 'url' => array('controller' => 'courses', 'action' => 'index')),
                'add' => array('title' => __('Add Courses', true), 'url' => array('controller' => 'courses', 'action' => 'add'))
            ),
//            __('Templates', true) => array(
//                'list' => array('title' => __('List Templates', true), 'url' => array('controller' => 'templates', 'action' => 'index')),
//                'add' => array('title' => __('Add Template', true), 'url' => array('controller' => 'templates', 'action' => 'add'))
//            ),
            __('Home Tabs', true) => array(
                'list' => array('title' => __('List Tabs', true), 'url' => array('controller' => 'tabs', 'action' => 'index')),
                'add' => array('title' => __('Add Tab', true), 'url' => array('controller' => 'tabs', 'action' => 'add'))
            ),
            __('Home Gallery', true) => array(
                'list' => array('title' => __('List Home Gallery images', true), 'url' => array('controller' => 'ads', 'action' => 'index')),
                'add' => array('title' => __('Add Home Gallery image', true), 'url' => array('controller' => 'ads', 'action' => 'add'))
            ),
            __('SEO', true) => array(
                'list' => array('title' => __('List SEO rules', true), 'url' => array('controller' => 'seo', 'action' => 'index')),
                'add' => array('title' => __('Add SEO rule', true), 'url' => array('controller' => 'seo', 'action' => 'add'))
            ),
//            __('Snippets', true) => array(
//                'list' => array('title' => __('List Snippets', true), 'url' => array('controller' => 'snippets', 'action' => 'index'))
//            ),
            __('Galleries', true) => array(
                'list' => array('title' => __('List Galleries', true), 'url' => array('controller' => 'galleries', 'action' => 'index')),
                'add' => array('title' => __('Add Galleries', true), 'url' => array('controller' => 'galleries', 'action' => 'add'))
            ),
            __('Gallery images', true) => array(
                'list' => array('title' => __('List Gallery images', true), 'url' => array('controller' => 'gallery_images', 'action' => 'index')),
                'add' => array('title' => __('Add Gallery images', true), 'url' => array('controller' => 'gallery_images', 'action' => 'add'))
            ),
            __('Gallery categories', true) => array(
                'list' => array('title' => __('List Gallery categories', true), 'url' => array('controller' => 'categories', 'action' => 'index')),
                'add' => array('title' => __('Add Gallery Category', true), 'url' => array('controller' => 'categories', 'action' => 'add'))
            ),
            __('Top links', true) => array(
                'list' => array('title' => __('List Top links', true), 'url' => array('controller' => 'resources', 'action' => 'index')),
                'add' => array('title' => __('Add Top link', true), 'url' => array('controller' => 'resources', 'action' => 'add')),
            ),
        );

        return $this->__outputMenu($adminMenu, $selected);
    }

    function outputSubAdminMenu($selected) {
        $subAdminMenu = array(
        );
        return $this->__outputMenu($subAdminMenu);
    }

    function __outputMenu($menu, $selected) {
        $output = '';
        $selected = empty($selected) ? strtolower($this->params['controller']) : $selected;
        foreach ($menu as $title => $content) {
            $class = (strtolower($title) == strtolower($selected)) ? 'current' : 'menu_options';

            $output .= "<li class='options'>";
            $output .= $this->Html->link($title, 'javascript: void(0)', array('class' => '%CLASS%', 'onclick' => "javascript: \$('#" . Inflector::slug($title) . "').slideToggle('fast'); return 0;"));
            $output .= "<ul id='" . Inflector::slug($title) . "' class='%CLASS%'>";

            foreach ($content as $item) {
                if (Router::url($item['url'], true) == Router::url('/' . $this->params['url']['url'], true)) {
                    $class = 'current';
                }
                $output .= "<li>" . $this->Html->link($item['title'], $item['url']) . "</li>";
            }

            $output = str_replace('%CLASS%', $class, $output);
            $output .= '</ul>';
            $output .= '</li>';
        }
        return $output;
    }

}
