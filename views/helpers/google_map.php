<?php
class GoogleMapHelper extends AppHelper {
    var $name = "GoogleMap";
    var $helpers = array('Javascript', 'Html','Form');
    var $count ;
    /**
     * Google Map latitude , longitude  fitcher
     * @param string $model model name
     * @param string $field the field to save data to
     * @param string $label the label to give the input
     * @param string $prefix to sepeate between maps when morethan one are used
     * @param double $latitude the latitude to start with  , Default = -21.391705
     * @param double $longitude the longitude to start with , Default = 133.330078
     * @param string $google_key  default =''
     * @param integer $zoom the zoom to start with , Default = 4
     * @return string
     */
    function embed($model,$field,$label,$latfield,$longfield,$zoomfield,$prefix='',$latitude = false ,$longitude = false ,$zoom = false ,$google_key = '' ) {
        if($latitude === false)
            $latitude = empty($this->data[$model][$latfield])? 0:$this->data[$model][$latfield];
        if($longitude === false)
            $longitude = empty($this->data[$model][$longfield])? 0:$this->data[$model][$longfield];
        if($zoom === false)
            $zoom = empty($this->data[$model][$zoomfield])? 0:$this->data[$model][$zoomfield];

        $this->count++;
        if($prefix == '') {
            if($this->count == 1)
                $prefix = '_1st';
            else if($this->count == 2)
                $prefix = '_2nd';
            else if($this->count == 3)
                $prefix = '_3rd';
            else
                $prefix = '_'.$this->count.'th';
        }
        else
            $prefix = '_'.$prefix;

        $config = parse_ini_file(APP.DS.'app_config.ini');


        $this->Javascript->link('http://maps.google.com/maps/api/js?sensor=false', false); //key='.$google_key,false);
        $this->Javascript->codeBlock('
			var '.$prefix.'_map = false;
			function '.$prefix.'_initialize() {
				'.$prefix.'_map = new google.maps.Map(document.getElementById("'.$prefix.'_map_canvas"), {mapTypeId: google.maps.MapTypeId.ROADMAP, center: new google.maps.LatLng('.$latitude.', '.$longitude.'),zoom: '.$zoom.'});
				google.maps.event.addListener('.$prefix.'_map, "click", function(event) {
					var latlng = event.latLng;
					document.getElementById("'.$field.'").value = latlng.lat()+","+latlng.lng()+","+'.$prefix.'_map.getZoom();
				});
			}


			$(function(){
			$(".'.$prefix.'_map").hide();
				$("#'.$prefix.'_browseMap").click(function(){
					$(".'.$prefix.'_map").toggle();
					if (!'.$prefix.'_map){
						'.$prefix.'_initialize();
					}
				});
				$("#'.$prefix.'_close").click(function(){
						$(".'.$prefix.'_map").hide();
				});
			});
			',array('inline'=>false));
        $output = '
				<div class="Map_box" id="Map_Block">
					<label for="'.$prefix.'_Glatlng">'.$label.'</label>
					<p class="hint">Single click on the location you want it to be the center of the map in the front end</p>
					<input type="text" name="data['.$model.']['.$field.']" id="'.$field.'" value="'.$latitude.','.$longitude.','.$zoom.'" />
					<input type="button" id="'.$prefix.'_browseMap" value="..." />

<div class="'.$prefix.'_map Map_box"  style="position:absolute" >
<input type="button" id="'.$prefix.'_close" value="Close"  />
					<div id="'.$prefix.'_map_canvas" style="width: 650px; height: 300px;">
					</div>
				   </div>
				</div>';
        return $output;
    }
    //-------------------------------------
    /*
     * $googleMap->create_map(30.216355,30.928574,10,array('height'=>240,'width'=>600,'direction'=>true));
     *
     */
    function create_map($latitude = 0 ,$longitude = 0 ,$zoom = 10,$options = array()) {
        $default = array('width' => 646, 'height' => 240,'direction'=>false,'country'=>'Australia');
        $options = array_merge($default, $options);
        
        echo $this->Javascript->link('http://maps.google.com/maps/api/js?sensor=false');
        echo $this->Javascript->codeBlock("
                var map;
                var gdir;
                var geocoder = null;
                var center=null;
                var addressMarker;
                var directionDisplay;
                var directionsService  = new google.maps.DirectionsService();
                var latlng;
                function initialize() {
                    latlng = new google.maps.LatLng($latitude,$longitude);
                    directionsDisplay = new google.maps.DirectionsRenderer();
                    var settings = {
                        zoom: $zoom,
                        center: latlng,
                        mapTypeControl: true,
                        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                        navigationControl: true,
                        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById('map_canvas'), settings);
                    var companyPos = new google.maps.LatLng($latitude,$longitude);
                    var companyMarker = new google.maps.Marker({
                        position: companyPos,
                        map: map
                    });
                    directionsDisplay.setMap(map);
                    directionsDisplay.setPanel(document.getElementById('map_directions'));

                }
                $(document).ready(function(){
                    initialize();
                    $('#DirectionsForm').submit(function(){
                        calcRoute($('#txtAddress').val());
                        return false;
                    });
                });
                
                function calcRoute(address) {
                    var country = '{$options["country"]}';
                    var start = address + ','+country;
                    var end = latlng;
                    var request = {
                        origin:start,
                        destination:end,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };
                    directionsService.route(request, function(response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(response);
                        }else{
                            alert('No Result Found.');
                        }
                    });
                }

            ");

        $output = "<div class='Map' id='map_canvas' style='width: {$options['width']}px; height: {$options['height']}px;'></div>";
        if($options['direction']){
            $output.="<form action='' id='DirectionsForm'>
                    <div class='directions_input'>
                        <label>Starting from:</label>
                        <input type='text' name='address' id='txtAddress'/>
                        <input type='submit' class='getdirections blackbutton' value='Get Directions' />
                    </div>
                </form>
                <div class='direction' id='map_directions' style='vertical-align: top;'></div>";
        }
        return $output;
    }
    /*
     *<img src='$googleMap->create_static_map(30.216355,30.928574,12,array('width'=>500,'height'=>240));' />
     * 
     */
    function create_static_map($latitude = 0 ,$longitude = 0 ,$zoom = 11,$options = array()) {
        $default = array('width' => 646, 'height' => 240);
        $options = array_merge($default, $options);
        $this->Javascript->link('http://maps.google.com/maps/api/js?sensor=false', false);
        $output="
            http://maps.google.com/maps/api/staticmap?center=$latitude,$longitude&zoom=$zoom&size={$options['width']}x{$options['height']}&maptype=roadmap
            &markers=color:blue|label:S|$latitude,$longitude&sensor=false
        ";

        return $output;
    }

}

?>
