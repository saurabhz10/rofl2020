<div class="mailJobs index">
    <h1><?php __('Scheduled Jobs'); ?></h1>
    <?php
    echo $list->filter_form($modelName, $filters);
    $fields = array(
        'MailJob.id' => array(),
        'MailJob.created' => array('title' => 'Created Date', 'date_format' => 'd-m-Y H:i'),
        'MailJob.date' => array('title' => 'Send Date', 'date_format' => 'd-m-Y H:i'),
        'Sent' => array('php_expression' => '".($row[$model]["is_sent"])?("Sent  - Success: {$row[$model]["sent_emails"]}  - Failed: {$row[$model]["not_sent_emails"]}"):"Pending"."', 'title' => 'Status'),
    );
    $links = array(
        //$html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), //, __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));
    echo $list->adminIndexList($fields, $mailJobs, $links, true, $multi_select_actions);
    ?>
</div>