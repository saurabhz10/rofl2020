<?php echo $form->create('MailJob',array('action'=>$this->action)); ?><div class="rounded-item config-msg">
    <b class="bl-corners"></b><b class="br-corners"></b>
    <div class="FormExtended confirm-message">
	<h3><?=__('Confirmation Message !',true)?></h3>
		<?php echo __('Are you sure you want to delete?'); ?>
	<div class='delete-items'>
	    <?php foreach($mailJobs as $i=>$mailJob):?>
		<input type="hidden" value='<?=$mailJob['MailJob']['id']?>' name='ids[]' />
		<span class='one-item'><?="Job#{$mailJob['MailJob']['id']}"?></span>
		<?php if(!empty ($mailJob[$i+1])) {?>
		    <span class="seprated-item">,</span>
		<?php } ?>
	    <?php endforeach;?>
	</div>	<div class="clear"></div>
    </div>
</div>

<div class="confirm-action">
    <button class="green-button" type="submit" name="submit_btn" value="yes" ><?=__('Yes',true)?></button>
    <button class="red-button" type="submit" name="submit_btn" value="no" ><?=__('No',true)?></button>
</div>

<?php echo $form->end(); ?>