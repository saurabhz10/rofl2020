<div class="FormExtended">
	<?php 
	echo $form->create('Newsletter');

	echo $form->input('id', array('class' => 'INPUT required'));
	echo $form->input('email', array('class' => 'INPUT email required'));

	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();

	?>
</div>
