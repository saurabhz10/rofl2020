<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administrator Login</title>
        <meta name="keywords" content="keywords, keywords, keywords," />
        <meta name="description" content="Description." />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $html->css(array('admin_login/login')); ?>
        <?php echo $javascript->link(array('admin_login/jquery', 'admin_login/infieldlabel.min')); ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".field-labels label").inFieldLabels();
            });
        </script>
    </head>
    <body style="background:#FFF;">
        <div class="st-login">
            <div class="st-logo"><a href="http://www.silvertrees.net/" target="_blank"><img src="<?= Router::url('/css/admin_login/login/silvertrees-logo.jpg') ?>" alt="" title="" /></a></div>
            <h2 class="sec-title">Administrator Login:</h2>
            <div class="st-globle">
                <div class="wa-ico">website admin</div>
            </div>
            <div class="st-box">
                <?php $session->flash('AdminLogin'); ?>
                <div class="box-snippets">Please enter your administrator username below to login to your account and manage your site. </div>
                <div class="st-form">		
                    <?php
                    $session->flash('AdminLogin');
                    echo $form->create('Admin', array('action' => 'admin'));
                    echo $form->input('name', array('div' => array('autocomplete'=>'off','class' => 'input text field-labels'), 'class' => 'INPUT required', 'label' => 'Username'));
                    echo $form->input('password', array('div' => array('autocomplete'=>'off','class' => 'input text field-labels'), 'class' => 'INPUT required', 'label' => 'Password'));
                    ?>

                    <div class="submit">
                        <button type="submit"><span class="submit-btn"><span>Login</span></span></button>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
