<div class="FormExtended">
	<?php 
	echo $form->create('Resource');

	echo $form->input('id', array('class' => 'INPUT required'));
	echo $form->input('title', array('class' => 'INPUT required'));
	echo $form->input('url', array('class' => 'INPUT required',"type"=>"text"));
	echo $form->input('active', array('class' => 'INPUT'));
	echo $form->input('display_order', array('class' => 'INPUT number'));

	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();

	?>
</div>
