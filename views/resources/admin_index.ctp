<div class="resources index">
<h1><?php __('Resources');?></h1>
<?php
echo $list->filter_form($modelName, $filters);
$fields=array(
	'Resource.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Resource.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Resource.display_order' => array()
);
$links = array(
	$html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')),//, __('Are you sure?', true)),
);

$multi_select_actions=array('delete'=>array('action'=>Router::url(array('action'=>'delete')),'confirm'=>true));

echo $list->adminIndexList($fields, $resources, $links,true,$multi_select_actions);
?>
</div>