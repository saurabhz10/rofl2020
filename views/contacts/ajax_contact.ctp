
<h4 class="condensed">Contact Form</h4>
<?php
$security_code = generateCode(6);
$_SESSION['security_code'] = $security_code;
$session->flash();
echo $form->create('Contact', array('action' => 'ajax_contact', 'id' => 'contact'));
?>
<div class="input text in-field">
    <label for="name">Your Name</label>
    <span class="footer-input inputrequired">   
        <?php echo $form->input('name', array('class' => 'required', 'id' => 'name', 'div' => false, 'label' => false)); ?></span>
</div>
<div class="input text  in-field">
    <label for="n-email">Your Email</label>
    <span class="footer-input inputrequired"> <?php echo $form->input('email', array('class' => 'required email', 'id' => 'n-email', 'div' => false, 'label' => false)); ?></span>
</div>
<div class="input text in-field">
    <label for="message">Message</label>
    <span class="footer-textarea inputrequired"><?php echo $form->input('message', array('class' => 'required', 'rows' => '10', 'cols' => '10',  'id' => 'message', 'div' => false, 'label' => false)); ?> </textarea></span>
</div>

<?php echo $form->input('security_code', array('type'=>'hidden','value'=>"", 'label' => '', 'div' => false, 'value' => false)); ?>

<!--<div class="input text in-field">
    <label for="ContactSecurityCode">Security Code</label>
    <img id="SecurImage" src="<?//= Router::url('/image.jpg/'); ?>"/>
    <span class="footer-input inputrequired">            
        <?php //echo $form->input('security_code', array('class' => 'required', 'label' => '', 'div' => false, 'value' => false)); ?>
    </span>
</div>-->

<div class="input submit">
    <input type="image" src="<?= Router::url('../css/img/btns/send-btn.gif') ?>" />
    <span class="inline-loading" style="display:none;"><img src="<?=Router::url('/css/img/form-loader.gif')?>"/></span>
        
</div>
<?php echo $form->end(); ?>
<script type="text/javascript">
    var security_code = '<?=$security_code?>';
    $("#contact input").removeClass('form-error');
    $(".in-field label").inFieldLabels();
    $('.error-message').each(function(){
        $(this).insertAfter($(this).parent());
    });
</script>