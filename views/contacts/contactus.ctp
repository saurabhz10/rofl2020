<h1>Contact</h1>
<div class="snippets-box">
    <?php echo $contact_snippet; ?>
</div>
<div class="contact-form">
    <h2>Enquiry form:</h2>
    <?php $session->flash(); ?>

    <?php
    $url = array('action' => 'contactus');
    echo $form->create('Contact', array('url' => $url));
    echo $form->input('subject', array("type" => "hidden", "value" => "contact us enquiry "));
    echo $form->input('contact_type', array("type" => "hidden", "value" => "contact"));
    ?>

    <div class="input text left">
        <label for="ContactFirstName">First Name<span class="star">*</span></label>
        <?php echo $form->input('first_name', array('class' => 'required', 'div' => false, 'label' => false)); ?>
    </div>
    <div class="input text left">
        <label for="ContactLastName">Last Name <span class="star">*</span></label>
        <?php echo $form->input('last_name', array('class' => 'required ', 'div' => false, 'label' => false)); ?>
    </div>
    <div class="input text left">
        <label for="ContactLEmail">Email<span class="star">*</span></label>
        <?php echo $form->input('email', array('class' => 'required ', 'div' => false, 'label' => false)); ?>
    </div>

    <div class="clear"></div>
    <div class="input textarea">
        <label for="ContactMessage">Message</label>
        <?php echo $form->input('message', array('class' => 'required ', 'div' => false, 'label' => false)); ?>
    </div>
    <div class="clear"></div>
    <div class="input text captcha">
        <?php
        echo $form->label('ContactCaptchaCode', 'Security Code');
        ?><img alt="" id="SecurImage" src="<?= Router::url('/image.jpg/'); ?>"/><?
        echo $form->input('security_code', array('class' => 'required', 'label' => '', 'div' => false));
        if (!empty($error_captcha)):
            ?>
            <div class='error-message'><?= $error_captcha ?></div>
        <?php endif; ?>
    </div>
    <div class="input submit submit-center">
        <button type="submit"><span class="btn-a corner-all">SUBMIT</span></button>
    </div>
    <?php
    echo $form->end();
    echo $this->element('FormValidation', array('FormId' => "ContactContactusForm"));
    ?>
</div>
