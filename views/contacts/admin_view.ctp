<div class="Order_PaymentDetails">

    <h3>Contact details</h3>     
    <table border="0" cellpadding="0" cellspacing="0" class="listing-table">
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Subject</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['subject'] ?></font></td>
        </tr>
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Type</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['contact_type'] ?></font></td>
        </tr>
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>First Name</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['first_name'] ?></font></td>
        </tr>
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Last Name</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['last_name'] ?></font></td>
        </tr>

        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Email</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['email'] ?></font></td>
        </tr>
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Message</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= nl2br($contact['Contact']['message']) ?></font></td>
        </tr>
        <tr>
            <td width="15%"><font face="Arial" style="font-size:12px;"><strong>Contact Date</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= $contact['Contact']['created'] ?></font></td>
        </tr>
    </table>
</div>