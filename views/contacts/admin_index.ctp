<div class="faqs index">
    <h2><?php __('Contact Us Enquiries'); ?></h2>

    <?php
    echo $list->filter_form($modelName, $filters);
    $fields = array(
        'Contact.id' => array('edit_link' => array('action' => 'view', '%id%')),
        'Contact.first_name' => array('title' => 'First Name'),
        'Contact.last_name' => array('title' => 'Last Name'),
        'Contact.email' => array('title' => 'Client Email'),
        'Contact.subject' => array('title' => 'Subject'),
        'Contact.contact_type' => array('title' => 'Contact Type'),
        'Contact.created' => array('title' => 'Date')
    );



    $links = array(
        $html->link('View', array('action' => 'view', '%id%'), array('class' => 'View')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
    );
    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete_multi')), 'confirm' => true)
    );

    echo $list->adminIndexList($fields, $contacts, $links, true, $multi_select_actions)
    ?>
</div>
