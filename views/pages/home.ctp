<?php /* <div class="portfolio" id="portfolio">
  <?php foreach ($ads as $key => $ad) {
  ?>
  <div class="box">
  <div class="box-wrap">
  <a href="#"><img src="<?php echo $ad["Ad"]["image_full_path"]; ?>" alt="" />
  </a>
  <div class="sliding-box">
  <p><?php echo $ad["Ad"]["description"]; ?>
  <a href="<?php echo $ad["Ad"]["url"]; ?>" class="color-a" title="<?php echo $ad["Ad"]["title"]; ?>"> [ See More ] </a>
  </p>
  </div>
  </div>
  </div>
  <?php }
  ?>
  </div> */ ?>
<?php if (!empty($ads)) echo $this->element('ads'); ?>
<div class="clear"></div>

<div class="tabs" id="tabs">
    <ul class="tabs-links">

        <?php foreach ($tabs as $tab) { ?>
            <li><a href="#<?= strtolower(Inflector::slug($tab['Tab']['title'], '-')); ?>"><?= $tab['Tab']['title']; ?></a></li>
        <?php } ?>
    </ul>
    <?php foreach ($tabs as $tabid => $tab) { ?>
        <div class="tab-content" id="<?= strtolower(Inflector::slug($tab['Tab']['title'], '-')); ?>">
            <p><?= $tab['Tab']['content']; ?></p>
        </div>
    <?php } ?>

</div>
<?php if (!empty($recentposts)) { ?>

    <div class="news-listing recent-posts">
        <h3>Recent Posts</h3>
        <?php
        foreach ($recentposts as $key => $newss) {
            ?>

            <div class="items-block">
                
                <?php
            if (!empty($newss["News"]["image"])) {
                $image_path = $newss["News"]["image"];
            } else {
                $image_path = "default.png";
            }
            
            ?>
            <a title="<?php echo $newss["News"]["title"]; ?>" href="<?php echo Router::url(array("controller" => "news",'action' => 'view', $newss['News']['permalink'])) ?>" class="left">
                <img src="<?php echo get_resized_image_url($image_path, 112 , 85 , true ); ?>" alt="<?php echo $newss["News"]["title"]; ?>" />
            </a>
                <div class = "block-details">
                    <h2><a href = "<?php echo Router::url(array("controller" => "news", 'action' => 'view', $newss['News']['permalink'])) ?>"><?php echo $newss["News"]["title"];
                ?></a></h2>
                    <p>
                        <?php echo $newss["News"]["short_description"]; ?>
                    </p>
                    <a href="<?php echo Router::url(array("controller" => "news", 'action' => 'view', $newss['News']['permalink'])) ?>" class="btn-a corner-all">Read More</a>
                </div>
                <div class="clear"></div>
            </div>
            <?php
        }
        ?>         
    </div>

<?php } ?>