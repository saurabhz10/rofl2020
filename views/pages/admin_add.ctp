<h1>
<?php  if($this->action == 'admin_add'){
    echo "Add Page";
}else{
    echo "Edit '" . $this->data['Page']['title']."'";
}  ?>
    </h1>
<?php echo $javascript->link('menu'); ?>
<div class="FormExtended">
    <?php echo $form->create('Page'); ?>
    <?php
    if (isset($this->data['Page']['category_id'])) {
        echo $form->input('category_id', array('class' => 'INPUT', 'type' => 'hidden'));
    }
    echo $form->input('id', array('class' => 'INPUT'));
    echo $form->input('title', array('class' => 'INPUT'));
    if (empty($this->data['Page']['default'])) {
        echo $form->input('permalink', array('class' => 'INPUT'));
    }
    ?>
    <div class="PageSelector">
        <h5>Page will act as </h5>
        <div>
        <div class="tab-bar">
        	<ul>
                <li><a href="#" id="OpenPage" >Content Page</a> </li>
                <li><a href="#" id="OpenLink" >Just a Link</a></li>
            </ul>
        </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="LinkContainer" class="tab-content">
        <?php echo $form->input('url', array('class' => 'INPUT', 'label' => 'URL')); ?>
        <?php echo $form->hidden('is_url', array('class' => 'INPUT', 'id' => 'is_url', 'label' => __('Is URL', true))); ?>
    </div>
    <div id="PageContainer" class="tab-content">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td valign="top" width="300">
                    <?php
                    echo $form->label('Use saved template');
                    $template_images = array();
                    echo $this->element('uboard_left', array('templates' => $templates, 'prefix' => 'templates', 'template_id' => 'UseTemplate', 'current_template' => $this->data['Page']['template_id']));
                    echo "<span class='note'>Note: if you select a template,it will be delete all the content you have added for this page </span>";
                    echo "<div style='display:none;'>";
                    echo $form->input('template_id', array(
                        'class' => 'INPUT',
                        'id' => 'UseTemplate',
                        'after' => $html->link('Preview selected template', '#', array('id' => 'TemplatePreviewLink', 'class' => 'Preview'))));
                    echo "</div>";
                    ?>
                </td>
            </tr>
        </table>
        <?php
        echo $fck->create('Page', 'content', null, 'Default', Router::url('/css/screen.css'), array('height' => 550, 'style' => "body {background:#FFF }"));
        ?>

        <?php
        echo $form->input('keywords', array('class' => 'INPUT', 'label' => __("Meta Keywords", true)));
        echo $form->input('description', array('class' => 'INPUT', 'label' => __("Meta description", true)));
        ?>	
    </div>
     
    <?php
    //if (empty($this->data['Page']['default'])) {
    //echo $form->input('access', array('class' => 'INPUT'));
    //}

    echo $form->input('add_to_main_menu', array('id' => 'add-to-main', 'class' => 'INPUT', 'label' => 'Add To Menu?'));
    echo "<div id='MenuList' class='menu-options' style='display:none;'>";
    echo $form->input('menu_id', array('id' => 'SelectMenu', 'class' => 'INPUT', 'empty' => 'No Parent'));
    echo $form->input('submenu_id', array('options' => array(), 'class' => 'INPUT', 'empty' => 'No Submenu'));
    echo "</div>";
    echo $form->input('display_order', array('class' => 'INPUT'));

    if (empty($this->data['Page']['default'])) {
        echo $form->input('active', array('class' => 'INPUT'));
    }
    

     if($this->params['named']['dce'] == 'yes'){ //to enable/disable dynamic children
    echo $form->input('has_dynamic_children', array('id' => 'has_dc', 'class' => 'INPUT', 'label' => 'Dynamic Children: Does this menu have dynamic children? ( e.g. parent of a list from the database, such as categories, products etc.. )', 'style' => 'display:'));
    
    echo '<div id="children-options" style="display: ; ">'; 
    
    echo $form->input('dc_model', array('id' => 'dc_model', 'class' => 'INPUT', 'label' => 'Children Model Name e.g. Category'));
    echo $form->input('dc_condition', array('id' => 'dc_condition', 'class' => 'INPUT', 'label' => 'Condition e.g. active = \'1\''));
    echo $form->input('dc_limit', array('id' => 'dc_limit', 'class' => 'INPUT', 'label' => 'Limit (max items)'));
    echo $form->input('dc_order', array('id' => 'dc_order', 'class' => 'INPUT', 'label' => 'order eg. display_order asc, published desc'));
    echo $form->input('dc_controller', array('id' => 'dc_controller', 'class' => 'INPUT', 'label' => 'Controller e.g. categories'));
    echo $form->input('dc_action', array('id' => 'dc_action', 'class' => 'INPUT', 'label' => 'action e.g. view'));
    echo $form->input('dc_title_field', array('id' => 'dc_title_field', 'class' => 'INPUT', 'label' => 'Title field (the field that will be used as title for menu items'));
    echo $form->input('dc_uri_field', array('id' => 'dc_uri_field', 'class' => 'INPUT', 'label' => 'Url field (the field that will be used as url for menu items e.g. permalink or id'));
    
    echo "</div>" ; 
 }
    
    ?>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (!empty($this->data['Page']['id'])) {
        echo $this->element('save_as_new', array('model' => 'Page'));
    }
    ?>
    <?php echo $form->end(); ?>
</div>
<?php
if (empty($this->data['Page']['default'])) {
    echo $this->element('permalink');
}

echo $javascript->link('chosen.jquery.min');
echo $html->css('chosen');
?>
<script type="text/javascript"> $(".chzn-select").chosen(); </script>
<script type="text/javascript">
    var  email_templates=<?= $javascript->object($templates); ?>;
    
    
    
    function loadTemplate(id)
    {
        var oEditor = FCKeditorAPI.GetInstance('data[Page][content]') ;
        var OldText = oEditor.GetHTML();
        var NewText = (id)?OldText:"";
        if(OldText){
            if(confirm('Are you sure you want to update page content?')){
                NewText = (id)?email_templates[id].html : "" ;
                console.log(id);
                oEditor.SetHTML(NewText);
            }
        }else{
            NewText = email_templates[id].html ; 
            oEditor.SetHTML(NewText);
        }
        
    }
    
</script>



<script type="text/javascript">
    function check_menu_list(){
        if($('#add-to-main').is(':checked')){
            $('#MenuList').show();
        }else{
            $('#MenuList').hide();			
        }
    }
    $(function(){
		
        check_menu_list();
        $('#add-to-main').click(function(){
            check_menu_list();
        });
		
        $('#LinkConatiner,#PageContainer').hide();
		
        $('#OpenLink').bind('click', function(){
            $('#LinkContainer').fadeIn();
            $('#is_url').val(1);
            $('#PageContainer').hide();
			$(this).addClass('active');
			 $('#OpenPage').removeClass('active');
            return false;
        });

        $('#OpenPage').bind('click', function(){
            $('#PageContainer').fadeIn();
            $('#LinkContainer').hide();
			$(this).addClass('active');
			 $('#OpenLink').removeClass('active');
            return false;
        });

<?php if (!empty($this->data['Page']['url'])): ?>
            $('#OpenLink').click();
<?php else: ?>
            $('#OpenPage').click();
<?php endif; ?>
			
    });
</script>

<script type="text/javascript">//<![CDATA[
    $(function(){
        //		$('a', '.breadcrumb').click(function(){
        //			var href = $(this).attr('href');
        //
        //			$('.attachment-tab').hide();
        //			$(href).show();
        //			console.log(href);
        //			$('.active', '.breadcrumb').removeClass('active');
        //			$(this).addClass('active');
        //
        //			window.location.hash = 'tab:' + href.substr(1);
        //			return false;
        //		});
        //		
        //		
        //		var activeSelector = ':first';
        //		var rx = new RegExp('#tab:');
        //		if (window.location.hash != '' && rx.test(window.location.hash)){
        //			var hash = window.location.hash;
        //			activeSelector = hash.replace('tab:', '');
        //		}
        //		
        //		$('.attachment-tab').not(activeSelector).hide();
    });
    //]]></script>

<script type="text/javascript">
    $('document').ready(function(){
        $("#PageSubmenuId").attr("disabled", true);
        change_submenu($('#SelectMenu').val());
        if (!$('#PageSubmenuId').val())
        {
            document.getElementById('PageSubmenuId').disabled = true;
            $('#PageSubmenuId').html('<option></option>');
        }
        $('#SelectMenu').change(function(){
            change_submenu($(this).val());
        });
    });
    // Function to handle ajax.
    function change_submenu(menu_id)
    {
        if(!menu_id){
            return false;
        }
        $.ajax({
            async: true,
            type: "GET",
            url: "<?php echo Router::url('/admin/pages/get_submenus/'); ?>"+menu_id+"/"+<?php echo empty($this->data['Page']['id']) ? 0 : $this->data['Page']['id']; ?>,
            dataType: "json",
            success: function(data){
                $('#PageSubmenuId').html('<option value=0>No Submenu</option>');

                for(var i=0; i < data.submenus.length; i++)
                {
                    var submenu = data.submenus[i];
                    var is_Selected="";
                    var selected_submenu="<?php echo $this->data['Page']['submenu_id']; ?>";
                    //alert(submenu.Submenu.id);
                    if(submenu.Page.id==selected_submenu)
                    {
                        is_Selected="selected=selected";
                    }
                    $('#PageSubmenuId').append('<option value="'+submenu.Page.id+'"'+is_Selected+'>' +submenu.Page.title+'</option>');

                }
                document.getElementById('PageSubmenuId').disabled = false;
                return false;
            }
			

        });

    }
</script>