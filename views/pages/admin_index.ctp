<div class="staticPages index">
    <h2><?php __('Pages'); ?></h2>

    <?php
    echo $list->filter_form($modelName, $filters);
    $fields = array(
        'Page.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Page.title' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Page.permalink' => array('edit_link' => array('action' => 'edit', '%id%')),
		'AddMenu' => array('title'=>'In Menu ?','php_expression' => '".(empty($row["Page"]["add_to_main_menu"])?("<div class=\"no\">No</div>"):("<div class=\"yes\">Yes</div>"))."'),
        'Menu' => array('php_expression' => '".($row["Page"]["menu_id"])?$row["Page"]["title"]:"No Parent"."'),
        //'Permission' => array('title'=>'Permission','php_expression'=>'".$params[$row[$model]["access"]]."'),
		'Page.display_order' => array(),
    );
    $links = array(
        $html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
    );

    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete', 'admin' => true)), 'confirm' => true)
    );
    //debug($pages);
    ?><?= $list->adminIndexList($fields, $pages, $links, true, $multi_select_actions,$accesses) ?></div>
