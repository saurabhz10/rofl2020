<?php
echo $javascript->link('jquery-ui.min');
?>


<ul id="test-list">
    <li id="listItem_1"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle" /><strong>Item 1 </strong>with a link to <a href="http://www.google.co.uk/" rel="nofollow">Google</a>


        <ul id="test-list-2">
            <li id="listItem_2_2"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle-2" /><strong>SUB Item 2</strong></li>
            <li id="listItem_3_2"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle-2" /><strong>SUB Item 3</strong></li>
            <li id="listItem_4_2"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle-2" /><strong>SUB Item 4</strong></li>

        </ul>

    </li>
    <li id="listItem_2"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle" /><strong>Item 2</strong></li>
    <li id="listItem_3"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle" /><strong>Item 3</strong></li>
    <li id="listItem_4"><img src="<?= Router::url('/css/img/arrow.png') ?>" alt="move" width="16" height="16" class="handle" /><strong>Item 4</strong></li>

</ul>


<script type="text/javascript">
    $(document).ready(function() {
        $("#test-list").sortable({
            handle : '.handle',
            update : function () {
                alert('update');
                var order = $('#test-list').sortable('serialize');
                alert(order);
            }
        });
        
        $("#test-list-2").sortable({
            handle : '.handle-2',
            update : function () {
                alert('update-2');
                var order = $('#test-list-2').sortable('serialize');
                alert(order);
            }
        });
    });
</script>