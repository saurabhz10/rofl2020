
<div class="FormExtended">
    <?php echo $form->create('Category', array('type' => 'file')); ?>
    <?php
    echo $form->input('id', array('class' => 'INPUT'));
    echo $form->input('title', array('class' => 'INPUT', 'id' => 'PageTitle'));
    echo $form->input('short_description', array('class' => 'INPUT'));
    echo $form->input('description', array('class' => 'INPUT'));
    echo $form->input('image', array('class' => 'INPUT', 'type' => 'file', 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['Category']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['Category']['image'] : '')))
    ));
    echo $form->input('display_order', array('class' => 'INPUT'));
    echo $form->input('active', array('class' => 'INPUT'));
    ?>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (is_array($this->data) && $this->data['Category']['id']) {
        echo $this->element('save_as_new', array('model' => 'Category'));
    }
    ?><?php echo $form->end(); ?>
</div>
<?php e($this->element('permalink')) ?>
