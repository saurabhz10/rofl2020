<div class="news index">
    <h2><?php __('Categorys'); ?></h2>

    <?php
    $fields = array(
        'Category.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Category.title' => array('edit_link' => array('action' => 'edit', '%id%')),
    );
    $linksUrl = array(
        $html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
    );

    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete', 'admin' => true)), 'confirm' => true)
    );
    echo $list->filter_form($modelName, $filters);
    echo $list->adminIndexList($fields, $categories, $linksUrl, true, $multi_select_actions);
    ?> 
</div>
