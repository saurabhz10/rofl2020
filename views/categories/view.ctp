<div class="categories-container">
    <div class="snippets-box">
<h1><?php echo $categories["Category"]["title"]; ?></h1>
</div>
<div class="snippets-box">
    <?php echo $categoriessnippet; ?>
</div>    

  <div class="news-listing">
    <?php
    $i=0;
    foreach ($galleries as $key => $course) {
        $i++;
        $hideDetails = empty($course["Gallery"]['hide_details_button'])?false:true;
        if(($i % 2) == 1):
        ?>
     
      <div class="row">
          
              <?php endif;?>
          
      <div class="col-md-6">
         
        <div class="items-block">
            <?php if (!empty($course["GalleryImage"][0]['image'])) { ?>
                <a title="<?php echo $course["Gallery"]["title"]; ?>" href="<?php echo $hideDetails ? "#" : Router::url(array('controller'=>'galleries','action' => 'view', $course['Gallery']['id'])) ?>" class="left"><img alt="<?php echo $course["Gallery"]["title"]; ?>" src="<?php echo get_resized_image_url($course["GalleryImage"][0]['image'], 112 , 85 , true ); ?>" /></a>
            <?php } ?>
            <div class="block-details">
              <a title="<?php echo $course["Gallery"]["title"]; ?>" href="<?php echo $hideDetails ? "#" : Router::url(array('controller'=>'galleries','action' => 'view', $course['Gallery']['id'])) ?>"><?php echo $course["Gallery"]["title"]; ?></a>
<!--                <p>
                    <?php // echo $course["Gallery"]["description"]; ?>
                </p>-->
                <?php if(!$hideDetails){ ?>
                <a href="<?php echo Router::url(array('controller'=>'galleries','action' => 'view', $course['Gallery']['id'])) ?>" class="btn-a corner-all">View</a>
                <?php } ?>
            </div>
            <!--<div class="clear"></div>-->
        </div>
        </div>
        <?php 
        
        
        if(($i % 2) == 0): ?>
          </div>
   
           <?php endif;?>
        <?php
        
    }
    ?>  
      <?php if(($i % 2) == 1): ?>
          </div>
    
           <?php endif;?>
</div>

<div class="pagination">
    <?php if ($paginator->numbers()) { 
        $paginator->options(array('url'=>array('controller'=>'categories','action'=>'view/'.$permalink)));
         
        ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>
</div>


<script type="text/javascript">
$(document).ready(function(){
    $(".socialbar").addClass("catClas");
  });

</script>