<h1>Categories</h1>

<div class="snippets-box">
    <?php echo $snippet; ?>
</div>

<div class="news-listing">
    <?php
    foreach ($categories as $key => $link) {
        ?>

        <div class="items-block">
            <?php if (!empty($link["Category"]["image"])) { ?>
                <a target="_blank" title="<?php echo $link["Category"]["title"]; ?>" href="<?php echo $link['Category']['url']; ?>" class="left">
                    <img alt="<?php echo $link["Category"]["title"]; ?>" src="<?php echo get_resized_image_url($link["Category"]["image"], 112 , 85 , true ); ?>" /></a>
            <?php } ?>
            <div class="block-details">
                <h2><a target="_blank" title="<?php echo $link["Category"]["title"]; ?>" href="<?php echo $link['Category']['url']; ?>" target="_blank"><?php echo $link["Category"]["title"]; ?></a></h2>
                <p>
                    <?php echo $link["Category"]["short_description"]; ?>
                </p>
                <a href="<?php echo $link['Category']['url']; ?>" target="_blank" class="btn-a corner-all">Visit Site</a>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>         
</div>
<div class="pagination">
    <?php if ($paginator->numbers()) { ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>