<div class="galleryImages index">
<h1><?php __('Gallery Images');?></h1>
<?php
echo $list->filter_form($modelName, $filters);
$fields=array(
	'GalleryImage.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'GalleryImage.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'GalleryImage.display_order' => array(),
    'GalleryImage.image_full_path' => array('title'=>'Image','format'=>'image','options'=>array('height'=>100,))
);
$links = array(
	$html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')),//, __('Are you sure?', true)),
);

$multi_select_actions=array('delete'=>array('action'=>Router::url(array('action'=>'delete')),'confirm'=>true));

echo $list->adminIndexList($fields, $galleryImages, $links,true,$multi_select_actions);
?>
</div>