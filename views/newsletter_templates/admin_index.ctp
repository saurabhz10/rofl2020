<div class="newsletterTemplates index">
    <h1>Newsletter Templates</h1>
    <?php
    echo $list->filter_form($modelName, $filters);
    $fields = array(
        'NewsletterTemplate.id' => array('edit_link' => array('action' => 'edit', '%id%'), 'title' => 'ID'),
        'NewsletterTemplate.name' => array('edit_link' => array('action' => 'edit', '%id%'), 'title' => 'Name')
    );
    $links = array(
        $html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
        $html->link(__('Preview', true), 'javascript:void(0)', array('onclick' => 'window.open(\'' . Router::url(array('action' => 'view', '%id%')) . '\',\'Preview\', \'width=640,height=600,scrollbars=yes\');', 'class' => 'View'), false, false)
    );
    $subject = $list->adminIndexList($fields, $newsletterTemplates, $links);
    $prms = array();
    foreach ($params as $key => $val) {
        $prms[] = "$key=" . urlencode($val);
    }
    $qs = implode('&', $prms);
    $search = array('/page:', '/sort:', '/direction:');
    $replace = array("?$qs&page=", "&sort=", "&direction=");
    echo str_replace($search, $replace, $subject);
    ?>
</div>
