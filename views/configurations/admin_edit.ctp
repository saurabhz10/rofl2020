<div class='FormExtended'>
    <?php
    $labels = array(
            //"max_use_by_promo_code" => "Max $ Discount Per Booking (for fixed amount Promocodes)"
    );
    ?>
    <form action='<?= Router::url(array('controller' => 'configurations', 'action' => 'admin_edit')) ?>' method="post">
        <?php
        foreach ($config as $key => $value) {

            $ext = substr($key, 0, strpos($key, '.'));
            $name = substr($key, strpos($key, '.') + 1);
//		$value = $config[$section][$key];
            if ($ext == 'pwd') {
                $value = '';
            }
            ?>
            <label for="<?= $key ?>"><?php
                if (!empty($labels[$name]))
                    echo $labels[$name];
                else
                    echo Inflector::humanize($name);
                ?></label>
            <?php
            if ($ext == 'checkbox') {
                $val = $value;
                $value = "1";
                ?>
                <input id='<?= $key ?>' type='hidden'  name="<?= $key ?>" value="0" class='INPUT' />

                <?php
            }
            ?>
            <?php
            if (in_array($key, array('txt.keywords', 'txt.description', 'txt.google_analytics'))) {
                ?>
                <textarea class='INPUT' rows="6" cols="30"  name="<?= $key ?>" id='<?= $key ?>'><?= $value ?></textarea>
                <?php
            } else {
                ?>
                <input id='<?= $key ?>' type='<?= $extensions[$ext] ?>' <?php if ($ext == 'checkbox' && $val == "1") echo 'checked="checked"' ?> name="<?= $key ?>" value="<?= $value ?>" class='INPUT' />



                <?php
            }
        }
        ?>
        <input type="SUBMIT" class="Submit" value='Submit' />
    </form>
</div>