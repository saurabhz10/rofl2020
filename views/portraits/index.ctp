<h1>Portraits</h1>

<div class="snippets-box">
    <?php echo $snippet; ?>
</div>

<div class="news-listing">
    <h2>Portrait Galleries</h2>
    <?php foreach ($portraits as $key => $portrait) { 
        $url = ife(!empty($portrait['Portrait']['gallery_id']), Router::url(array("controller" => "galleries", 'action' => 'view', $portrait['Portrait']['gallery_id'])), $portrait['Portrait']['url']);
        $title = ife(!empty($portrait['Portrait']['gallery_id']), "Gallery", "Site");
        ?>

        <div class="items-block">
            <?php if (!empty($portrait["Portrait"]["image"])) { ?>
                <a href="<?php echo $url; ?>" class="left"><img src="<?php echo get_resized_image_url($portrait["Portrait"]["image"], 112 , 85 , true ); ?>" alt="<?php echo $title; ?>" /></a>
            <?php } ?>
            <div class="block-details">

                <h2><a href="<?php echo $url; ?>"><?php echo $portrait["Portrait"]["title"]; ?></a></h2>
                <p>
                    <?php echo $portrait["Portrait"]["short_description"]; ?>
                </p>
                <a href="<?php echo $url; ?>" class="btn-a corner-all">Visit <?php echo $title; ?></a>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>         
</div>
<div class="pagination">
    <?php if ($paginator->numbers()) { ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>