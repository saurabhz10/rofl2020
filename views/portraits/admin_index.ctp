<div class="news index">
<h2><?php __('Portraits');?></h2>

<?php
$fields=array(
	'Portrait.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Portrait.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Portrait.display_order' => array()
);
$links = array(
	$html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
);

$multi_select_actions=array(
    'delete'=>array('action'=>Router::url(array('action'=>'delete','admin'=>true)),'confirm'=>true)
);
echo $list->filter_form($modelName, $filters);
echo $list->adminIndexList($fields, $portraits, $links,true,$multi_select_actions) ?></div>
