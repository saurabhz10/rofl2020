<h1>Education</h1>

<div class="snippets-box">

</div>
<div class="snippets-box">
    <?php echo $snippet; ?>
</div>
<div class="news-listing">
    <?php
    foreach ($courses as $key => $course) {
        ?>

        <div class="items-block">
            <?php if (!empty($course["Course"]["image"])) { ?>
                <a title="<?php echo $course["Course"]["title"]; ?>" href="<?php echo Router::url(array('action' => 'view', $course['Course']['permalink'])) ?>" class="left"><img alt="<?php echo $course["Course"]["title"]; ?>" src="<?php echo get_resized_image_url($course["Course"]['image'], 112 , 85 , true ); ?>" /></a>
            <?php } ?>
            <div class="block-details">
                <h2><a title="<?php echo $course["Course"]["title"]; ?>" href="<?php echo Router::url(array('action' => 'view', $course['Course']['permalink'])) ?>"><?php echo $course["Course"]["title"]; ?></a></h2>
                <p>
                    <?php echo $course["Course"]["short_description"]; ?>
                </p>
                <a href="<?php echo Router::url(array('action' => 'view', $course['Course']['permalink'])) ?>" class="btn-a corner-all">Details</a>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>         
</div>

<div class="pagination">
    <?php if ($paginator->numbers()) { ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>
