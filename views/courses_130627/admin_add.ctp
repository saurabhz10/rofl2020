
<div class="FormExtended">
    <?php echo $form->create('Course', array('type' => 'file')); ?>
    <?php
    echo $form->input('id', array('class' => 'INPUT'));
    echo $form->input('title', array('class' => 'INPUT', 'id' => 'PageTitle'));
    echo $form->input('permalink', array('class' => 'INPUT', 'id' => 'PagePermalink'));
    echo $form->input('short_description', array('class' => 'INPUT'));
    //echo $form->input('content', array('class' => 'INPUT'));
    $content = '';
    if (!empty($this->data['Course']['content']))
        $content = $this->data['Course']['content'];
    $contentWTB = '';
    if (!empty($this->data['Course']['what_to_bring']))
        $contentWTB = $this->data['Course']['what_to_bring'];
    ?>  
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td valign="top" width="300">
                <?php
                echo $form->label('Use saved template');
                $template_images = array();
                echo $this->element('uboard_left', array('templates' => $templates, 'prefix' => 'templates', 'template_id' => 'UseTemplate', 'current_template' => $this->data['Page']['template_id']));
                echo "<span class='Hints'>Note: if you select a template,it will be delete all the content you have added for this page </span>";
                echo "<div style='display:none;'>";
                echo $form->input('template_id', array(
                    'class' => 'INPUT',
                    'id' => 'UseTemplate',
                    'after' => $html->link('Preview selected template', '#', array('id' => 'TemplatePreviewLink', 'class' => 'Preview'))));
                echo "</div>";
                ?>
            </td>
        </tr>
    </table><br /><br />
    <div class="courseDetails">
        <h5>Course Details</h5>
        <?php
        $detailsLength = 0;
        if (empty($this->data["Course"]["course_details"])) {
            echo $form->input('course_details.0', array('class' => 'INPUT', "label" => false, "div" => "link_page_set", "type" => "textarea", "after" => '<a href="javascript:void(0)" class="remove-element removeEmail">×</a>'));
        } else {
            $courseDetails = explode(",", $this->data["Course"]["course_details"]);
            $detailsLength = count($courseDetails);
            foreach ($courseDetails as $key => $course) {
                echo $form->input("course_details.$key", array("value" => $course, 'class' => 'INPUT', "div" => "link_page_set", "label" => false, "type" => "textarea", "after" => '<a href="javascript:void(0)" class="remove-element removeEmail">×</a>'));
            }
        }
        ?>
        <a href="#" class="addomail AddItem">Add Course Detail</a>

    </div>
    <?php
    //echo $fck->create('Course', 'content', $content);
    echo $fck->create('Course', 'content', $content, 'Default', Router::url('/css/screen.css'), array('height' => 550, "label" => "long_description", 'style' => "body {background:#FFF }"));
    echo $fck->create('Course', 'what_to_bring', $contentWTB, 'Default', Router::url('/css/screen.css'), array('height' => 550, 'style' => "body {background:#FFF }"));
    echo $form->input('image', array('class' => 'INPUT', 'type' => 'file', 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['Course']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['Course']['image'] : '')))
    ));

    echo $form->input('display_order', array('class' => 'INPUT'));
    echo $form->input('active', array('class' => 'INPUT'));
    ?>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (is_array($this->data) && $this->data['Course']['id']) {
        echo $this->element('save_as_new', array('model' => 'Course'));
    }
    ?><?php echo $form->end(); ?>
</div>
<?php e($this->element('permalink')) ?>


<script type="text/javascript">
    var email_templates =<?= $javascript->object($templates); ?>;



    function loadTemplate(id) {
        var oEditor = FCKeditorAPI.GetInstance('data[Course][content]');
        var OldText = oEditor.GetHTML();
        var NewText = (id) ? OldText : "";
        if (OldText) {
            if (confirm('Are you sure you want to update page content?')) {
                NewText = (id) ? email_templates[id].html : "";
                console.log(id);
                oEditor.SetHTML(NewText);
            }
        } else {
            NewText = email_templates[id].html;
            oEditor.SetHTML(NewText);
        }

    }

    $(function() {
        var $eindex = <?php echo ($detailsLength - 1); ?>;
        $(".addomail").click(function() {
            var $NewEmail = '<div class="link_page_set"><textarea name="data[Course][course_details][0]" cols="30" rows="6" class="INPUT" id="CourseCourseDetails0"></textarea><a href="javascript:void(0)" class="remove-element removeEmail">×</a></div>';
            $eindex++;
            $(".addomail").before($NewEmail.replace(/\[0\]/g, "[" + $eindex + "]"));
            return false;
        });

        $(".remove-element").live('click', function() {
            $(this).parents('.link_page_set').remove();
            return false;
        });
    });
</script>

</script>