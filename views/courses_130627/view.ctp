<h1><?php echo $courses["Course"]["title"]; ?></h1>
<div class="snippets-box">
    <img src="<?php echo $courses["Course"]['image_full_path']; ?>" class="post-img left" title="<?php echo $courses["Course"]['title']; ?>"/>
    <?php echo $courses["Course"]["content"]; ?>
    <div class="clear"></div>
</div>
<div class="snippets-box">
    <h2>Course details:</h2>

    <ul>
        <?php
        $courseDetails = explode(",", $courses["Course"]["course_details"]);
        foreach ($courseDetails as $key => $course) {
 
            ?>
            <li><?php echo nl2br($course); ?></li>
            <?php
        }
        ?>
    </ul>
    <br />
    <br />
    <h2>What to bring:</h2>
    <?php echo $courses["Course"]["what_to_bring"]; ?>

    <a href="#" class="btn-a corner-all left"  id="enquire" data-reveal-id="enquire">Enquire</a>

</div>