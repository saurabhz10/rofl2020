<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?= $html->charset(); ?>
        <title>
            <?= $title_for_layout; ?>
        </title>
        <meta name="keywords" content="<?= $metaKeywords ?>" />
        <meta name="description" content="<?= $metaDescription ?>" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php
        echo $html->css(array('screen', "notifications"));
        echo $javascript->link(array("jquery-1.7.1.min.js", "jquery.reveal.js"));
        echo $scripts_for_layout;
        ?>

        <script type="text/javascript">var switchTo5x = true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "b80af988-c9c6-4f91-b28f-d136b241b523", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
        <script type="text/javascript">
<?php echo $config['txt.google_analytics'] ?>
        </script>
    </head>
    <body onload="test()">
        <div class="wrap">
            <div class="content" style="padding-top: 10px; padding-left: 10px;">
                <?php echo $content_for_layout; ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <!-- / footer -->
        </div>
        <?php echo $javascript->link(array("custom.js", "jquery.masonry.min.js")); ?>

        <script type="text/javascript">

            function test() {
                $('#portfolio').masonry({
                    // set columnWidth a fraction of the container width
                    columnWidth: function (containerWidth) {
                        return containerWidth / 34;
                    }
                });
            }
        </script>
    </body>
</html>