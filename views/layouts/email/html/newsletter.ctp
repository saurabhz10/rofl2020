<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $title_for_layout;?></title>
</head>
<body>
<table bgcolor="#d9d9d9" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td><br>
			<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
				<tbody><tr>
					<td>
					<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
					<tbody><tr>
						<td><a href="http://www.richardofarrell.com.au/"><img src="http://test.silvertrees.net/test-mm/rof/images/rof-email-header.jpg" alt="Richard O'Farrell Photography website" border="0"></a>
						</td>
					</tr>
					<tr bgcolor="#d9d9d9">
					<td><p>&nbsp;</p></td>
					</tr>
					</tbody></table>
							
			<!--editable content goes here -->
					<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
						<tbody><tr>
							<td style="padding:15px;">
								
								<font style="font-size:12px; font-family:Arial, sans-serif; color:#666666;">

<?php echo $content_for_layout;?>


</font>
								
								
								
								
							</td>
							</tr>
						</tbody></table>
				<!--end of editable content -->
					<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
					<tbody><tr bgcolor="#d9d9d9">
						<td><p>&nbsp;</p></td>
					</tr><tr>
						<td><p style="font-size:11px; font-family:Arial, sans-serif; color:#666666; padding:15px;" align="center">&copy; Richard O'Farrell &ndash; 2013 <em> &nbsp; To stop receiving these emails, please reply with 'unsubscribe' in the subject</em></p></td>
					</tr>
					<tr bgcolor="#d9d9d9">
						<td><p>&nbsp;</p></td>
					</tr><tr>
					</tr></tbody></table>
			</td>
			</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

		
          


</body>
</html>