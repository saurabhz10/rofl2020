<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
	<title><?php echo $title_for_layout;?></title>
</head>
<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#D9D9D9">
    <tbody>
        <tr>
            <td valign="top">
            <table width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center" style="border:1px solid #FFF">
                <tbody>
                    <tr>
                        <td height="110" align="center" style="border-bottom:5px solid #D9D9D9" colspan="3"><font face="Arial, Helvetica, sans-serif" style="font-size:28px;"><a href="#" style="text-decoration:none; color:#555555">RICHARD O'FARRELL PHOTOGRAPHY</a></font></td>
                    </tr>
                    <tr>
                        <td colspan="3"><img height="30" alt="" src="img/spacer.gif"></td>
                    </tr>
                    <tr>
                        <td width="30">&nbsp;</td>
                        <td>
                            <br />
                            
                            <?php echo $content_for_layout;?>
                            
                        <br />
                        </td>
                        <td width="30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3"><img height="40" alt="" src="img/spacer.gif"></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
<table width="600" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
        <tr>
            <td><img height="20" src="img/spacer.gif" alt=""></td>
        </tr>
        <tr>
            <td align="center"><span style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000">&copy; 2013 Richard O'Farrell </span></td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>

</body>
</html>