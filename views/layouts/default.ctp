<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<?= $html->charset(); ?>
        <title>
			<?= $title_for_layout; ?>
        </title>
        <meta name="keywords" content="<?= $metaKeywords ?>" />
        <meta name="description" content="<?= $metaDescription ?>" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width">

		<?php
		echo $html->css(array('screen.css?v=1.12', "notifications"));
		echo $javascript->link(array("jquery-1.7.1.min.js", "jquery.reveal.js"));
		echo $scripts_for_layout;
		?>

        <script type="text/javascript">var switchTo5x = true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "b80af988-c9c6-4f91-b28f-d136b241b523", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
        <script type="text/javascript">
<?php echo $config['txt.google_analytics'] ?>
        </script>
    </head>
    <body onload="test()">
		<?php echo $form->input("enquriySubject", array("type" => "hidden", "value" => isset($galleryimages[0]['GalleryImage']['title']) ? $galleryimages[0]['GalleryImage']['title'] : "")); ?>

        <div id="enquire" class="reveal-modal"> 
            <a class="close-reveal-modal">&#215;</a>
            <h2>Enquiry form:</h2>
            <p id="enquriySubjectHead"></p>
			<?php echo $this->element("contactUsAajax"); ?>

        </div>
        <div class="wrap">
			<?php if ($this->name != 'Galleries'): ?>
				<div class="header">
					<div class="logo left">
						<a href="<?php echo Router::url('/') ?>">
							<?php /* <img src="<?php echo $this->webroot; ?>/css/img/logo.png" alt="" title="" /> */ ?>
							<img src="<?php echo $siteLogos["Logo"]["image_thumb_full_path"]; ?>" alt="" title="" />

						</a>
					</div>
					<div class="header-meta left">
						<h2>RICHARD O'FARRELL ART</h2>
						<h3>
							<?php
							$arr_links = array();
							foreach ($resources as $resource) {

								if (!empty($resource["Resource"]["url"]))
									$arr_links[] = ' <a href="' . $html->url(array("controller" => "galleries", "action" => "view")) . '">' . $resource["Resource"]["title"] . '</a>';
								else
									$arr_links[] = $resource["Resource"]["title"];
							}
							?>
							<?php echo implode(", ", $arr_links) ?>
						</h3>
					</div>
					<div class="clear"></div>
				</div>
				<!-- / header -->
			<?php endif; ?>
            <div id="nav" class="main-nav">
				<?php echo $this->element('topmenu', array('topmenus' => $topmenus)); ?>
                <div class="clear"></div>
            </div>
            <div class="content">
				<?php
				if ($this->action != "home") {
					?>

					<div class="socialbar">
						<!-- <span class='st_twitter_large' displayText='Tweet'></span>
						<span class='st_facebook_large' displayText='Facebook'></span>
						<span class='st_linkedin_large' displayText='LinkedIn'></span>
						<span class='st_email_large' displayText='Email'></span>
						<span class='st_sharethis_large' displayText='ShareThis'></span> -->
						
				    	<img class="main_prev_image" src="/img/uploads/168ba_Kodak_Logo_Web.jpg" style="position: relative;right: 35px; top: 10px;">
					    
					</div>
					<div class="clear"></div>
					<?php
				}
				?>
				<?php echo $content_for_layout; ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <!-- / footer -->

            <div class="footer">
                <div class="copyrights left"><?php echo $footer_snippet; ?> </div>

                <div class="right">
                    <div class="newsletters">
                        <div class="corner-all quick-form">
							<?= $form->create('Newsletter', array('id' => 'NewsletterForm', 'action' => 'add')) ?>
                            <label >Subscribe to our newsletter: </label>
                            <div class="newsletters-form left">
								<?= $form->input('email', array('label' => false, 'div' => false, 'class' => 'mail-input', 'placeholder' => 'Email Here')) ?>
                                <button type="submit"><span class="btn-a corner-all">Join</span></button>                             </div>
							<?= $form->end() ?>
                        </div> 
                    </div> 
                    <div id="subscription-form-message"> </div>
                </div>

                <div class="clear"></div>
            </div>


        </div>
		<?php echo $javascript->link(array("custom.js", "jquery.masonry.min.js")); ?>

        <script type="text/javascript">

			function test() {
				$('#portfolio').masonry({
					// set columnWidth a fraction of the container width
					columnWidth: function(containerWidth) {
						return containerWidth / 34;
					}
				});
			}

			$(function() {
				if ($('.header').length) {
					var header = $('.header');
					var height = header.outerHeight();
					smoothTransition = function(e) {
						e.preventDefault();
						var href = this.href;
						$('.header').animate({marginTop: -height, opacity: 0, marginBottom: 0}, {duration: 900, complete: function() {
							window.location = href;
						}});
					};

					$('a').each(function() {
						if ($(this).prop('href').match(/\/galleries/i)) {
							$(this).on('click', smoothTransition);
						}
					});
				}

				$("#enquire").live("click", function() {
					//                if("<?php //echo ife(!empty($galleryimages[0]['GalleryImage']['title']) , nl2br($galleryimages[0]['GalleryImage']['title']) : $courses['Course']['title']));)                                      ?>"){
					//                    
					//                }
					if ("1" == "<?php echo ife(isset($courses['Course']['title']), 1, 0) ?>") {
						$("#enquriySubject").val("<?php echo $courses['Course']['title']; ?>");
					}
					$('#enquriySubjectHead').html("<strong>Subject:</strong> " + $("#enquriySubject").val() + " - Richard O'Farrell website enquiry");
					$('#ContactSubject').val($("#enquriySubject").val() + " - Richard O'Farrell website enquiry");
				});
				ajaxize_form();
				function ajaxize_form() {
					$('#contact').bind('submit', function() {
						$('#enquiry-form-message').addClass('form-loader');
						$('#enquiry-form-message').html('Please wait');
						$.ajax({
							cache: false,
							type: 'POST',
							url: this.action,
							data: $(this).serialize(),
							dataType: 'json',
							success: function(data) {
								if (data.success == 1) {
									$('#enquiry-form-message').removeClass('form-loader');
									$('#enquiry-form-message').html(data.message);
									$("#contact").find("input[type=text]").val("");
									$("#contact").find("textarea").val("");
									$("#flashMessage").removeClass("Errormessage").addClass("Sucmessage").html(data.message);
									$(".error-message").remove();
									$("#flashMessage").remove();
									$(".contact-form").prepend('<div id="flashMessage" class="flashMessage Sucmessage">Your message has been sent.</div>');
									//                                $('.close-reveal-modal').trigger('click');
									//                                $('.close-reveal-modal').click();
								}
								else if (data.status == 'error') {
									$('#enquire').html(data.content);
									ajaxize_form();
								}

								return false;
							}

						});
						return false;
					});
				}
			});</script>
        <script type="text/javascript" >


			$('#NewsletterForm').submit(function() {


				var post_data = $(this).serialize();
				var url = $(this).attr('action');
				$('#subscription-form-message').html('');
				$('#subscription-form-message').addClass('form-loader');
				$('#subscription-form-message').html('Please wait');
				$.ajax({
					cache: true,
					url: url,
					type: "POST",
					dataType: 'jsonp',
					data: post_data,
					success: function(data) {
						$('#subscription-form-message').removeClass('form-loader');
						$('#subscription-form-message').removeClass('form-succ');
						$('#subscription-form-message').removeClass('validate-email-error');
						if (data['success']) {
							$('#subscription-form-message').html(data['message']);
							$('#subscription-form-message').addClass('form-succ');
							$('#NewsletterEmail').val('');
						} else {

							$('#subscription-form-message').html(data['error']['email']);
							$('#subscription-form-message').addClass('validate-email-error');
						}
					}
				});
				return false;
			});

        </script>
    </body>
</html>