<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d9d9d9">
    <tr>
        <td><table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                <tr>
                    <td><table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                            <tr>
                                <td><a href="http://www.richardofarrell.com.au/"><img border="0" src="<?php echo Router::url("/css/img/",TRUE) ?>rof-email-header.jpg" alt="Richard O'Farrell Photography website" /></a></td>
                            </tr>
                            <tr bgcolor="#d9d9d9">
                                <td><p>&nbsp;</p></td>
                            </tr>
                        </table>


                        <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                            <tr>
                                <td><br />
                                    <br />
                                    <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td>
                                                <!--editable content goes here -->
                                                <?php echo $content_for_layout; ?>
                                                <!--end of editable content -->
                                            </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>


                        <table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                            <tr bgcolor="#d9d9d9">
                                <td><p>&nbsp;</p></td>
                            <tr>
                                <td><p style="font-size:11px; font-family:Arial, sans-serif; color:#666666; padding:15px;" align="center">&copy; Richard O'Farrell &ndash; 2013 <em> &nbsp; To stop receiving these emails, please reply with 'unsubscribe' in the subject</p></td>
                            </tr>
                            <tr bgcolor="#d9d9d9">
                                <td><p>&nbsp;</p></td>
                            <tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
</table>
