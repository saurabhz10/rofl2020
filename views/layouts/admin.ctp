<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title_for_layout; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<?php
echo $html->css(array('admin', 'notifications', 'chosen'));
echo $javascript->link(array('jquery', 'chosen.jquery.min'));
echo $scripts_for_layout;
 ?>
 </head>
 <style type="text/css">
.side-nav li a, .widget-header, .btn{ background-color:<?= $config['txt.theme_colour'] ?>;}
 </style>
<body>
<div class="layout">
  <div class="sidebar left">
    <div class="logo"> <a href="http://www.silvertrees.net/" target="_blank"><img src="<?php echo Router::url('/css/admin/silvertrees_webdevelopment.png') ?>" alt="silvertrees webdevelopment" /></a> </div>
    <div class="side-nav">
      <ul>
        <?php echo $sidemenu->outputAdminMenu($selectedMenu); ?>
      </ul>
    </div>
    <!-- / side-nav -->
  </div>
  <div class="main"> 
    <div class="admin-bar">
    
    
    <div class="breadcrumbs left">
            <?php
                                /* @var $html HtmlHelper */
                                $html->addCrumb($config['txt.site_name'], '/');
                                if(!isset($crumbs['prefix']))
                                    $html->addCrumb(Inflector::humanize(__($this->params['prefix'], true)), "/{$this->params['prefix']}");
                                else if(!empty($crumbs['prefix'][0]))
                                    $html->addCrumb($crumbs['prefix'][0], "/{$crumbs['prefix'][1]}");
    
                                if(!isset($crumbs['controller']))
                                    $html->addCrumb($titleAlias, array('controller' => $this->params['controller'], 'action' => 'index'));
                                else if(!empty($crumbs['controller'][0]))
                                    $html->addCrumb($crumbs['controller'][0], $crumbs['controller'][1]?"/{$crumbs['controller'][1]}":null);
    
                                if(!isset($crumbs['action'])) {
                                    $prefix = $this->params['prefix'];
                                    $action=substr($this->action, strlen("{$this->params['prefix']}_"));
                                    if ($action != 'index') {
                                        $html->addCrumb(Inflector::humanize($action));
                                    }
                                }
                                else if(!empty($crumbs['action'][0]))
                                    $html->addCrumb($crumbs['action'][0], $crumbs['action'][1]?"/{$crumbs['action'][1]}":null);
    
    
                                if(isset($crumbs['more'])) {
                                    foreach($crumbs['more'] as $crumb)
                                        $html->addCrumb($crumb[0], $crumb[1]?"/{$crumb[1]}":null);
                                }
    
    
                                echo $html->getCrumbs(' &raquo; ');
                                ?>
          </div>
      <div class="admin-actions right">
        <a href="<?php echo  Router::url(array('controller' => '/', 'action' => 'index','admin'=>false)); ?>" class="dashboard" target="_blank">Preview Live Site</a>
        <a href="<?php echo Router::url(array('controller' => 'admins', 'action' => 'logout','admin'=>true)); ?>" class="logout">Logout</a>
        </div>
        <div class="clear"></div>
    </div>
    <!-- / admin-bar -->
    <div class="contents">
      <?php $session->flash();
        echo $content_for_layout; ?>
    </div>
    <!-- / contents --> 
  </div>
  <div class="clear"></div>
</div>
</body>
</html>
