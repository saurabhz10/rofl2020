<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $title_for_layout; ?></title>
		<meta name="keywords" content="keywords, keywords, keywords," />
		<meta name="description" content="Description." />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<?php echo $html->css('admin'); ?>
		<?php echo $javascript->link('jquery'); ?>
		<?php echo $scripts_for_layout; ?>
	</head>
	<body>
		<div id="Adminpanel"><!-- Side Bar -->
			<div id="SideBar">
				<div id="TopBlock">
					<div id="Logo"><a href="http://www.silvertrees.net" title=" Silver trees web development"> Silver trees web development </a></div>
					<div id="SideMenu"><?php echo $sidemenu->outputAdminMenu($selectedMenu); ?>
					</div>
				</div>
				<div id="Bottomblock"><a href="http://www.silvertrees.net/" title="Silvertrees seb development" >Silvertrees seb development</a></div>
			</div>
			<div id="Contents"><!-- News Block --->
				<?php if ($this->params['prefix'] != 'admin'): ?><div class='Logout'><?= $html->link('Logout', array('controller' => 'companies',  'action' =>'logout', 'prefix' => 0)) ?></div><?php endif; ?>
				<div class="Blcok">
                    <div class="Logout">
                                <?= $html->link('Home', array('controller' => '/', 'action' => 'index','admin'=>false), array('class' => 'AdminHome')) ?>
                                <?= $html->link('Logout', array('controller' => 'admins', 'action' => 'logout','admin'=>true), array('class' => 'AdminLogout')) ?>                    
                    </div>
					<div class="BlcokHead"><?php echo $html->image('admin/block_arrow.gif'); ?>
						<h3>
							<?php
							/* @var $html HtmlHelper */
							$html->addCrumb($config['txt.site_name'], '/');
							if(!isset($crumbs['prefix']))
								$html->addCrumb(Inflector::humanize(__($this->params['prefix'], true)), "/{$this->params['prefix']}");
							else if(!empty($crumbs['prefix'][0]))
								$html->addCrumb($crumbs['prefix'][0], "/{$crumbs['prefix'][1]}");

							if(!isset($crumbs['controller']))
								$html->addCrumb($titleAlias, array('controller' => $this->params['controller'], 'action' => 'index'));
							else if(!empty($crumbs['controller'][0]))
								$html->addCrumb($crumbs['controller'][0], $crumbs['controller'][1]?"/{$crumbs['controller'][1]}":null);

							if(!isset($crumbs['action'])) {
								$prefix = $this->params['prefix'];
								$action=substr($this->action, strlen("{$this->params['prefix']}_"));
								if ($action != 'index') {
									$html->addCrumb(Inflector::humanize($action));
								}
							}
							else if(!empty($crumbs['action'][0]))
								$html->addCrumb($crumbs['action'][0], $crumbs['action'][1]?"/{$crumbs['action'][1]}":null);


							if(isset($crumbs['more'])) {
								foreach($crumbs['more'] as $crumb)
									$html->addCrumb($crumb[0], $crumb[1]?"/{$crumb[1]}":null);
							}


							echo $html->getCrumbs(' &raquo; ');
							?>
						</h3>
						<div class="clear"></div>
					</div>
					<div class="BlcokContents"><?php $session->flash();
echo $content_for_layout; ?></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</body>
</html>