<div class="FormExtended">
	<?php 
	echo $form->create('Tab');

	echo $form->input('id', array('class' => 'INPUT required'));
	echo $form->input('title', array('class' => 'INPUT required'));
	echo $form->input('content', array('class' => 'INPUT'));
	echo $form->input('active', array('class' => 'INPUT required'));
	echo $form->input('display_order', array('class' => 'INPUT required number'));

	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();

	?>
</div>
