<div class="ads index">
<h2><?php __('Ads');?></h2>

<?php
echo $list->filter_form($modelName, $filters);
$fields=array(
	'Ad.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Ad.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Ad.place' => array(),
	//'Ad.display_order' => array()
);
$links = array(
	$html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
);

$multi_select_actions=array(
    'delete'=>array('action'=>Router::url(array('action'=>'delete_multi')),'confirm'=>true)
);

echo $list->adminIndexList($fields, $ads, $links,true,$multi_select_actions,null,true);

//echo $this->element('add_new');

?></div>