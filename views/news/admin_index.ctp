<div class="news index">
<h2><?php __('News');?></h2>

<?php
$fields=array(
	'News.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'News.title' => array('edit_link' => array('action' => 'edit', '%id%')),
    'News.post_date' => array('date_format'=>'d/m/Y h:i A','edit_link' => array('action' => 'edit', '%id%')),
	'News.permalink' => array('edit_link' => array('action' => 'edit', '%id%'))
);
$links = array(
	$html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
);

$multi_select_actions=array(
    'delete'=>array('action'=>Router::url(array('action'=>'delete','admin'=>true)),'confirm'=>true)
);
echo $list->filter_form($modelName, $filters);
echo $list->adminIndexList($fields, $news, $links,true,$multi_select_actions) ?></div>
