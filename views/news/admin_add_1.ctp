
<div class="FormExtended">
    <?php echo $form->create('News', array('type' => 'file')); ?>
    <?php
    echo $form->input('id', array('class' => 'INPUT'));
    echo $form->input('title', array('class' => 'INPUT', 'id' => 'PageTitle'));
    echo $form->input('permalink', array('class' => 'INPUT', 'id' => 'PagePermalink'));
    echo $form->input('short_description', array('class' => 'INPUT'));
    //echo $form->input('content', array('class' => 'INPUT'));
    $content = '';
    if (!empty($this->data['News']['content']))
        $content = $this->data['News']['content'];
    ?>  
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td valign="top" width="300">
                <?php
                echo $form->label('Use saved template');
                $template_images = array();
                echo $this->element('uboard_left', array('templates' => $templates, 'prefix' => 'templates', 'template_id' => 'UseTemplate', 'current_template' => $this->data['Page']['template_id']));
                echo "<span class='Hints'>Note: if you select a template,it will be delete all the content you have added for this page </span>";
                echo "<div style='display:none;'>";
                echo $form->input('template_id', array(
                    'class' => 'INPUT',
                    'id' => 'UseTemplate',
                    'after' => $html->link('Preview selected template', '#', array('id' => 'TemplatePreviewLink', 'class' => 'Preview'))));
                echo "</div>";
                ?>
            </td>
        </tr>
    </table>
    <?php
    //echo $fck->create('News', 'content', $content);
    echo $fck->create('News', 'content', $content, 'Default', Router::url('/css/screen.css'), array('height' => 550,"label"=>"Long Description", 'style' => "body {background:#FFF }"));
    echo $form->input('image', array('class' => 'INPUT', 'type' => 'file', 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['News']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['News']['image'] : '')))
    ));
    //echo $form->input('display_order', array('class' => 'INPUT'));
    echo $form->input('post_date', array('class' => 'INPUT'));
    echo $form->input('keywords', array('class' => 'INPUT'));
    
    echo $form->input('home_page', array('class' => 'INPUT'));
    echo $form->input('active', array('class' => 'INPUT'));
    ?>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (is_array($this->data) && $this->data['News']['id']) {
        echo $this->element('save_as_new', array('model' => 'News'));
    }
    ?><?php echo $form->end(); ?>
</div>
<?php e($this->element('permalink')) ?>


<script type="text/javascript">
    var email_templates =<?= $javascript->object($templates); ?>;



    function loadTemplate(id)
    {
        var oEditor = FCKeditorAPI.GetInstance('data[News][content]');
        var OldText = oEditor.GetHTML();
        var NewText = (id) ? OldText : "";
        if (OldText) {
            if (confirm('Are you sure you want to update page content?')) {
                NewText = (id) ? email_templates[id].html : "";
                console.log(id);
                oEditor.SetHTML(NewText);
            }
        } else {
            NewText = email_templates[id].html;
            oEditor.SetHTML(NewText);
        }

    }

</script>