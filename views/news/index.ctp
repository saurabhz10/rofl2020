<h1>Latest News</h1>

<div class="snippets-box">
    <?php echo $snippet; ?>
</div>

<div class="news-listing">
    <?php
    foreach ($news as $key => $newss) {
        ?>

        <div class="items-block">
            <?php
            if (!empty($newss["News"]["image"])) {
                $image_path = $newss["News"]["image"];
            } else {
                $image_path = "default.png";
            }
            
            ?>
            <a title="<?php echo $newss["News"]["title"]; ?>" href="<?php echo Router::url(array('action' => 'view', $newss['News']['permalink'])) ?>" class="left">

                <img src="<?php echo get_resized_image_url($image_path, 112 , 85 , true ); ?>" alt="<?php echo $newss["News"]["title"]; ?>" />

            </a>

            <div class="block-details">
                <h2><a href="<?php echo Router::url(array('action' => 'view', $newss['News']['permalink'])) ?>"><?php echo $newss["News"]["title"]; ?></a></h2>
                <p>
                    <?php echo $newss["News"]["short_description"]; ?>
                </p>
                <a href="<?php echo Router::url(array('action' => 'view', $newss['News']['permalink'])) ?>" class="btn-a corner-all">Read more</a>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>         
</div>
<div class="pagination">
    <?php if ($paginator->numbers()) { ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>