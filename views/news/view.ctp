<h1><?php echo $news["News"]["title"]; ?></h1>
<div class="news-content">
    <p>
        <?php
        if (!empty($news["News"]["image"])) {
                $image_path = $news["News"]["image"];
            } else {
                $image_path = "default.png";
            }
            ?>
        <img class="post-img left" src="<?php echo get_resized_image_url($image_path, 220 ); ?>" alt="<?php echo $news["News"]["title"]; ?>">
        
        <?php echo $news["News"]["content"]; ?></p>
    
</div>
