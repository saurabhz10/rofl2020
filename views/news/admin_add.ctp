
<div class="FormExtended">
    <?php echo $form->create('News', array('type' => 'file')); ?>
    <?php
    echo $form->input('id', array('class' => 'INPUT'));
    echo $form->input('title', array('class' => 'INPUT', 'id' => 'PageTitle'));
    echo $form->input('permalink', array('class' => 'INPUT', 'id' => 'PagePermalink'));
    echo $form->input('short_description', array('class' => 'INPUT'));
	/* @var $fck FckHelper */
//    echo $fck->input('content', array('class' => 'INPUT'));
	echo $fck->create('News', 'content', null, 'Default', Router::url('/css/screen.css'), array('height' => 550, 'style' => "body {background:#FFF }"));
//    $content = '';
//    if (!empty($this->data['News']['content']))
//        $content = $this->data['News']['content'];
//   
//    echo $fck->create('News', 'content', $content);
//    echo $fck->create('News', 'content', $content, 'Default', Router::url('/css/screen.css'), array('height' => 550,"label"=>"Long Description", 'style' => "body {background:#FFF }"));
    echo $form->input('image', array('class' => 'INPUT', 'type' => 'file', 'between' =>
        $this->element('image_input_between', array('info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($this->data) ? $this->data['News']['id'] : null), 'base_name' => (is_array($this->data) ? $this->data['News']['image'] : '')))
    ));
    //echo $form->input('display_order', array('class' => 'INPUT'));
    echo $form->input('post_date', array('class' => 'INPUT'));
    echo $form->input('keywords', array('class' => 'INPUT'));
    
    echo $form->input('home_page', array('class' => 'INPUT'));
    echo $form->input('active', array('class' => 'INPUT'));
    ?>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php
    if (is_array($this->data) && $this->data['News']['id']) {
        echo $this->element('save_as_new', array('model' => 'News'));
    }
    ?><?php echo $form->end(); ?>
</div>
<?php e($this->element('permalink')) ?>


<script type="text/javascript">
    var email_templates =<?= $javascript->object($templates); ?>;



    function loadTemplate(id)
    {
        var oEditor = FCKeditorAPI.GetInstance('data[News][content]');
        var OldText = oEditor.GetHTML();
        var NewText = (id) ? OldText : "";
        if (OldText) {
            if (confirm('Are you sure you want to update page content?')) {
                NewText = (id) ? email_templates[id].html : "";
                console.log(id);
                oEditor.SetHTML(NewText);
            }
        } else {
            NewText = email_templates[id].html;
            oEditor.SetHTML(NewText);
        }

    }

</script>