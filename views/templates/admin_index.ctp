<div class="templates index">
    <h1><?php __('Templates'); ?></h1>
    <?php
    echo $list->filter_form($modelName, $filters);
    $fields = array(
        'Template.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'image' => array("options"=>array('width'=>'90','height'=>'60','src'=>'".$row["Template"]["image_thumb_full_path"]."'),"format"=>"image" ),
        'Template.title' => array('edit_link' => array('action' => 'edit', '%id%')));
    $links = array(
        $html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), //, __('Are you sure?', true)),
    );

    $multi_select_actions = array('delete' => array('action' => Router::url(array('action' => 'delete')), 'confirm' => true));

    echo $list->adminIndexList($fields, $templates, $links, true, $multi_select_actions);
    ?>
</div>