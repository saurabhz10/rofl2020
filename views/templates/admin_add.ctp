<div class="FormExtended">
	<?php 
	echo $form->create('Template',array('type' => 'file'));

	echo $form->input('id', array('class' => 'INPUT required'));
	echo $form->input('title', array('class' => 'INPUT required'));
	echo $form->input('image', array('class' => 'INPUT required', 'type' => 'file','between'=>
		$this->element('image_input_between',array('info'=>$image_settings['image'],'field'=>'image','id'=>(is_array($this->data)?$this->data['Template']['id']:null),'base_name'=>(is_array($this->data)?$this->data['Template']['image']:'')))
	));

	echo $fck->create('Template','html',$this->data['Template']['html'],'Default',false,array('width' => 800,'height'=>800));

	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();

	?>
</div>
