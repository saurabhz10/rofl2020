<div class="FormExtended">
	<?php 
	echo $form->create('Ad',array('type' => 'file'));

	echo $form->input('id', array('class' => 'INPUT required'));
 
 	echo $form->input('title', array('class' => 'INPUT'));
	echo $form->input('description', array('class' => 'INPUT'));      
	echo $form->input('image', array('class' => 'INPUT', 'type' => 'file','between'=>
		$this->element('image_input_between',array('info'=>$image_settings['image'],'field'=>'image','id'=>(is_array($this->data)?$this->data['Ad']['id']:null),'base_name'=>(is_array($this->data)?$this->data['Ad']['image']:'')))
	));

	echo $form->input('url', array('class' => 'INPUT'));
	echo $form->input('display_order', array('class' => 'INPUT number'));
	echo $form->input('active', array('class' => 'INPUT'));

	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	if(!empty($this->data['Ad']['id'])) {
		echo $this->element('save_as_new',array('model'=>'Ad'));
	}
	echo $form->end();

	?>
</div>
 