<div class="newsMessages index">
    <h1>Newsletter Messages</h1>
    <?php
    echo $list->filter_form($modelName, $filters);
    $form_email_code = '
			<form action=\"' . Router::url(array('action' => 'send_test_email')) . '\" method=\"post\">
			<input type=\"hidden\" name=\"id\" value=\"{$row[$model]["id"]}\" >
			<input type=\"text\" name=\"email_address\"  >
			<input class=\"button-secondary\" type=\"submit\"  value=\"Send\" >
			</form>
			';
    $fields = array(
        'NewsMessage.id' => array('edit_link' => array('action' => 'edit', '%id%'), 'title' => 'ID'),
        'NewsMessage.subject' => array('edit_link' => array('action' => 'edit', '%id%'), 'title' => 'Subject'),
        'send' => array('php_expression' => $form_email_code, 'title' => 'Send Test Message', 'edit_link' => '')
    );
    $links = array(
        $html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit', 'title' => 'Edit')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete', 'title' => 'Delete'), __('Are you sure?', true)),
        $html->link(__('Preview', true), 'javascript:void(0)', array('onclick' => 'window.open(\'' . Router::url(array('action' => 'view', '%id%')) . '\',\'Preview\', \'width=640,height=600,scrollbars=yes\');', 'class' => 'View', 'title' => 'View'), false, false),
        $html->link(__('export as HTML', true), array('action' => 'export', '%id%'), array('class' => 'htmlexport', 'alt' => 'export as HTML', 'title' => 'export as HTML')),
    );
//	$multi_select_actions = array(
//		'delete' => array('action' => Router::url(array('action' => 'delete_multi', 'admin' => true)), 'confirm' => true)
//	);
    echo $list->adminIndexList($fields, $newsMessages, $links); //, true, $multi_select_actions);
    ?>
</div>
<a href="#" class="Sendgroup Schedule btn button-primary">Schedule </a>
<div class="SendgroupFrom FormExtended" style="display: block;">
    <?= $this->element('add_schedule') ?>
</div>
<script type="text/javascript">
    $('.SendgroupFrom').hide();

    $(document).ready(function() {
        $('.Sendgroup').click(function() {
            $('.SendgroupFrom').show();
            return false;
        });
    });
</script>