<?php echo $javascript->link('jscalendar/calendar.js'); ?>
<?php echo $javascript->link('jscalendar/lang/calendar-en.js'); ?>
<?php echo $javascript->link('common.js'); ?>
<!-- CSS Theme-->
<?php echo $html->css('../js/jscalendar/skins/aqua/theme');?>
<?php echo $html->css('calendar.css');?>
<div class="FormExtended">
	<?php echo $form->create('NewsMessage',array('action'=>($this->action=='admin_send_to_group')?'send_to_group':'send_to_staff'));?>
	<?php

    echo $form->input('subject', array('class' => 'INPUT required',"value"=>empty($subject)?"":$subject));
    if($this->action=='admin_send_to_group'){
    echo $form->input('group_id', array('class' => 'INPUT required','label'=>'Group','id'=>'groups'));}
    else{
    echo $form->input('staffscategory_id', array('class' => 'INPUT required','label'=>'category','id'=>'staff'));
    }
     echo $form->hidden('news_message_id', array('class' => 'INPUT required','value'=>$id));
     echo '<div class="input text">'.$datePicker->picker('day',array('div'=>'','class'=>'INPUT','label'=>'Date : ')).'</div>'. ' ' ;
     echo "<label>Time</label>";
	echo $form->select('hour', range(0, 23), null, array('class' => 'INPUT'), false) . ' : ' . $form->select('minute', range(0, 59), null, array('class' => 'INPUT'), false);
	?>
	<?php echo $form->submit(__('Send', true), array('class' => 'Submit')) ?>
	<?php echo $form->end();
 ?>


</div>
