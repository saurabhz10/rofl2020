<div class="FormExtended">
    <?php echo $form->create('NewsMessage', array('type' => 'file')); ?>
    <?php
    echo $form->input('subject', array('class' => 'INPUT'));
    //echo $form->input('html', array('class' => 'INPUT'));
//			$fck = new FckHelper;
    $template_html = empty($template_html) ? $this->data['NewsMessage']['html'] : $template_html;
    echo $fck->create('NewsMessage', 'html', $template_html, 'Default', false, array('height' => 500));
    ?>
    <div class="qualification_set block_set clone-block" style="display:none;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
            <tr>
                <td valign="top">
                    <?php
                    echo $form->input('NewsMessageFile.0.file', array('class' => 'INPUT', 'type' => 'file'));
                    ?>
                </td>

            </tr>
        </table>
        <div class='DeleteSection'><a href="#">Delete</a></div>
        <div class="clear"></div>
    </div>

    <div  id="relatedAttachments" class="items-block">
        <h4>Attachments:</h4>

        <?php if (!empty($this->data['NewsMessageFile'])): ?>
            <?php
            foreach ($this->data['NewsMessageFile'] as $i => $NewsMessageFile):
                // debug($NewsMessageFile);
                $additional_class = '';
                if ($i > 0) {
                    $additional_class = 'clone-block';
                }
                ?>
                <div class="qualification_set block_set clone-block">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                        <tr>
                            <td valign="top">
                                <?php
                                echo $form->input("NewsMessageFile.$i.id", array('class' => 'INPUT', 'value' => $NewsMessageFile['NewsMessageFile']['id']));
                                if (!empty($NewsMessageFile['NewsMessageFile']['file_full_path'])) {
                                    echo '<a href="' . $NewsMessageFile['NewsMessageFile']['file_full_path'] . '" class="Download" >Download</a> <div class="clear"></div><br />';
                                }
                                echo $form->input("NewsMessageFile.$i.file", array('class' => 'INPUT', 'type' => 'file'));
                                ?>
                            </td>	
                        </tr>
                    </table>
                    <div class='DeleteSection delete-ico'><a href="#"><img src="<?php echo Router::url('/css/admin/delete-ico.png')?>" alt="" /> </a></div>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="qualification_set block_set clone-block">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                    <tr>
                        <td valign="top">
                            <?php
                            echo $form->input('NewsMessageFile.0.file', array('class' => 'INPUT', 'type' => 'file'));
                            ?>
                        </td>

                    </tr>
                </table>
                    <div class='DeleteSection delete-ico'><a href="#"><img src="<?php echo Router::url('/css/admin/delete-ico.png')?>" alt="" /> </a></div>
                <div class="clear"></div>
            </div>
        <?php endif; ?>

    </div>
    <a href="javascript:appendEmploymentUploader();" class="AddAttachemnt AddItem">Add another Attachment</a>
    <?php echo $form->submit(__('Submit', true), array('class' => 'Submit')) ?>
    <?php echo $form->end(); ?>


</div>
<script type="text/javascript">
     var i='<?php count($this->data['NewsMessageFile'])==0?print(1):print(count($this->data['NewsMessageFile']));?>';
    $(function(){
        $('.DeleteSection').live('click', function() {
            if(confirm('Are you sure?')){
                container_id = $(this).parents('.items-block').attr('id');
				
                if($('#'+container_id).find('.block_set').length <= 1){
                    $(this).parent('.block_set').find('input').val('');
                }else{
                    $(this).parent('.block_set').remove();
                    i--;
                }
            }
            return false;
        });

    });
    
   
    function appendEmploymentUploader()
    {
        if(i<5){
            if($('#relatedAttachments').is(':visible')){
                txt='<div class="qualification_set block_set clone-block">'+$('.qualification_set:first').html().replace(/\[0\]/g,'['+$('.qualification_set').length+']')+'</div>';                
                $('#relatedAttachments').append(txt);
                $('#relatedAttachments .qualification_set:last input').val('');
            }
            else{
                $('#relatedAttachments').show();
            }
            i++;
        }else{
            alert('can\'t add more than five attachments'); 
        }
    }
    
</script>
