<?php

$subject=trim(htmlspecialchars($message['NewsMessage']['subject']), ' ');
$subject=str_replace(' ', '_', $subject);
$filename =  $subject . '.html';

if (strstr($_SERVER['HTTP_USER_AGENT'],"MSIE"))
{
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
    header('Content-Type:text/html');
    header("Content-Disposition:attachment;filename=$filename");
    header("Content-Transfer-Encoding:binary");
}
else
{
    header('Content-Type:text/html');
    header("Content-Disposition:attchment;filename=$filename");
}

echo $this->element('email_template' , array('content'=>$message['NewsMessage']['html']))
?>
