<div class="news index">
<h2><?php __('Courses');?></h2>

<?php
$fields=array(
	'Course.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Course.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Course.permalink' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Course.display_order' => array( ),
    'Course.hide_details_button' => array('format'=>'bool'),
    'Course.active' => array('format'=>'bool')
);
$links = array(
	$html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
);

$multi_select_actions=array(
    'delete'=>array('action'=>Router::url(array('action'=>'delete','admin'=>true)),'confirm'=>true)
);
echo $list->filter_form($modelName, $filters);
echo $list->adminIndexList($fields, $courses, $links,true,$multi_select_actions) ?></div>
