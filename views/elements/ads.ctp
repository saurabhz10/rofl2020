<div class="home-gallery">
    <div class="left-side left">
        <div class="gallery-top has-sliding">

            <!-- / sliding-box -->
            <a href="<?= $ads['top_left']['url']; ?>" title="" ><img class="img-x" src="<?= $ads['top_left']['image_full_path']; ?>" alt="<?= $ads['top_left']['title']; ?>" title="<?= $ads['top_left']['title']; ?>" /></a>
            <div class="sliding-box-wrap">
                <div id="sliding-1" class="sliding-box">
                    <p><strong><?= $ads['top_left']['title']; ?></strong></p>
                    <p><?= strLimited($ads['top_left']['description'], 50); ?> <a href="<?= $ads['top_left']['url']; ?>" class="color-a"> [ See More ] </a></p>
                </div>
            </div>

        </div>
        <div class="gallery-mid">
            <a href="<?= $ads['small_left']['url']; ?>" title="" class="left"><img class="img-x" src="<?= $ads['small_left']['image_full_path']; ?>" alt="<?= $ads['small_left']['title']; ?>" title="<?= $ads['small_left']['title']; ?>" /></a>
            <a href="<?= $ads['small_right']['url']; ?>" title="" class="right"><img class="img-x" src="<?= $ads['small_right']['image_full_path']; ?>" alt="<?= $ads['small_right']['title']; ?>"  title="<?= $ads['small_right']['title']; ?>"/></a>
            <div class="clear"></div>
        </div>
        <div class="gallery-bottom has-sliding">
            <div class="sliding-box-wrap">
                <div id="sliding-1" class="sliding-box">
                    <p><strong><?= $ads['bottom_left']['title']; ?></strong></p>
                    <p><?= strLimited($ads['bottom_left']['description'], 50); ?><a href="<?= $ads['bottom_left']['url']; ?>" class="color-a"> [ See More ] </a></p>
                </div>
            </div>
            <!-- / sliding-box -->
            <a href="<?= $ads['bottom_left']['url']; ?>" title=""><img class="img-x" src="<?= $ads['bottom_left']['image_full_path']; ?>" alt="<?= $ads['bottom_left']['title']; ?>" title="<?= $ads['bottom_left']['title']; ?>"/></a></div>
    </div>
    <div class="righ-side right">
        <div class="right-top has-sliding left">
            <div class="sliding-box-wrap">
                <div id="sliding-1" class="sliding-box">
                    <p><strong><?= $ads['top_middle']['title']; ?></strong></p>
                    <p><?= strLimited($ads['top_middle']['description'], 50); ?>  <a href="<?= $ads['top_middle']['url']; ?>" class="color-a"> [ See More ] </a></p>
                </div>
            </div>
            <!-- / sliding-box -->            
            <a href="<?= $ads['top_middle']['url']; ?>"><img class="img-x" src="<?= $ads['top_middle']['image_full_path']; ?>" alt="<?= $ads['top_middle']['title']; ?>" title="<?= $ads['top_middle']['title']; ?>" /></a>
        </div>

        <div class="right-top has-sliding right">
            <div class="sliding-box-wrap">
                <div id="sliding-1" class="sliding-box">
                    <p><strong><?= $ads['top_right']['title']; ?></strong></p>
                    <p><?= strLimited($ads['top_right']['description'], 50); ?>  <a href="<?= $ads['top_right']['url']; ?>" class="color-a"> [ See More ] </a></p>
                </div>
            </div>
            <!-- / sliding-box -->            
            <a href="<?= $ads['top_right']['url']; ?>"><img class="img-x" src="<?= $ads['top_right']['image_full_path']; ?>" alt="<?= $ads['top_right']['title']; ?>" title="<?= $ads['top_right']['title']; ?>"/></a>
        </div>

        <div class="clear"></div>

        <div class="right-bottom has-sliding">
            <div class="sliding-box-wrap">
                <div id="sliding-1" class="sliding-box">
                    <p><strong><?= $ads['bottom_right']['title']; ?></strong></p>
                    <p><?= strLimited($ads['bottom_right']['description'], 50); ?>  <a href="#" class="color-a"> [ See More ] </a></p>
                </div>
            </div>
            <!-- / sliding-box -->
            <a href="<?= $ads['bottom_right']['url']; ?>"><img class="img-x" src="<?= $ads['bottom_right']['image_full_path']; ?>" alt="<?= $ads['bottom_right']['title']; ?>" title="<?= $ads['bottom_right']['title']; ?>"/></a></div>

    </div>
    <div class="clear"></div>
</div>


<script type="text/javascript">
	$(function() {
		$(".fade").css("opacity", "0.4");
		$(".fade").hover(function() {
			$(this).stop().animate({
				opacity: 1
			}, "slow");
		}, function() {
			$(this).stop().animate({
				opacity: 0.5
			}, "slow");
		});

		$('.sliding-box').hide();
		$('.has-sliding').mouseenter(function() {

			if ($(this).data('timeHandler'))
			{
				clearTimeout($(this).data('timeHandler'));

			}
			if (!$(this).data('isMouseOver')) {
				$('.sliding-box', this).slideDown();
			}

			$(this).data('isMouseOver', true);


		}).mouseleave(function() {
			var this_product = this;

			$(this).data('timeHandler', setTimeout(function() {
				$('.sliding-box', this_product).slideUp();

				$(this_product).data('isMouseOver', false);
			}, 750));


		});
	});

</script>

<!--- home gallery -->