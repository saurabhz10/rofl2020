<?php if (!empty($topmenus)): ?>
            <ul  id="<?php echo (isset($menuid) ? $menuid: 'menu') ?>">
                <?php
                foreach ($topmenus as $topmenu):
                
                    $title = $topmenu['Page']['title'];
                    $url = (!empty($topmenu['Page']['is_url']) && !empty($topmenu['Page']['url'])) ?  ( isset($topmenu['Page']['routed']) ? $topmenu['Page']['url'] :  Router::url($topmenu['Page']['url']) ): (Router::url("/content/{$topmenu['Page']['permalink']}"));
                    $class = (!empty($topmenu['Page']['Submenu'])) ? "sub-item" : "";
                    $class2 = (!empty($topmenu['Page']['Submenu'])) ? "has-sub" : "";
                    $target = "";
                    if ($url == $_SERVER['REQUEST_URI']) {
                        $class.=" active";
                    }
                    ?>
                    <li class="<?= $class2 ?>"><a target="<?= $target ?>" class="<?= $class ?>" href="<?= $url ?>"><span class="helvetica">
                                <?= $title ?>
                            </span></a>
                        <?php if (!empty($topmenu['Page']['Submenu'])) { ?>
                            <ul style="display:none ;" class="sub-items">
                                <?php
                                foreach ($topmenu['Page']['Submenu'] as $i => $submenu):

                                    $title = $submenu['Page']['title'];
                                    $url = (!empty($submenu['Page']['is_url']) && !empty($submenu['Page']['url'])) ?  ( isset($submenu['Page']['routed']) ? $submenu['Page']['url']: Router::url($submenu['Page']['url']) ) : (Router::url("/content/{$submenu['Page']['permalink']}"));
                                    $class = !empty($topmenu['Page']['Submenu'][($i + 1)]) ? "" : "last";
                                    $class2 = (!empty($submenu['Page']['SubSubmenu'])) ? "has-sub" : "";
                                    $target = "";
                                    ?>
                                    <li class="<?= $class ?> <?= $class2 ?>"><a href="<?= $url ?>"><span>
                                                <?= $title ?>
                                            </span></a>
                                        <?php if (!empty($submenu['Page']['SubSubmenu'])) { ?>
                                            <ul style="display:none ;" class="sub-items">
                                                <?php
                                                foreach ($submenu['Page']['SubSubmenu'] as $j => $submenu2):
                                                    $title = $submenu2['Page']['title'];
                                                    $url = (!empty($submenu2['Page']['is_url']) && !empty($submenu2['Page']['url'])) ?  ( isset($submenu2['Page']['routed']) ? $submenu2['Page']['url'] :  Router::url($submenu2['Page']['url']) ): (Router::url("/content/{$submenu2['Page']['permalink']}"));
                                                    $class_last = !empty($submenu['Page']['SubSubmenu'][($j + 1)]) ? "" : "last";
                                                    $target = "";
                                                    ?>
                                                    <li class="<?= $class_last ?>"><a href="<?= $url ?>"><span>
                                                                <?= $title ?>
                                                            </span></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php endforeach; ?>
            </ul>
<?php endif; ?>
            

	    <script type="text/javascript">
		$(document).ready(function(){
			
            $('ul.sub-items').each(function() {
                $length = $(this).find('li').length;
                if ($length > 8) {
                    $(this).addClass('large-menu');
                }
            })
			
                   $('.has-sub').hover(function(){
                    $(this).find('> a').addClass('active') ; 
                    $(this).find('ul:first').show();
                    
                   },function(){
                       $(this).find('ul:first').hide();   
                       $(this).find('> a').removeClass('active') ; 
                   }); 
                });
	    </script>


 