<style type="text/css">
img.play { opacity: 0.8;} 
img.play:hover{opacity: 0.9 ; }
</style>

<?php
$width=empty ($width)?430:$width;
$height=empty ($height)?286:$height;
$autoPlayer=empty ($autoPlayer)?false:true;
$thumb_image=empty ($video['Video']['image'])?'':  get_resized_image_url($video['Video']['image'], $width, $height);
$style="display:block;width:{$width}px;height:{$height}px;";
$play_btn='';
$play_img = Router::url('/js/flowplayer/play_icon.png');

if(! ($autoPlayer) && !empty ($thumb_image)) {
    
   
	$style.="background-image: url('$thumb_image');text-align: center;";
	$margin_top= ($height/2)-20;
	$play_btn="<img src='".$play_img."' class='play' style='cursor: pointer;margin-top: {$margin_top}px;'/>";
}
echo $javascript->link(array('flowplayer/flowplayer-3.2.6.min')); ?>
<?php
$rand_number=time()."_".rand(1, 1000);
?>
<a href="<?=$file?>" style="<?=$style?>" class="PlayerVideo content-video">
	<?=$play_btn?>
</a>

<script type="text/javascript">
	$('document').ready(function(){
		flowplayer("a.PlayerVideo",{wmode: "transparent",src: "<?=Router::url('/js/flowplayer/flowplayer-3.2.0.swf', true)?>"});
	});
</script>