<h3>Product Details</h3>
<table border="0" cellspacing="0" width="100%" >
    <tr>
        <td><font face="Arial" style="font-size:12px;">Product Name</font></td>
        <td><font face="Arial" style="font-size:12px;">Quantity</font></td>
        <td><font face="Arial" style="font-size:12px;">Price/Unit</font></td>
        <td><font face="Arial" style="font-size:12px;">Subtotal</font></td>
    </tr>
    <?php $total = 0;?>
    <?php foreach($products as $product): ?>
    <tr>
            <?php
            $name=$product['Product']['name'];
            ?>
        <td><font face="Arial" style="font-size:12px;"><?=$name?></font></td>
        <td><font face="Arial" style="font-size:12px;"><?= $product['qty'] ?></font></td>
        <td><font face="Arial" style="font-size:12px;"><?= format_price($product['price']) ?></font></td>
        <td><font face="Arial" style="font-size:12px;"><?= format_price($product['subtotal']) ?></font></td><?php $total+= $product['subtotal']; ?>
    </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2">&nbsp;</td>
        <td align="right"><font face="Arial" style="font-size:12px;"><strong>Total</strong></font></td>
        <td><font face="Arial" style="font-size:12px;"><?= format_price($total) ?></font></td>
    </tr>
</table>