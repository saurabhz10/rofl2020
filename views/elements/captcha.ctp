<?php
$rand_code = rand();
$label = isset ($capatchalabel) ? $capatchalabel :  __('Security Code', true) ;
$image = "<span id='CaptchaImage' >".$html->image('/captcha.jpg?code=' . $rand_code, array('id' => 'SecurImage'))."</span>";
$captcha = $form->input('security_code', array('id'=>'security_code','label' => $label, 'class' => 'required', 'value'=>'','div'=>false,'between'=>$image));
$captcha .= "<div class=\"clear\"></div><p>Type the characters you see in this picture</p>";
echo $html->div('', $captcha, array('id' => 'Captcha','class'=>'input text captcha'));
?>
