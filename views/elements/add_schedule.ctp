<?php
echo $form->create('NewsMessage', array('action' => 'add_schedule'));
echo $form->input('news_message_id', array('label' => 'Newsletter Message', 'class' => 'INPUT required', 'options' => $news_messages));
echo $form->input('day', array('div' => 'input text', 'class' => 'INPUT', 'label' => 'Date : ', 'class' => 'DayPicker'));
?>
<div class="input text">
    <?php
    echo "<label>Time</label>";
    echo $form->select('hour', range(0, 23), null, array('class' => 'INPUT'), false) . ' : ' . $form->select('minute', range(0, 59), null, array('class' => 'INPUT'), false);
    ?>
</div>
<?php echo $form->submit(__('Send', true), array('class' => 'Submit')) ?>
<?php echo $form->end(); ?>

<?php
echo $javascript->link('chosen.jquery.min');
echo $html->css('chosen');
?>
<script type="text/javascript"> $(".chzn-select").chosen();</script>
<?php echo $javascript->link('jquery.datepick'); ?>
<?php echo $html->css('jquery.datepick'); ?>
<script type="text/javascript">
    $('.SendgroupFrom').hide();
    $('.Sendgroup').click(function() {
        $('.SendgroupFrom').toggle();
        return false;
    });
    $('.DayPicker').datepick({dateFormat: 'dd-m-yy', minDate: new Date()});
</script>