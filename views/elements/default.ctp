<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?= $html->charset(); ?>
        <title>
            <?= $title_for_layout; ?>
        </title>
        <meta name="keywords" content="<?= $metaKeywords ?>" />
        <meta name="description" content="<?= $metaDescription ?>" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php
        echo $html->css('screen');
        echo $html->css('popup');
        echo $javascript->link('jquery-1.7.2.min');
        echo $javascript->link('jquery-ui-1.8.22.custom.min');
        echo $javascript->link('in-label');
        echo $javascript->link('script');
        echo $scripts_for_layout;
        ?>


        <script type="text/javascript">
            <?= $config['txt.google_analytics'] ?>      
        </script>


    </head>

    <body>

        <div class="layout wrap">
            <div class="sidebar left">
                <div class="side-section">
                    <div class="logo"><a href="#" title="Premier Tint" ><img src="<?= Router::url('/css/img/premiertint.png'); ?>" alt="Premier Tint" /></a></div>
                </div>
                <!-- / section -->
                <div class="side-section">
                    <div class="nav">
                        <ul>
                            <li><a href="#">sub-menu</a>
                                <ul>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                    <li><a href="#">Lorem Ipsum</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / section -->

                <div class="side-section">
                    <div class="side-content">
                        <h3>Search Now</h3>
                        <div class="search corner-all quick-form">
                            <form action="" method="">
                                <input type="text"  value="" name="" id="" class="search-input"  placeholder="Search here"/>
                                <button type="submit"><span class="btn-a corner-all">GO!</span></button>
                            </form>
                        </div>
                        <!--quick-form -->
                    </div>

                    <div class="side-content social">
                        <h3>Get Social</h3>
                        <a href="#" title=""><img src="<?= Router::url('/css/img/icons/fb.gif'); ?>" alt="" /></a>
                        <a href="#" title=""><img src="<?= Router::url('/css/img/icons/rss.gif'); ?>" alt="" /></a>
                        <a href="#" title=""><img src="<?= Router::url('/css/img/icons/tw.gif'); ?>" alt="" /></a>
                    </div>

                    <div class="side-content contacts">
                        <h3>Contact Us</h3>
                        <dl>
                            <dt>P |</dt>
                            <dd>1300 323 454</dd>
                            <dt>E |</dt>
                            <dd>info@premiertint.com.au</dd>
                            <dt>W |</dt>
                            <dd>www.premiertint.com.au</dd>
                            <dt>P |</dt>
                            <dd>96 Penhurst Street<br />Chatswood NSW 2060</dd>
                        </dl>
                    </div>
                </div>

                <div class="side-section">
                    <div class="side-content">
                        <h3>Join Newsletter</h3>
                        <div class="newsletters corner-all quick-form">

                            <?= $form->create('Newsletter', array('id' => 'NewsletterForm', 'action' => 'add')) ?>
                            <div class="newsletters-form">
                                <?= $form->input('email', array('label' => false, 'div' => false, 'class' => 'mail-input', 'placeholder' => 'Email Here')) ?>
                                <button type="submit"><span class="btn-a corner-all">Join</span></button>                             </div>
                            <?= $form->end() ?>



                        </div>

                        <div id="subscription-form-message"> </div>

                        <?php // <p>Ut eget metus nibh, nec scelerisque sem. Nulla dui purus, pellentesque sit amet rutrum vitae.</p> ?>
                        <!--quick-form -->
                    </div>
                </div>
                <!-- / section -->

            </div>
            <!-- / side bar -->
            <div class="main left">
                <div class="main-nav">
                    
                    
                    
                    <!--            <ul>
                                    <li><?= $html->link('Home', '/'); ?></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Products &amp; Services</a></li>
                                    <li><a href="#">Brochures &amp; Resources</a></li>
                                    <li><a href="#">News</a></li>
                                    <li><?= $html->link('Contact Us', '/contact'); ?></li>
                                </ul>-->
                    
                                        <?php echo $this->element('topmenu', array('topmenus' => $topmenus)); ?>

                    <div class="clear"></div>





                </div>
                <!-- / main-nav -->
                <div class="content">

                    <?php $session->flash(); ?>
                    <?= $content_for_layout; ?>




                </div>
            </div>
            <!-- / main -->
            <div class="clear"></div>
        </div>
        <div class="footer wrap">
            <p>&copy; <?php echo date('Y') ?> Premiertint NSW<span>|</span><a href="#">Terms &amp; Conditions</a></p>
        </div>





        <script type="text/javascript" >
                           
							
            $('#NewsletterForm').submit(function(){
                                
                                
                var post_data= $(this).serialize();
                var url= $(this).attr('action');
                $('#subscription-form-message').html('');
                $('#subscription-form-message').addClass('form-loader');
                $('#subscription-form-message').html('Please wait');
                $.ajax({
                    cache:true,
                    url:url,
                    type:"POST",
                    dataType:'jsonp',
                    data:post_data,
                    success:function(data){
                                        
                                       
					
                        $('#subscription-form-message').removeClass('form-loader');
                        $('#subscription-form-message').removeClass('form-succ');
                        $('#subscription-form-message').removeClass('validate-email-error');
                        if(data['success']){
                            $('#subscription-form-message').html(data['message']);
                            $('#subscription-form-message').addClass('form-succ');
                            $('#NewsletterEmail').val('');
                        }else{
											
                            $('#subscription-form-message').html(data['error']['email']);
                            $('#subscription-form-message').addClass('validate-email-error');
                        }
                    }
                });
                return false;
            });
				
        </script>


    </body>
</html>