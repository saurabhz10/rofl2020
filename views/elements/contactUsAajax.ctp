<div class="contact-form">
    <?php
    $session->flash();

    echo $form->create('Contact', array('action' => 'ajax_contact', 'id' => 'contact'));
    echo $form->input('subject', array("type" => "hidden", "value" => !empty($galleryimages[0]['GalleryImage']['title']) ? nl2br($galleryimages[0]['GalleryImage']['title']) : $courses['Course']['title']));
    echo $form->input('contact_type', array("type" => "hidden", "value" => !empty($galleryimages[0]['GalleryImage']['title']) ? "gallery" : "course"));
    ?>
    <div class="input text left">
        <label for="ContactFirstName">First Name<span class="star">*</span></label>
        <?php echo $form->input('first_name', array('class' => 'required', 'div' => false, 'label' => false)); ?>
    </div>
    <div class="input text left">
        <label for="ContactLastName">Last Name <span class="star">*</span></label>
        <?php echo $form->input('last_name', array('class' => 'required ', 'div' => false, 'label' => false)); ?>
    </div>
    <div class="input text left">
        <label for="ContactLEmail">Email<span class="star">*</span></label>
        <?php echo $form->input('email', array('class' => 'required ', 'div' => false, 'label' => false)); ?>
    </div>

    <div class="clear"></div>
    <div class="input textarea">
        <label for="ContactMessage">Message</label>
        <?php echo $form->input('message', array('class' => 'required ', 'div' => false, 'label' => false, "type" => "textarea")); ?>
    </div>
    <div class="clear"></div>
    <div class="input text captcha">
        <?php
        echo $form->label('ContactCaptchaCode', 'Security Code');
        ?><img alt="" id="SecurImage" src="<?= Router::url('/image.jpg/'); ?>"/><?
        echo $form->input('security_code', array('class' => 'required', 'label' => '', 'div' => false));
        if (!empty($error_captcha)):
            ?>
            <div class='error-message'><?= $error_captcha ?></div>
        <?php endif; ?>
    </div>
    <div class="input submit submit-center">
        <button type="submit"><span class="btn-a corner-all">SUBMIT</span></button>
        <div id="enquiry-form-message"> </div>            </div>
        <?php echo $form->end(); ?>
</div>
