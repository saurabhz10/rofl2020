<div class="ViewOrder">
    <div class="Order_PaymentDetails">
        <h3>Customer Details</h3>
        <table border="0" cellpadding="0" cellspacing="0" class="Admin_details" width="100%">
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>First Name</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['first_name'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Last Name</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['last_name'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Address</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['address1'] . ' ' . $order['Order']['address2'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Suburb</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['city'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>State</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['state'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Postcode</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['postcode'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Phone</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['telephone'] ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Email</strong></font></td>
                    <td><font face="Arial" style="font-size:12px;"><?= $order['Order']['email'] ?></font></td>
                </tr>
        </table>
    </div>
    <div class="Order_PaymentDetails">
        <?php
        echo $this->element('standard_products', array('products' => $order['OrdersProduct']));
        ?>
    </div>
    <div class="Order_PaymentDetails">
        <h3>Payment Details</h3>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="20%"><font face="Arial" style="font-size:12px;"><strong>GST (10%)</strong></font></td>
                <td><font face="Arial" style="font-size:12px;"><?= format_price($order['Order']['gst'])?></font></td>
            </tr>
            
            <tr>
            <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Total</strong></font></td>
            <td><font face="Arial" style="font-size:12px;"><?= format_price($order['Order']['total']) ?></font></td>
            </tr>

            <tr>
                <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Payment Method</strong></font></td>
                <td><font face="Arial" style="font-size:12px;"><?= Inflector::humanize($order['Order']['pay_method']) ?></font></td>
            </tr>
            <tr>
                <td width="20%"><font face="Arial" style="font-size:12px;"><strong>Sent On</strong></font></td>
                <td><font face="Arial" style="font-size:12px;"><?= date('d/m/Y', strtotime($order['Order']['created'])) ?></font></td>
            </tr>

        </table>
    </div>
</div>