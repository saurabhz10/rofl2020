<font face="Arial" style="font-size:18px;" >Contact Us message </font><br /><br />
<ul style="font-family:Arial, Helvetica, sans-serif; font-size:12px; ">    
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', 'First name') . ': ' . $contact_data['first_name']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', 'Last name') . ': ' . $contact_data['last_name']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', 'Email Address') . ': ' . $contact_data['email']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', 'Message') . ': ' . nl2br($contact_data['message'])); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', 'Date on') . ': ' . date('d/m/Y')); ?>
    </font>

</ul>
