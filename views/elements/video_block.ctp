<div class="items-slider">
    <ul class="list-items org-list">
        <?php foreach ($videos as $video): ?>
            <li>
                <div class="video-player">
                    <?php
                    $image = Router::url('/css/img/video-test.png');
                    if ($video['Video']['image']) {
                        $image = get_resized_image_url($video['Video']['image'], 210, 0);
                    }
                    
                    if($video['Video']['type']==0)
                    {
                        $width=550;
                    }else{
                        $info=getSwfSize($video['Video']['flash']);
                       $width=$info[0];
                    }
                   
                    ?>
<a  class="VideoPopup thumb_video" id="width-<?php echo $width?>" href="<?= Router::url(array('controller' => 'videos', 'action' => 'view_video', $video['Video']['id'], Inflector::slug($video['Video']['title']))) ?>"><img src="<?= $image ?>" /></a>
                </div>
                <a  class="video_player VideoPopup" id="width-<?php echo $width?>" href="<?= Router::url(array('controller' => 'videos', 'action' => 'view_video', $video['Video']['id'], Inflector::slug($video['Video']['title']))) ?>"><strong><?= $video['Video']['title'] ?></strong><?= $mixed->strLimit($video['Video']['description'], 50) ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="slider-dots">
    <?php $paginator->options(array('url' => array('controller' => 'videos', 'action' => 'index'))); ?>
    <?php echo $this->element('paginator', array('model' => 'Video')) ?>
</div>