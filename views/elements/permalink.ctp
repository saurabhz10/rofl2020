<script type="text/javascript">
		//<[CDATA[
		<?php
		if (empty($model)){ $model = 'Page'; }
		if (empty($field)){ $field = 'permalink'; }
		if (empty($srcField)){ $srcField = 'title'; }
		if (empty($titleID)) { $titleID = $model . Inflector::humanize($srcField); }
		if (empty($permalinkID)) { $permalinkID = $model . Inflector::humanize($field); }
		?>
		var title = document.getElementById('<?= $titleID ?>');
		var permalink = document.getElementById('<?= $permalinkID ?>');
		var titleChanged = function(){
			var t = title.value.split(/\s+|\W+/);
			permalink.value = t.join('-').replace(/\-{2,}/, '-').replace(/^\-|\-$/, '').toLowerCase();
		}
		title.onkeyup = titleChanged;

		<?php
		if (empty($this->data[$model][$field])) {
			echo "\$(titleChanged).change();\n";
		}
		?>
		title.onchange = titleChanged;
		//]]>
</script>