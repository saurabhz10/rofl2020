<h1><?=__('SEO Rules',true)?></h1>
<?php
echo $list->filter_form($modelName, $filters);
$fields = array(
	'Seo.criteria' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Seo.title' => array('edit_link', array('action' => 'edit', '%id%'))
);
$actions = array(
	$html->link('Edit', array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')), //, __('Are you sure?', true)),
);
$multi_select_actions = array(
	'delete' => array('action' => Router::url(array('action' => 'delete', 'admin' => true)), 'confirm' => true)
);
echo $list->adminIndexList($fields, $seos, $actions, true, $multi_select_actions);
?>
