<div class="FormExtended">
	<?php
	$url = ($this->action == 'add') ? "/admin/seo/add" : '/admin/seo/edit/' . $this->data['Seo']['id'];
	echo $form->create('Seo', array('url' => $url));
	echo $form->input('criteria', array('class' => 'INPUT required',
		'between' => $html->para('Hints', 'Please enter the page URL starting with the forward slash \' / \' after your domain name.<br>For example: http://www.domain-name.com/contacts (for this page you would enter: /contacts).')));
	echo $form->input('title', array('class' => 'INPUT required',
		'between' => $html->para('Hints', 'Page Title.')));
	echo $form->input('keywords', array('class' => 'INPUT required',
		'between' => $html->para('Hints', 'Page keywords meta tag.')));
	echo $form->input('description', array('class' => 'INPUT required',
		'between' => $html->para('Hints', 'Page description meta tag.')));

	echo $form->end(array('label' => 'Submit', 'class' => 'Submit'));
	?>
</div>

