<div class="logos index">
<h1><?php __('Logos');?></h1>
<?php
echo $list->filter_form($modelName, $filters);
$fields=array(
	'Logo.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Logo.title' => array('edit_link' => array('action' => 'edit', '%id%')),
	'Logo.display_order' => array()
);
$links = array(
	$html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')),//, __('Are you sure?', true)),
);

$multi_select_actions=array('delete'=>array('action'=>Router::url(array('action'=>'delete')),'confirm'=>true));

echo $list->adminIndexList($fields, $logos, $links,true,$multi_select_actions);
?>
</div>