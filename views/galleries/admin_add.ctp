<div class="FormExtended">
	<?php 
	echo $form->create('Gallery',array('type' => 'file'));

	echo $form->input('id', array('class' => 'INPUT required'));
	echo $form->input('title', array('class' => 'INPUT'));
	echo $form->input('description', array('class' => 'INPUT'));
	echo $form->input('active', array('class' => 'INPUT'));
	echo $form->input('display_order', array('class' => 'INPUT number'));
	echo $form->input('keywords', array('class' => 'INPUT'));
	echo $form->input('category_id', array('options'=>$categories,'empty'=>'{ Please select category }','class' => 'INPUT'));
	?>
	<fieldset  id="RelatedPages">
        <legend>Gallery Images</legend>
<?php
            if ($this->action == "admin_edit") {
                $fields = array(
                    'GalleryImage.id' => array('edit_link' => array('controller' => 'gallery_images', 'action' => 'edit', '%id%')));
                $links = array(
                    $html->link(__('Edit', true), array('controller' => 'gallery_images', 'action' => 'edit', '%id%'), array('class' => 'Edit')),
                    $html->link(__('Delete', true), array('controller' => 'gallery_images', 'action' => 'delete', '%id%'), array('class' => 'Delete')), //, __('Are you sure?', true)),
                );
            }

            $counter = 0;
            $delete_image_url = Router::url('/img/admin2/close.gif');
            if (!empty($galleryimage)) {
                foreach ($galleryimage as $i => $image) {
                    ?>
                <div class="page_set">
                    <?php
                    echo $form->input('GalleryImage.' . $i . '.id', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['id']));                   
                    echo $form->input('GalleryImage.' . $i . '.title', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['title']));                   
                    echo $form->input('GalleryImage.' . $i . '.description', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['description']));                   
                    echo $form->input('GalleryImage.' . $i . '.price', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['price']));                   
                    echo $form->input('GalleryImage.' . $i . '.image', array('label' => 'Image', 'class' => 'INPUT required', 'type' => 'file', 'between' =>
                    $this->element('image_input_between', array('controller'=>'gallery_images','info' => $image_settings['image'], 'field' => 'image', 'id' => (is_array($image) ? $image['GalleryImage']['id'] : null), 'base_name' => (is_array($image) ? $image['GalleryImage']['image'] : '')))
                    ));
                    ?><div class="clear"></div><?php
                    echo $form->input('GalleryImage.' . $i . '.display_order', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['display_order']));

                     $counter++;
                    ?>
                    <div class='DeleteSection'><a href="#"><img alt="Delete" src="<?= $delete_image_url ?>"/></a></div>
                    <div class="clear"></div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="page_set">
                <?php
                    
	                echo $form->input('GalleryImage.0.title', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['title']));                   
                    echo $form->input('GalleryImage.0.description', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['description']));                   
                    echo $form->input('GalleryImage.0.price', array('class' => 'INPUT ', 'value' => $image['GalleryImage']['price']));                   
                    echo $form->input('GalleryImage.0.image', array('label' => 'Image', 'class' => 'INPUT required', 'type' => 'file'));
                
                    ?><div class="clear"></div><?php
            echo $form->input('GalleryImage.0.display_order', array('class' => 'INPUT'));
                ?>
                <div class='DeleteSection'><img alt="Delete" src="<?= $delete_image_url ?>"/></div>
                <div class="clear"></div>
            </div>
        <?php } ?>
    </fieldset>
    <a href="javascript:appendPage();" class="AddItem">Add another image</a> 
<?php 	echo $form->submit(__('Submit', true), array('class' => 'Submit'));
	echo $form->end();

	?>
</div>



   
   <script type="text/javascript">
   x = '<?= $javascript->escapeString($form->input('GalleryImage.0.id', array('class' => 'INPUT', 'value' => ''))) ?>';

   x += '<?= $javascript->escapeString($form->input('GalleryImage.0.title', array('class' => 'INPUT', 'value' => ''))) ?>';

   x += '<?= $javascript->escapeString($form->input('GalleryImage.0.description', array('class' => 'INPUT', 'value' => ''))) ?>';

    x += '<?=
    $javascript->escapeString($form->input('GalleryImage.0.image', array('label' => 'Image', 'value' => '', 'class' => 'INPUT required', 'type' => 'file'
            )))
    ?>';
        x += '<?= $javascript->escapeString('<div class="clear"></div>') ?>';
        x += '<?= $javascript->escapeString($form->input('GalleryImage.0.display_order', array('class' => 'INPUT', 'value' => ''))) ?>';
           
        x += '<?= $javascript->escapeString("<div class='DeleteSection'><img alt='Delete' src='$delete_image_url'/></div>") ?>';
        x += '<?= $javascript->escapeString('<div class="clear"></div>') ?>';
        var  sections_counter = '<?= $counter ?>';
        sections_counter++ ;
        function appendPage()
        {
            if($('#RelatedPages').is(':visible')){
                y = x.replace(/ProductImage0/g,'GalleryImage'+sections_counter);
                y = y.replace(/\[0\]/g,'['+sections_counter+']');
                $('#RelatedPages').append('<div class="page_set">'+y+'</div>');
                /*$('.page_set:last').find('input').val('');
        $('.page_set:last #PrjectImage'+sections_counter+'Active_').val(0);
        $('.page_set:last #PrjectImage'+sections_counter+'Active').val(1);*/
                sections_counter++;

            }else
                $('#RelatedPages').show();
        }
        
        $(function(){
            $('.DeleteSection').live('click', function() {
                if(confirm('Are you sure?')){
                    $(this).parent('.page_set').remove();
                }
                return false;
            });

            $('.DeleteLinkSection').live('click', function() {
                if(confirm('Are you sure?')){
                    $(this).parent('.link_page_set').remove();
                }
                return false;
            });

        });
</script>







