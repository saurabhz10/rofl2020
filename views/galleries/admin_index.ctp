<div class="galleries index">
<h1><?php __('Galleries');?></h1>
<?php
echo $list->filter_form($modelName, $filters);
$fields=array(
	'Gallery.id' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Gallery.title' => array('edit_link' => array('action' => 'edit', '%id%')),
    'Category.title' => array('title'=>'Category','edit_link' => array('action' => 'edit', '%id%')),
	'Gallery.display_order' => array()
);
$links = array(
	$html->link(__('Edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
	$html->link(__('Delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete')),//, __('Are you sure?', true)),
);

$multi_select_actions=array('delete'=>array('action'=>Router::url(array('action'=>'delete')),'confirm'=>true));


echo $list->adminIndexList($fields, $galleries, $links,true,$multi_select_actions);
?>
</div>