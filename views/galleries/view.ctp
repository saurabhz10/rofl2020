<?php
echo $html->css('lightbox');
echo $javascript->link('jquery-1.7.2.min');
echo $javascript->link('jquery-ui-1.8.18.custom.min');
echo $javascript->link('lightbox');
?>
<script>
    var count = '<?php echo count($galleryimages); ?>';
    var descriptions = new Array();
    var prices = new Array();
    var titles = new Array();
    descriptions["productimage_0"] = "<?php echo $javascript->escapeString(($galleryimages[0]['GalleryImage']['description'])); ?>";
    prices["productimage_0"] = "<?php echo nl2br($galleryimages[0]['GalleryImage']['price']); ?>";
    titles["productimage_0"] = "<?php echo nl2br($galleryimages[0]['GalleryImage']['title']); ?>";
    var currentImage = "productimage_0";
<?php for ($i = 1; $i < count($galleryimages); $i++) { ?>

        descriptions["productimage_<?php echo $i; ?>"] = "<?php echo $javascript->escapeString(($galleryimages[$i]['GalleryImage']['description'])); ?>";
        prices["productimage_<?php echo $i; ?>"] = "<?php echo nl2br(format_price($galleryimages[$i]['GalleryImage']['price'])); ?>";
        titles["productimage_<?php echo $i; ?>"] = "<?php echo nl2br($galleryimages[$i]['GalleryImage']['title']); ?>";
    <?php
}
?>

    allow = true;
    function loadImage(index)
    {

        if (index == "-" || index == "+") {
//            console.log(currentImage);
            var n = currentImage.split("_");
//            console.log(n);

            if (index == "-")
                next_index = (parseInt(n[1]) - 1);
            else
                next_index = (parseInt(n[1]) + 1);
            if (count == next_index || next_index < 0)
                index = currentImage;
            else
                index = n[0] + "_" + next_index;
        }
        if (allow && index != currentImage) {
            $('#' + currentImage).fadeOut(100, function() {
                allow = false;
                $('#' + index).fadeIn(500, function() {
                    allow = true;
                });
                currentImage = index;
                // alert("hi"+currentImage);
            });
            $('#product_description').fadeOut(100, function() {
                $('#product_description').html(descriptions[index]);
                $('#product_description').fadeIn(500);
            });
            $('#enquriySubject').val(titles[index]);
            $('#imagePrice').fadeOut(100, function() {
                if (prices[index] != "") {
                    $('#imagePrice').html('<strong>Price: </strong>' + prices[index]);
                } else {
                    $('#imagePrice').html("");
                }
                $('#imagePrice').fadeIn(500);
            });
        }
    }
</script>


<div class="snippets fixText">
    <h1><?php echo $gallery['Gallery']['title']; ?></h1>
    <p><?php echo $gallery['Gallery']['description']; ?></p>
</div>
<?php if (!empty($galleryimages)) { ?>
    <div class="listing product-view gallery-view">
                   <div class="listing-thumb"> 

   <div class="thumb-viewer">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="475">

        <a  id="productimage_0" href="<?php echo $galleryimages[0]['GalleryImage']['image_full_path']; ?>" class="large-img" title="<?php echo $galleryimages[0]['GalleryImage']['title']; ?>" rel="lightbox[staff]"  >
                <!-- <img src="<?php //echo get_resized_image_url($galleryimages[0]['GalleryImage']['image'], 630, 470); ?>" alt="<?php //echo $galleryimages[0]['GalleryImage']['title']; ?>"> -->
                <img class="main_prev_image" src="<?php echo '/img/uploads/'.$galleryimages[0]['GalleryImage']['image']; ?>" alt="<?php echo $galleryimages[0]['GalleryImage']['title']; ?>">
            </a>  
            <?php for ($i = 1; $i < count($galleryimages); $i++) {
                ?>
<a style="display:none;" id="productimage_<?php echo $i; ?>" href="<?php echo $galleryimages[$i]['GalleryImage']['image_full_path']; ?>" class="large-img" title="<?php echo $galleryimages[$i]['GalleryImage']['title']; ?>" rel="lightbox[staff]" >
                    <!-- <img src="<?php // echo get_resized_image_url($galleryimages[$i]['GalleryImage']['image'], 630, 470) ?>" alt="<?php // echo $galleryimages[$i]['GalleryImage']['title']; ?>" /> -->
                    <img class="main_prev_image" src="<?php echo '/img/uploads/'.$galleryimages[$i]['GalleryImage']['image'] ?>" alt="<?php echo $galleryimages[$i]['GalleryImage']['title']; ?>" />
                </a>
                <?php
            }
            ?>
            
            </td>
  </tr>
</table>

        </div> 
</div>
        
        

        <div class="thumb-lsiting">
            <ul>
                <li><a href="javascript:void(0)"  >
                        <img onclick="loadImage('productimage_0')"  src="<?php echo get_resized_image_url($galleryimages[0]['GalleryImage']['image'], 69, 73, true); ?>" alt="<?php echo $galleryimages[0]['GalleryImage']['title']; ?>" title="<?php echo $galleryimages[0]['GalleryImage']['title']; ?>" />
                    </a></li>
                <?php for ($i = 1; $i < count($galleryimages); $i++) { ?>

                    <li>
                        <a href="javascript:void(0)"><img onclick="loadImage('productimage_<?php echo $i; ?>')"  src="<?php echo get_resized_image_url($galleryimages[$i]['GalleryImage']['image'], 69, 73, true); ?>" alt="<?php echo $galleryimages[$i]['GalleryImage']['title']; ?>" title="<?php echo $galleryimages[$i]['GalleryImage']['title']; ?>" />
                        </a></li>
                    <?php
                }
                ?>
            </ul>
        </div> 

        <div class="clear"></div>
    </div>
    <div class="product-nav left"> 

        <a href="javascript:void(0)" class="previous" title="Previous" onclick="loadImage('-')">Previous</a>
        <a href="javascript:void(0)" class="next" title="Next" onclick="loadImage('+')">Next</a>
        <div class="clear"></div>
    </div>
    <div class="pagination">
        <?php if ($paginator->numbers()) { ?>
            <?php echo $paginator->options(array('url' => array($gallery['Gallery']['id']))) ?>
            <div class="right paging">
                <ul>
                    <li class="previous"><?php echo $paginator->prev('<') ?></li>
                    <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                    <li class="next"><?php echo $paginator->next('>'); ?></li>
                </ul>
                <?php //echo str_replace(array(h('next>>'), h('<<prev')), array(h('>'), h('<')), $list->paging());  ?>
            </div>
            <div class="clear"></div>
            <?php
        }
        ?>
        <div class="clear"></div>
        <div class="gallery-description">
            <p id="product_description"><?php 
            
            
            echo nl2br($galleryimages[0]['GalleryImage']['description']); ?></p>
        </div>
        <div class="gallery-actions"> <a href="#" class="btn-a corner-all left"  id="enquire" data-reveal-id="enquire">Enquire</a>
            <p class="left" id="imagePrice">
                <?php if (!empty($galleryimages[0]['GalleryImage']['price'])) {
                    ?>
                    <strong>Price: </strong><?php echo nl2br(format_price($galleryimages[0]['GalleryImage']['price'])); ?>
                <?php }
                ?>
            </p>
            <div class="clear"></div>
        </div>
    </div>


<?php } ?>

<style type="text/css">
	.main_prev_image{
		max-height: 480px;
		max-width: 660px;
	}
    .fixText {
        padding-right: 188px;
    }
    @media only screen and (max-width: 768px){
        /*.fixText {
            padding-right: 0;
        }*/
        .fixText p {
            font-size: 20px;
        }
        .fixText h1 {
            color: #555;
        }
    }
</style>

<script>
	// $(document).ready(function() {
	// 	$("p").each(function() {
	// 		var text = $(this).text();
	// 		var part1 = text.substring(250);
	// 		var part2 = text.substring(200);
	// 		if(part1){
	// 			$(this).parent('div').css('font-size','90%');
	// 		}
	// 		if(!part2){
	// 			$(this).parent('div').css('font-size','133%');	
	// 		}
	// 	});
	// });
</script>