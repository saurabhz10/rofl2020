<div class="news index">
    <h2><?php __('Testimonials'); ?></h2>

    <?php
    $fields = array(
        'Testimonial.id' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Testimonial.title' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Testimonial.permalink' => array('edit_link' => array('action' => 'edit', '%id%')),
        'Testimonial.display_order' => array()
    );
    $linksUrl = array(
        $html->link(__('edit', true), array('action' => 'edit', '%id%'), array('class' => 'Edit')),
        $html->link(__('delete', true), array('action' => 'delete', '%id%'), array('class' => 'Delete'), __('Are you sure?', true)),
    );

    $multi_select_actions = array(
        'delete' => array('action' => Router::url(array('action' => 'delete', 'admin' => true)), 'confirm' => true)
    );
    echo $list->filter_form($modelName, $filters);
    echo $list->adminIndexList($fields, $testimonials, $linksUrl, true, $multi_select_actions);
    ?> 
</div>
