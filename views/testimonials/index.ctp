<h1>Testimonials</h1>

<div class="snippets-box">
    <?php echo $snippet; ?>
</div>

<div class="news-listing">
    <?php
    foreach ($testimonials as $key => $testimonial) {
        // debug($testimonial);
        //die;
        ?>

        <div class="items-block">
            <?php if (!empty($testimonial["Testimonial"]["image"])) { ?>
                <a href="#" class="left"><img src="<?php echo get_resized_image_url($testimonial["Testimonial"]["image"], 112 , 85 , true ); ?>" alt="" /></a>
            <?php } ?>
            <div class="block-details">
                <?php /* <h2><a href="<?php echo Router::url(array('action' => 'view', $testimonial['Testimonial']['permalink'])) ?>"><?php echo $testimonial["Testimonial"]["title"]; ?></a></h2> */ ?>
                <p>
                    <?php echo nl2br($testimonial["Testimonial"]["short_description"]); ?>
                </p>
                <?php /*  <a href="<?php echo Router::url(array('action' => 'view', $testimonial['Testimonial']['permalink'])) ?>" class="btn-a corner-all">Visit Site</a> ? */ ?>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }
    ?>         
</div>
<div class="pagination">
    <?php if ($paginator->numbers()) { ?>
        <div class="right paging">
            <ul>
                <li class="previous"><?php echo $paginator->prev('<') ?></li>
                <?php echo $paginator->numbers(array('tag' => 'li', 'separator' => '')) ?>
                <li class="next"><?php echo $paginator->next('>'); ?></li>
            </ul>
        </div>
        <div class="clear"></div>
        <?php
    }
    ?>
</div>